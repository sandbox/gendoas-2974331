<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\XtotRequestFormTrait.
 */

namespace Drupal\sxt_opentalk;

use Drupal\sxt_workflow\SlogXtwf;

/**
 * A Trait for ...
 */
trait XtotRequestFormTrait {

  protected function isAdminFinalize() {
    return FALSE;
  }

  protected function addFieldNotice(array &$form, $description = '') {
    if (empty($description)) {
      $description = t('Specify a reason for your request.');
    }

    $form['notice'] = [
      '#type' => 'textfield',
      '#title' => t('Notice'),
      '#description' => $description,
      '#required' => TRUE,
      '#maxlength' => 64,
        ] + $this->getInputFieldWrapper();
  }

  protected function addFieldWorkflow(array &$form, $default_value = '', $add_extra = FALSE) {
    $node_state = $this->node_state;
    if (empty($default_value)) {
      $default_value = $node_state->getWorkflowId();
    }
    
    $requests_all = $node_state->getPostsRelevant();
    $workflows = SlogXtwf::getWorkflows($this->node->getType(), 'trunk');
    $options = SlogXtwf::getWorkflowOptions($workflows);
    $description = t('Select workflow');
    if (!$add_extra && $this->isAdminFinalize() && $node_state->hasAnyPosts()) {
      unset($options['']);
      $state_plugin = $node_state->getWfStatePlugin();
      $wf_best = $state_plugin->getPreferredBest('workflow');
      $wf_best_num = $state_plugin->getPreferredBest('workflow', 'number');
      $args = [
        '@best' => $options[$wf_best],
        '@num' => (integer) $wf_best_num,
        '@of' => $requests_all,
      ];
      $description = t('Best: @best (@num of @of)', $args);
      $default_value = $wf_best;
    }

    $form['workflow'] = [
      '#type' => 'select',
      '#title' => t('Workflow'),
      '#description' => $description,
      '#options' => $options,
      '#default_value' => $default_value,
      '#required' => TRUE,
      '#attributes' => [
        'id' => "xtot-workflow",
      ],
        ] + $this->getInputFieldWrapper();

    if ($add_extra) {
      $lines = [];
      $lines[] = t('The workflow has been set by the first request, but this can be discussed.');
      $lines[] = t('Select your preferred workflow if you are unhappy with the initial selection.');

      $form["xtot-workflow-none"] = [
        '#type' => 'hidden',
        '#value' => implode('<br />', $lines),
        '#attributes' => [
          'id' => "xtot-workflow-none",
        ],
      ];

      foreach ($workflows as $workflow_id => $workflow) {
        $label = $workflow->label();
        $type_settings = $workflow->getPluginCollections()['type_settings'];
        $description = $type_settings->getConfiguration()['description'] ?? '_???_addFieldWorkflow';
        $form["xtot-workflow-$workflow_id"] = [
          '#type' => 'hidden',
          '#value' => "$label: $description",
          '#attributes' => [
            'id' => "xtot-workflow-$workflow_id",
          ],
        ];
      }
    }
  }

  protected function addFieldSeverity(array &$form, $default_value = '', $add_extra = FALSE, $description = '') {
    $node_state = $this->node_state;
    if (empty($default_value)) {
      $default_value = $node_state->getSeverityId();
    }
    if (empty($description)) {
      $description = t('Select severity');
    }
    
    $requests_all = $node_state->getPostsRelevant();
    if (empty($this->xtwf_state)) {
      $add_extra = FALSE;
    }
    $options = SlogXtwf::getStateSeverityOptions();
    if (!$add_extra && $this->isAdminFinalize() && $node_state->hasAnyPosts()) {
      $options = SlogXtwf::getStateSeverityOptions();
      $state_plugin = $node_state->getWfStatePlugin();
      $sv_best = $state_plugin->getPreferredBest('severity');
      $sv_best_num = $state_plugin->getPreferredBest('severity', 'number');
      $args = [
        '@best' => $options[$sv_best],
        '@num' => (integer) $sv_best_num,
        '@of' => $requests_all,
      ];
      $description = t('Best: @best (@num of @of)', $args);
      $default_value = $sv_best;
    }
    $form['severity'] = [
      '#type' => 'select',
      '#title' => t('Severity'),
      '#description' => $description,
      '#options' => $options,
      '#default_value' => $default_value,
      '#required' => TRUE,
      '#attributes' => [
        'id' => "xtot-severity",
      ],
        ] + $this->getInputFieldWrapper();

    if ($add_extra) {
      $lines = [];
      $lines[] = t('Severity levels differ in the required items to collect and the timeout.');
      $lines[] = t('The selected severity level applies to the entire workflow, but can be changed later by another request.');
      $form["xtot-severity-none"] = [
        '#type' => 'hidden',
        '#value' => implode('<br />', $lines),
        '#attributes' => [
          'id' => "xtot-severity-none",
        ],
      ];
      foreach ($options as $key => $label) {
        if (!empty($key)) {
          $description = $this->xtwf_state->getSeverityDescription($key);
          $form["xtot-severity-$key"] = [
            '#type' => 'hidden',
            '#value' => "$label<br />$description",
            '#attributes' => [
              'id' => "xtot-severity-$key",
            ],
          ];
        }
      }
    }
  }

  protected function addFieldFinalizeBy(array &$form, $default_value = '', $description = '') {
    if (empty($description)) {
      $description = t('What do you prefer, how to finalize state.');
    }
    $options = SlogXtwf::getStateFinalizeOptions();
    if (empty($default_value) || !in_array($default_value, array_keys($options))) {
      $default_value = 'requests';
    }
    $form['finalize'] = [
      '#type' => 'radios',
      '#title' => t('Finalize by'),
      '#description' => $description,
      '#options' => $options,
      '#default_value' => $default_value,
      '#required' => TRUE,
        ] + $this->getInputFieldWrapper(TRUE);
  }

  protected function addFieldStrict(array &$form, $default_value = '', $description = '') {
    if (empty($description)) {
      $description = t('If strict and the required posts are not achieved, timeout will set state to "finished renewable".');
    }
    $options = SlogXtwf::getStrictOptions();
    if (empty($default_value) || !in_array($default_value, array_keys($options))) {
      $default_value = SlogXtwf::XTWF_SELECT_NO;
    }
    $form['strict'] = [
      '#type' => 'radios',
      '#title' => t('Strict'),
      '#description' => $description,
      '#options' => $options,
      '#default_value' => $default_value,
      '#required' => TRUE,
        ] + $this->getInputFieldWrapper(TRUE);
  }

  protected function addFieldDiscuss(array &$form, $default_value = '', $description = '') {
    if ($this->node_state->hasDiscussionState()) {
      if (empty($description)) {
        $description = t('You may vote for a discussion about the next steps.')
            . '<br />' . t('Discussion is opened on simple majority.');
      }      
      
      $options = SlogXtwf::getDiscussOptions();
      if (empty($default_value) || !in_array($default_value, array_keys($options))) {
        $default_value = SlogXtwf::XTWF_SELECT_NO;
      }
      $form['discuss'] = [
        '#type' => 'radios',
        '#title' => SlogXtwf::getTxtLetsDiscuss(),
        '#description' => $description,
        '#options' => $options,
        '#default_value' => $default_value,
        '#required' => TRUE,
        '#weight' => -999,
          ] + $this->getInputFieldWrapper(TRUE);
    }
  }

}
