<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\XtotConfirmControllerBase.
 */

namespace Drupal\sxt_opentalk;

use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\slogxt\Controller\XtConfirmControllerBase;
use Drupal\sxt_opentalk\XtotCollaborateFormTrait;

/**
 * Defines a controller ....
 */
abstract class XtotConfirmControllerBase extends XtConfirmControllerBase {

  use XtotCollaborateFormTrait;

  /**
   */
  public function __construct(ModuleHandlerInterface $module_handler, RendererInterface $renderer, ClassResolverInterface $class_resolver, EventDispatcherInterface $dispatcher) {
    parent::__construct($module_handler, $renderer, $class_resolver, $dispatcher);
    $this->setCollabNodeUserData();
  }

}
