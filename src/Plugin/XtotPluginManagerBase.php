<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Plugin\XtotPluginManagerBase.
 */

namespace Drupal\sxt_opentalk\Plugin;

use Drupal\slogxt\Plugin\DefinitionsSortedTrait;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Symfony\Component\DependencyInjection\Container;

/**
 * Base plugin manager for some types.
 *
 * @ingroup sxt_opentalk_plugins_manager
 */
class XtotPluginManagerBase extends DefaultPluginManager {

  use DefinitionsSortedTrait;

  public function __construct($type, \Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    $plugin_definition_annotation_name = 'Drupal\sxt_opentalk\Annotation\Xtot' . Container::camelize($type);
    $plugin_interface = null;
    parent::__construct("Plugin/sxt_opentalk/$type", $namespaces, $module_handler, //
        $plugin_interface, //
        $plugin_definition_annotation_name);
    $this->setCacheBackend($cache_backend, "sxt_opentalk:{$type}_plugins");
  }

}
