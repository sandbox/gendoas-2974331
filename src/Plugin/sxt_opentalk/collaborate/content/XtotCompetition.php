<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\XtotCompetition.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content;

use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabPluginEditBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collaborate_competition",
 *   bundle = "xtot_collaborate",
 *   title = @Translation("Competition vote"),
 *   route_name = "sxt_opentalk.collaborate.c.competition",
 *   skipable = false,
 *   weight = 0
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class XtotCompetition extends XtotCollabPluginEditBase {

  public function getDescription() {
    return $this->node_state->getStateInfo(TRUE, TRUE);
  }

  /**
   * Overrides \Drupal\slogxt\Plugin\XtPluginEditBase::access();
   */
  protected function access() {
    if ($this->user->isAuthenticated() && $this->has_node_state) {
      return $this->node_state->isStateCompetition();
    }

    return FALSE;
  }

}
