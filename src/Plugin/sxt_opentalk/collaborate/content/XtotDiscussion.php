<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\XtotDiscussion.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content;

use Drupal\sxt_opentalk\SlogXtot;
use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabPluginEditBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collaborate_discussion",
 *   bundle = "xtot_collaborate",
 *   title = @Translation("Discussion"),
 *   route_name = "sxt_opentalk.collaborate.c.discussion",
 *   skipable = false,
 *   weight = 0
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class XtotDiscussion extends XtotCollabPluginEditBase {

  protected $discuss_objects = [];
  protected $has_discuss_node = FALSE;
  protected $has_comments = FALSE;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $route_provider) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $route_provider);
    if ($this->has_node_state) {
      $this->discuss_objects = $objects = SlogXtot::getMwDiscussionObjects($this->node_state, FALSE);
      $this->has_discuss_node = !empty($objects['discussion_node']);
      if ($this->has_discuss_node) {
        $discussion_node_id = $objects['discussion_node']->id();
        $this->has_comments = SlogXtot::mwDiscussionHasComments($discussion_node_id);
      }
    }
  }

  protected function getResolveData() {
    if ($this->has_discuss_node && $this->has_comments) {
      return ['{next_entity_id}' => 'sxt_opentalk::resolvePathDiscussionAction'];
    }
    return FALSE;
  }

  protected function getResolveArgs() {
    if ($this->has_discuss_node && $this->has_comments) {
      $node_id = $this->baseEntityId;
      $route_name = 'sxt_opentalk.collaborate.c.viewdiscussion';
      $route_parameters = ['node_id' => $node_id];
//      info = Drupal.t('Click the view button for viewing the discussion.') + '<br /><br />';
////      info += Drupal.t('After clicking the button you may select your discussion action.');
//      info += Drupal.t('NOTE: You can look up the discussions in the content submenu.');
      $info = t('Click the view button for viewing the discussion.') 
          . '<br /><br />'
          . t('NOTE: You can look up the discussions in the content submenu.');

      return [
        'perms' => $this->getPermissions(),
        'path' => $this->getPathFromRoute($route_name, $route_parameters),
        '{next_entity_id}' => [
          'info' => $info,
          'xtInfo' => t('View discussion and select your edit action.'),
        ],
      ];
    }

    return FALSE;
  }

  public function getTitle() {
    if ($this->node_state->isDiscussForRequest()) {
      return t('Discussion');
    }
    return parent::getTitle();
  }

  public function getDescription() {
    return $this->node_state->getStateInfo(TRUE, TRUE);
  }

  /**
   * Overrides \Drupal\slogxt\Plugin\XtPluginEditBase::access();
   */
  protected function access() {
    if ($this->user->isAuthenticated() && $this->has_node_state) {
      return $this->node_state->isStateDiscussion();
    }

    return FALSE;
  }

}
