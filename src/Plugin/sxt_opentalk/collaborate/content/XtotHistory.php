<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\XtotHistory.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content;

use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabPluginEditBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collaborate_history",
 *   bundle = "xtot_collaborate",
 *   title = @Translation("History"),
 *   description = @Translation("History of node's workflow, i.e. of all states, transitions and their data."),
 *   route_name = "sxt_opentalk.collaborate.c.history",
 *   skipable = false,
 *   weight = 100
 * )
 */
class XtotHistory extends XtotCollabPluginEditBase {

  /**
   * Overrides \Drupal\slogxt\Plugin\XtPluginEditBase::access();
   */
  protected function access() {
    if ($this->user->isAuthenticated() && $this->has_node_state) {
      $history = $this->node_state->getHistory();
      return (!empty($history));
    }

    return FALSE;
  }

}
