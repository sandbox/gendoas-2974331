<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\XtotRequestCollaborate.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabPluginEditBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collaborate_request_collab",
 *   bundle = "xtot_collaborate",
 *   title = @Translation("Request collaboration"),
 *   route_name = "sxt_opentalk.collaborate.c.request_collab",
 *   skipable = false,
 *   weight = 0
 * )
 */
class XtotRequestCollaborate extends XtotCollabPluginEditBase {

  public function getDescription() {
    if ($this->has_node_state) {
      return $this->node_state->getStateInfo(TRUE, TRUE);
    }
    
    return t('Start collaboration request');
  }

  /**
   * Overrides \Drupal\slogxt\Plugin\XtPluginEditBase::access();
   */
  protected function access() {
    if ($this->user->isAuthenticated()) {
      if ($this->has_node_state) {
        return $this->node_state->isStateRequestCollab();
      }
      
      return SlogXtwf::hasNodeWorkflows($this->node_id);
    }

    return FALSE;
  }

}
