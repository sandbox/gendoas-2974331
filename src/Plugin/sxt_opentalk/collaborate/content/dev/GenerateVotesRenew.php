<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\dev\GenerateVotesRenew.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\dev;

use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabAdminPluginBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collaborate_generate_renewrequests",
 *   bundle = "devcgenerate",
 *   title = @Translation("Generate renew votes"),
 *   description = @Translation("You may generate renew requests ..."),
 *   route_name = "sxt_opentalk.collaborate.c.dev.generate_renew_requests",
 *   skipable = false,
 *   weight = 0
 * )
*/
class GenerateVotesRenew extends XtotCollabAdminPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function isActionExecutable() {
    if ($this->has_node_state) {
      return $this->node_state->isStateRequestRenew();
    }
    return FALSE;
  }

}
