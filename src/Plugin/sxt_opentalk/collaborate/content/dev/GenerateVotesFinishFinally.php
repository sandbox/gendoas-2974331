<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\dev\GenerateVotesFinishFinally.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\dev;

use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabAdminPluginBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collaborate_generate_ff_requests",
 *   bundle = "devcgenerate",
 *   title = @Translation("Generate votes for finish finally request"),
 *   description = @Translation("This will generate votes and set timeout to REQUEST_TIME"),
 *   route_name = "sxt_opentalk.collaborate.c.dev.generate_ff_requests",
 *   weight = 0
 * )
*/
class GenerateVotesFinishFinally extends XtotCollabAdminPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function isActionExecutable() {
    if ($this->has_node_state) {
      return $this->node_state->isStateRequestFinishFinally();
    }
    return FALSE;
  }

}
