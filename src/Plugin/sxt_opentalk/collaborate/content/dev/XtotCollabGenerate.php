<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\dev\XtotCollabGenerate.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\dev;

use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabAdminPluginBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collaborate_generate",
 *   bundle = "xtot_collaborate",
 *   title = @Translation("dev: Generate++"),
 *   description = @Translation("Generate entries for development purposes and others."),
 *   route_name = "sxt_opentalk.collaborate.c.dev.generate",
 *   skipable = false,
 *   weight = 99900
 * )
 */
class XtotCollabGenerate extends XtotCollabAdminPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function isActionExecutable() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function access() {
    if ($this->canDevGenerate()) {
      return parent::access();
    }

    return FALSE;
  }

}
