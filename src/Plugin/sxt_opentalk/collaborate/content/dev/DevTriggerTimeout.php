<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\dev\DevTriggerTimeout.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\dev;

use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabAdminPluginBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collab_cdev_trigger_timeout",
 *   bundle = "devcgenerate",
 *   title = @Translation("Trigger timeout"),
 *   description = @Translation("Trigger timeout for testing functioality."),
 *   route_name = "sxt_opentalk.collaborate.c.dev.trigger_timeout",
 *   skipable = false,
 *   weight = 300
 * )
 */
class DevTriggerTimeout extends XtotCollabAdminPluginBase {

  protected function isActionExecutable() {
    if ($this->has_node_state) {
      return !$this->node_state->stateIsFinishedType();
    }

    return FALSE;
  }
  
}
