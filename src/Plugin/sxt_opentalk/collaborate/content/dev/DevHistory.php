<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\dev\DevHistory.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\dev;

use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabAdminPluginBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collaborate_generate_history",
 *   bundle = "devcgenerate",
 *   title = @Translation("Dev-History"),
 *   description = @Translation("History with all node state variables, and current state too."),
 *   route_name = "sxt_opentalk.collaborate.c.dev.history",
 *   skipable = false,
 *   weight = 99900
 * )
 */
class DevHistory extends XtotCollabAdminPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function isActionExecutable() {
    return $this->has_node_state;
  }

}
