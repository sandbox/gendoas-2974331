<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\dev\GenerateDiscussion.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\dev;

use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabAdminPluginBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collaborate_generate_discussion",
 *   bundle = "devcgenerate",
 *   title = @Translation("Generate discussion"),
 *   route_name = "sxt_opentalk.collaborate.c.dev.generate_cdiscussion",
 *   weight = 0
 * )
*/
class GenerateDiscussion extends XtotCollabAdminPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function isActionExecutable() {
   if ($this->has_node_state) {
      return $this->node_state->isStateDiscussion();
    }
    return FALSE;
  }

  public function getDescription() {
    return $this->node_state->getStateInfo(TRUE, TRUE);
  }

}
