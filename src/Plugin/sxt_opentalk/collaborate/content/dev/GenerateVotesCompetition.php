<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\dev\GenerateVotesCompetition.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\dev;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabAdminPluginBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collaborate_generate_cvotes",
 *   bundle = "devcgenerate",
 *   title = @Translation("Generate votes"),
 *   description = @Translation("Generate votes"),
 *   route_name = "sxt_opentalk.collaborate.c.dev.generate_cvotes",
 *   weight = 0
 * )
*/
class GenerateVotesCompetition extends XtotCollabAdminPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function isActionExecutable() {
    if ($this->has_node_state) {
      return ($this->node_state->getStateId() === SlogXtwf::XTWF_COMPETITION);
    }
    return FALSE;
  }

}
