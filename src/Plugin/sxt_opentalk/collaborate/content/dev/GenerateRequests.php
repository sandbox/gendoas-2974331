<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\dev\GenerateRequests.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\dev;

use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabAdminPluginBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collaborate_generate_requests",
 *   bundle = "devcgenerate",
 *   title = @Translation("Generate collaboration requests"),
 *   description = @Translation("You may generate collaboration requests on each workflow state, i.e. reset to beginning"),
 *   route_name = "sxt_opentalk.collaborate.c.dev.generate_requests",
 *   skipable = false,
 *   weight = 100
 * )
*/
class GenerateRequests extends XtotCollabAdminPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function isActionExecutable() {
    return $this->has_node_state;
  }

}
