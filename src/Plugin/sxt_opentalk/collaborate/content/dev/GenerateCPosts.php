<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\dev\GenerateCPosts.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\dev;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabAdminPluginBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collaborate_generate_cposts",
 *   bundle = "devcgenerate",
 *   title = @Translation("Generate posts"),
 *   route_name = "sxt_opentalk.collaborate.c.dev.generate_cposts",
 *   weight = 0
 * )
*/
class GenerateCPosts extends XtotCollabAdminPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function isActionExecutable() {
    if ($this->has_node_state) {
      return $this->node_state->stateIsCollectContentType();
    }
    return FALSE;
  }

  public function getDescription() {
    return $this->node_state->getStateInfo(TRUE, TRUE);
  }

}
