<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\dev\DevRestoreFromHistory.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\dev;

use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabAdminPluginBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collab_cdev_restore_history",
 *   bundle = "devcgenerate",
 *   title = @Translation("Restore from history"),
 *   description = @Translation("Select a state from history and restore it."),
 *   route_name = "sxt_opentalk.collaborate.c.admin.hrestore",
 *   skipable = false,
 *   weight = 200
 * )
 */
class DevRestoreFromHistory extends XtotCollabAdminPluginBase {

  protected function isActionExecutable() {
    if ($this->has_node_state) {
      return $this->node_state->isStateFinishedFinally();
    }

    return FALSE;
  }
  
}
