<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\admin\RestoreFromHistory.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\admin;

use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabAdminPluginBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collab_cadmin_restore_history",
 *   bundle = "ccollab_admin",
 *   title = @Translation("Restore from history"),
 *   description = @Translation("Select a state from history and restore it."),
 *   route_name = "sxt_opentalk.collaborate.c.admin.hrestore",
 *   skipable = false,
 *   weight = 200
 * )
 */
class RestoreFromHistory extends XtotCollabAdminPluginBase {

  protected function isActionExecutable() {
    $node_state = $this->node_state;
    if (parent::isActionExecutable() && !$node_state->isStateFinishedFinally()) {
      $history = $node_state->getHistory();
      return !empty($history);
    }
    return FALSE;
  }
  
}
