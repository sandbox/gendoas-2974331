<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\admin\XtotCollaborateAdmin.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\admin;

use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabAdminPluginBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collaborate_admin_content",
 *   bundle = "xtot_collaborate",
 *   title = @Translation("Administer"),
 *   description = @Translation("Do administrative and extended tasks."),
 *   route_name = "sxt_opentalk.collaborate.c.admin",
 *   skipable = false,
 *   weight = 9900
 * )
 */
class XtotCollaborateAdmin extends XtotCollabAdminPluginBase {

}
