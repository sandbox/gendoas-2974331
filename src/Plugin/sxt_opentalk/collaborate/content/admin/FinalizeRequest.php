<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\admin\FinalizeRequest.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\admin;

use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabAdminPluginBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collab_cadmin_finalize_request",
 *   bundle = "ccollab_admin",
 *   title = @Translation("Finalize request"),
 *   route_name = "sxt_opentalk.collaborate.c.admin.finalize_request",
 *   skipable = false,
 *   weight = 1
 * )
 */
class FinalizeRequest extends XtotCollabAdminPluginBase {

  protected function isActionExecutable() {
    if (parent::isActionExecutable()) {
      $node_state = $this->node_state;
      return $node_state->stateIsRequestType() && !$node_state->isStateRequestRenew();
    }
    return FALSE;
  }

  public function getDescription() {
    return $this->node_state->getStateInfo(TRUE, TRUE);
  }

}
