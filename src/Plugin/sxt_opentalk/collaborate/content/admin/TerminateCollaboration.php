<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\admin\TerminateCollaboration.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\admin;

use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabAdminPluginBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collab_cadmin_terminate",
 *   bundle = "ccollab_admin",
 *   route_name = "sxt_opentalk.collaborate.c.admin.terminate",
 *   skipable = false,
 *   weight = 100
 * )
 */
class TerminateCollaboration extends XtotCollabAdminPluginBase {

  protected function isActionExecutable() {
    return parent::isActionExecutable() && !$this->node_state->isStateRequestFinishFinally();
  }

  public function getTitle() {
    if ($this->node_state->isStateFinishedRenewable()) {
      return t('Terminate collaboration finally');
    }
    return t('Terminate collaboration');
  }
  
  public function getDescription() {
    if ($this->node_state->isStateFinishedRenewable()) {
      return t('If you terminate the collaboration, the state will be set to finished finally.');
    }
    
    return t('If you terminate the collaboration, the state will be set to finished (renewable).');
  }

}
