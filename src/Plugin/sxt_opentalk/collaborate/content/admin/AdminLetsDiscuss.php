<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\admin\AdminLetsDiscuss.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\admin;

use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabAdminPluginBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collab_cadmin_dodiscuss",
 *   bundle = "ccollab_admin",
 *   title = @Translation("Let's discuss"),
 *   description = @Translation("Suspend voting for request and discuss it first."),
 *   route_name = "sxt_opentalk.collaborate.c.admin.dodiscuss",
 *   skipable = false,
 *   weight = 20
 * )
 */
class AdminLetsDiscuss extends XtotCollabAdminPluginBase {

  protected function isActionExecutable() {
    return parent::isActionExecutable() && $this->node_state->stateIsRequestType();
  }

}
