<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\admin\FinalizeDiscussion.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\admin;

use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabAdminPluginBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collab_cadmin_finalize_discussion",
 *   bundle = "ccollab_admin",
 *   title = @Translation("Finalize discussion"),
 *   route_name = "sxt_opentalk.collaborate.c.admin.finalize_discussion",
 *   skipable = false,
 *   weight = 1
 * )
 */
class FinalizeDiscussion extends XtotCollabAdminPluginBase {

  protected function isActionExecutable() {
    if (parent::isActionExecutable()) {
      return $this->node_state->isStateDiscussion();
    }
    return FALSE;
  }
  
  public function getDescription() {
    return $this->node_state->getStateInfo(TRUE, TRUE);
  }

}
