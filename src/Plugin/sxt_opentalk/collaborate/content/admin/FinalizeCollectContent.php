<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\admin\FinalizeCollectContent.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\admin;

use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabAdminPluginBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collab_cadmin_finalize_ccollect",
 *   bundle = "ccollab_admin",
 *   title = @Translation("Finalize content collection"),
 *   route_name = "sxt_opentalk.collaborate.c.admin.finalize_ccollect",
 *   skipable = false,
 *   weight = 1
 * )
 */
class FinalizeCollectContent extends XtotCollabAdminPluginBase {

  protected function isActionExecutable() {
    if (parent::isActionExecutable()) {
      return $this->node_state->stateIsCollectContentType();
    }
    return FALSE;
  }
  
  public function getDescription() {
    return $this->node_state->getStateInfo(TRUE, TRUE);
  }

}
