<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\admin\CollabRenew.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\admin;

use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabPluginEditBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collab_cadmin_renew",
 *   bundle = "ccollab_admin",
 *   title = @Translation("Renew collaboration"),
 *   description = @Translation("Renew collaboration by selecting a content collecting state"),
 *   route_name = "sxt_opentalk.collaborate.c.admin.renew",
 *   skipable = false,
 *   weight = 0
 * )
 */
class CollabRenew extends XtotCollabPluginEditBase {

  /**
   * Overrides \Drupal\slogxt\Plugin\XtPluginEditBase::access();
   */
  protected function access() {
    if ($this->user->isAuthenticated() && $this->has_node_state) {
      $node_state = $this->node_state;
      return ($node_state->isStateFinishedRenewable() || $node_state->isStateRequestRenew());
    }

    return FALSE;
  }

}
