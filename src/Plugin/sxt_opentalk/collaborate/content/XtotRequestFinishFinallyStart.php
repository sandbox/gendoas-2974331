<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\XtotRequestFinishFinallyStart.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content;

use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabPluginEditBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_request_finish_finally_start",
 *   bundle = "xtot_collaborate",
 *   title = @Translation("Request for finish finally"),
 *   description = @Translation("Start a request for finish finally."),
 *   route_name = "sxt_opentalk.collaborate.c.request_finish_finally",
 *   skipable = false,
 *   weight = 800
 * )
 */
class XtotRequestFinishFinallyStart extends XtotCollabPluginEditBase {
  // Start request with weight=800 !!!!

  /**
   * Overrides \Drupal\slogxt\Plugin\XtPluginEditBase::access();
   */
  protected function access() {
    if ($this->user->isAuthenticated() && $this->has_node_state) {
      return $this->node_state->canRequestFinishFinally();
    }

    return FALSE;
  }

}
