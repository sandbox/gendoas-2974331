<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\XtotRequestRenew.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content;

use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabPluginEditBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_request_renew",
 *   bundle = "xtot_collaborate",
 *   route_name = "sxt_opentalk.collaborate.c.request_renew",
 *   skipable = false,
 *   weight = 0
 * )
 */
class XtotRequestRenew extends XtotCollabPluginEditBase {

  public function getTitle() {
    if ($this->node_state->isStateRequestRenew()) {
      return t('Vote for renew');
    }
    return t('Request for renew');
  }
  
  public function getDescription() {
    return $this->node_state->getStateInfo(TRUE, TRUE);
  }

  /**
   * Overrides \Drupal\slogxt\Plugin\XtPluginEditBase::access();
   */
  protected function access() {
    if ($this->user->isAuthenticated() && $this->has_node_state) {
      return $this->node_state->canRequestRenew();
    }

    return FALSE;
  }

}
