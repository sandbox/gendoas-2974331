<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\XtotRequestFinishFinally.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content;

use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabPluginEditBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_request_finish_finally",
 *   bundle = "xtot_collaborate",
 *   title = @Translation("Vote for finish finally"),
 *   route_name = "sxt_opentalk.collaborate.c.request_finish_finally",
 *   skipable = false,
 *   weight = 0
 * )
 */
class XtotRequestFinishFinally extends XtotCollabPluginEditBase {
  // Vote for finish finally with weight=0 !!!!

  public function getDescription() {
    return $this->node_state->getStateInfo(TRUE);
  }

  /**
   * Overrides \Drupal\slogxt\Plugin\XtPluginEditBase::access();
   */
  protected function access() {
    if ($this->user->isAuthenticated() && $this->has_node_state) {
      $node_state = $this->node_state;
      return ($node_state->isStateRequestFinishFinally());
    }

    return FALSE;
  }

}
