<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\XtotCollectContent.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content;

use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabPluginEditBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collaborate_ccollect",
 *   bundle = "xtot_collaborate",
 *   title = @Translation("Content collection"),
 *   route_name = "sxt_opentalk.collaborate.c.collect",
 *   skipable = false,
 *   weight = 0
 * )
 */
class XtotCollectContent extends XtotCollabPluginEditBase {

  public function getDescription() {
    return $this->node_state->getStateInfo(TRUE, TRUE);
  }

  /**
   * Overrides \Drupal\slogxt\Plugin\XtPluginEditBase::access();
   */
  protected function access() {
    if ($this->user->isAuthenticated() && $this->has_node_state) {
      return $this->node_state->isStateCollectContent();
    }

    return FALSE;
  }

}
