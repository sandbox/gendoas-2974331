<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\XtotCancelRequest.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabPluginEditBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collaborate_cancel_request",
 *   bundle = "xtot_collaborate",
 *   route_name = "sxt_opentalk.collaborate.c.cancel_request",
 *   skipable = false,
 *   weight = 1
 * )
 */
class XtotCancelRequest extends XtotCollabPluginEditBase {

  public function getTitle() {
    $node_state = $this->node_state;
    if ($node_state->isStateRequestFinishFinally() || $node_state->isStateRequestRenew()) {
      return t('Cancel your vote');
    }
    return t('Cancel your request');
  }

  public function getDescription() {
    $user_data = $this->node_state->getUserData($this->user_id);
    if (!empty($user_data)) {
      $node_state = $this->node_state;
      if ($node_state->isStateRequestFinishFinally()) {
        $txt_vote = (string) SlogXtwf::getVoteLabel($user_data['vote']);
        $description = t('Your vote for finish finally request: @vote.', ['@vote' => $txt_vote]);
        return $description . '<br />' . t('You may cancel your vote.');
      }
      elseif ($node_state->isStateRequestRenew()) {
        $renew_ids = $node_state->getOptionsRequestRenew();
        $renew_id = $user_data['renew_id'] ?? $node_state->getWorkflowId();
        $severity_id = $user_data['severity'] ?? $node_state->getSeverityId();
        $args = [
          '@renew_id' => ($renew_ids[$renew_id] ?? $renew_id),
          '@severity' => SlogXtwf::getSeverityLabel($severity_id),
        ];
        $description = t('You voted with next=@renew_id and severity=@severity.', $args);
        return $description . '<br />' . t('You may cancel your vote.');
      }
      elseif ($node_state->isStateRequestCollab()) {
        $workflow_id = $user_data['workflow'] ?? $node_state->getWorkflowId();
        $workflow = SlogXtwf::getWorkflow($workflow_id);
        $severity_id = $user_data['severity'] ?? $node_state->getSeverityId();
        $args = [
          '@workflow' => $workflow ? $workflow->label() : $workflow_id,
          '@severity' => SlogXtwf::getSeverityLabel($severity_id),
        ];
        $description = t('You requested with workflow=@workflow and severity=@severity.', $args);
        return $description . '<br />' . t('You may cancel your request.');
      }
    }

    return t("Cancel your request for currently active node's state.");
  }

  /**
   * Overrides \Drupal\slogxt\Plugin\XtPluginEditBase::access();
   */
  protected function access() {
    if ($this->user->isAuthenticated() && $this->has_node_state && $this->node_state->stateIsRequestType()) {
      return $this->has_user_data;
    }

    return FALSE;
  }

}
