<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content\XtotSubscribe.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\content;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\sxt_workflow\XtwfNodeSubscribe;
use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabPluginEditBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collaborate_subscribe",
 *   bundle = "xtot_collaborate",
 *   route_name = "sxt_opentalk.collaborate.c.subscribe",
 *   skipable = false,
 *   weight = 10
 * )
 */
class XtotSubscribe extends XtotCollabPluginEditBase {

  protected $has_subscribed;

  public function getTitle() {
    if ($this->has_subscribed) {
      return t('Unsubscribe');
    }
    return t('Subscribe');
  }

  public function getDescription() {
    if ($this->has_subscribed) {
      $description = t('You have subscribed to receive e-mail notification of each transition.');
      return $description . '<br />' . t('You may unsubscibe.');
    }

    return t('Subscribe if you want to be notified by e-mail about each transition.');
  }

  /**
   * Overrides \Drupal\slogxt\Plugin\XtPluginEditBase::access();
   */
  protected function access() {
    if (SlogXtwf::canNotify() && $this->user->isAuthenticated()) {
      if ($this->has_node_state && !$this->node_state->isStateFinishedFinally()) {
        $this->has_subscribed = XtwfNodeSubscribe::hasSubscribed($this->node_id, $this->user_id);
        return TRUE;
      }
    }

    return FALSE;
  }

}
