<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\main\Subscriptions.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\main;

use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 * @XtotCollaborate(
 *   id = "xtot_collab_main_subscriptions",
 *   bundle = "xtot_collab_main",
 *   title = @Translation("Subscriptions"),
 *   description = @Translation("List of contents you are subscribed for"),
 *   route_name = "sxt_opentalk.collaborate.main.subscriptions",
 *   skipable = false,
 *   weight = 0
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class Subscriptions extends XtPluginEditBase {

}
