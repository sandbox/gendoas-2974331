<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabAdminPluginBase.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabPluginEditBase;
use Drupal\sxt_opentalk\XtotDevGenerateTrait;

/**
 */
class XtotCollabAdminPluginBase extends XtotCollabPluginEditBase {

  use XtotDevGenerateTrait;

  /**
   * Whether action is executable.
   * 
   * @return boolean
   */
  protected function isActionExecutable() {
    if ($this->has_node_state) {
      return !$this->node_state->isStateFinishedFinally();
    }
    
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function access() {
    if (isset($this->node) && SlogXtwf::hasNodePermCollaborateAdmin($this->node)) {
      return $this->isActionExecutable();
    }

    return FALSE;
  }

}
