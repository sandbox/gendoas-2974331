<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate\XtotCollabPluginEditBase.
 */

namespace Drupal\sxt_opentalk\Plugin\sxt_opentalk\collaborate;

use Drupal\slogxt\Plugin\XtPluginEditBase;
use Drupal\sxt_opentalk\XtotCollaborateTrait;

/**
 */
class XtotCollabPluginEditBase extends XtPluginEditBase {

  use XtotCollaborateTrait;
  
  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $route_provider) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $route_provider);
    $this->setCollabNodeUserData();
  }
 
}
