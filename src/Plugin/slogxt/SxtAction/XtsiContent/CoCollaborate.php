<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Plugin\slogxt\SxtAction\XtsiContent\CoCollaborate.
 */

namespace Drupal\sxt_opentalk\Plugin\slogxt\SxtAction\XtsiContent;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_opentalk_content_collaborate",
 *   title = @Translation("Collaborate"),
 *   menu = "xtsi_content",
 *   path = "collaborate",
 *   cssClass = "icon-collaborate",
 *   xtProvider = "sxt_opentalk",
 *   needsAuth = TRUE,
 *   useFilter = "nodetypeNeedsWorkflow",
 *   weight = -20
 * )
 */
class CoCollaborate extends SxtActionPluginBase {

}
