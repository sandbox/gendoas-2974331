<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Plugin\slogxt\SxtAction\XtsiContent\CoVote.
 */

namespace Drupal\sxt_opentalk\Plugin\slogxt\SxtAction\XtsiContent;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_opentalk_content_vote",
 *   title = @Translation("Vote"),
 *   menu = "xtsi_content",
 *   path = "vote",
 *   cssClass = "icon-vote",
 *   xtProvider = "sxt_opentalk",
 *   needsAuth = TRUE,
 *   weight = -21
 * )
 */
class CoVote extends SxtActionPluginBase {

}
