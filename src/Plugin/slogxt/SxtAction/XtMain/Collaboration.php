<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Plugin\slogxt\SxtAction\XtMain\Collaboration.
 */

namespace Drupal\sxt_opentalk\Plugin\slogxt\SxtAction\XtMain;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_collaboration",
 *   title = @Translation("Collaboration"),
 *   menu = "main_dropbutton",
 *   path = "collaboration",
 *   cssClass = "icon-collaborate",
 *   cssItemId = "slogxtmaindropbutton-li-collaborate",
 *   xtProvider = "sxt_opentalk",
 *   needsAuth = TRUE,
 *   weight = -920
 * )
 */
class Collaboration extends SxtActionPluginBase {

  public function access() {
    return !\Drupal::currentUser()->isAnonymous();
  }

}
