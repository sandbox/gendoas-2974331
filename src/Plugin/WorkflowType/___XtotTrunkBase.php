<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Plugin\WorkflowType\XtotTrunkBase.
 */

namespace Drupal\sxt_opentalk\Plugin\WorkflowType;

use Drupal\sxt_workflow\Plugin\WorkflowType\SxtWorkflowTypeBase;

/**
 * Attaches workflows to content entity types and their bundles.
 *
 * @WorkflowType___XX(
 *   id = "xtot_trunk_base",
 *   label = @Translation("Base trunk"),
 *   required_states = {
 *     "xtot_request_collaborate",
 *     "xtot_request_renewal",
 *     "xtot_finish_renewable",
 *     "xtot_finish_finally",
 *   },
 *   required_transitions = {
 *     "xtot_request_renewal",
 *     "xtot_finish_renewable",
 *     "xtot_finish_finally",
 *   },
 *   required_bundle = "trunk",
 * )
 */
class XtotTrunkBase extends SxtWorkflowTypeBase {

}
