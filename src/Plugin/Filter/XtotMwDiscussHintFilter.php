<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Plugin\Filter\XtotMwDiscussHintFilter.
 */

namespace Drupal\sxt_opentalk\Plugin\Filter;

use Drupal\sxt_opentalk\SlogXtot;
use Drupal\slogxt\SlogXt;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter for replacing headers in MediaWiki text.
 * 
 * @Filter(
 *   id = "xtot_wfdiscuss_hint",
 *   title = @Translation("Replace WF Diskussion hint placeholder"),
 *   description = @Translation("This works only for generated discussion nodes, where placeholders are inserted."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class XtotMwDiscussHintFilter extends FilterBase {


  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $pattern = '|' . SlogXtot::XTOTT_WFDISCUSS_HINT . '|';
    preg_match($pattern, $text, $match);
    if (!empty($match)) {
      $cls = 'class="mwdiscuss-start-info"';
      $replace = "<div $cls>\n" . SlogXtot::getWfDiscussHint() . '</div>';
      $text = str_replace($match[0], $replace, $text);
      $pattern = '|' . SlogXtot::XTOTT_WFDISCUSS_MAX . '|';
      preg_match($pattern, $text, $match);
      if (!empty($match)) {
        $text = str_replace($match[0], SlogXtot::getWfDiscussMax(), $text);
      }
      $pattern = '|' . SlogXtot::XTOTT_MWC_MAXDEPTH . '|';
      preg_match($pattern, $text, $match);
      if (!empty($match)) {
        $text = str_replace($match[0], SlogXt::getMwCommentMaxDepth(), $text);
      }
    }
    return new FilterProcessResult($text);
  }

}
