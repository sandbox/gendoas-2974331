<?php

/**
 * @file
 * Definition of Drupal\sxt_opentalk\Plugin\slogtx\TxRootTermGet\sys_node\TxCollaborate.
 */

namespace Drupal\sxt_opentalk\Plugin\slogtx\TxRootTermGet\sys_node;

use Drupal\slogtx\Plugin\slogtx\TxSysNodeRootTermGet;

/**
 * @TxSysNodeRootTermGet(
 *   id = "collaborate",
 *   vocabulary_description = @Translation("Collaboration storage for node entities."),
 * )
 * 
 *   bundle = "mainprosur",
 *   title = @Translation("Promoted select"),
 */
class TxCollaborate extends TxSysNodeRootTermGet {
  

}
