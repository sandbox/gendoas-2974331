<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Plugin\XtotPluginManagerCollaborate.
 */

namespace Drupal\sxt_opentalk\Plugin;

/**
 * Plugin manager for sxt_opentalk edit plugins.
 *
 * @ingroup sxt_opentalk_plugins_manager
 */
class XtotPluginManagerCollaborate extends XtotPluginManagerBase {

  public function getActionsData($bundle, $base_entity_id) {
    $definitions = $this->getDefinitionsSorted();
    $actionsData = [];
    $configuration = ['baseEntityId' => $base_entity_id];
    foreach ($definitions as $plugin_id => $definition) {
      if ($definition['bundle'] === $bundle) {
        $plugin = $this->createInstance($plugin_id, $configuration);
        if ($actionData = $plugin->getActionData()) {
          $actionsData[] = $actionData;
        }
      }
    }

    return $actionsData;
  }

}
