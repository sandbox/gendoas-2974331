<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Event\XtotEventSubscriber.
 */

namespace Drupal\sxt_opentalk\Event;

use Drupal\sxt_opentalk\SlogXtot;
use Drupal\sxt_slogitem\Event\SlogitemEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Slogitem event subscriber.
 */
class XtotEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    //
    //
    $events[SlogitemEvents::XTSI_SPECIAL_ENTITY_DATA][] = ['onGetNodeSpecialData', 0];

    return $events;
  }

  /**
   * Whether collaboratin area has to be shown for a given node.
   * 
   * @param Event $event
   */
  public function onGetNodeSpecialData(Event $event) {
    $node = $event->getData()[0];
    $event->entitySpecialData = SlogXtot::getNodeStateSpecialData($node->id());
  }

}
