<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Form\XtotSettingsForm.
 */

namespace Drupal\sxt_opentalk\Form;

use Drupal\sxt_opentalk\SlogXtot;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * SlogTx settings form.
 */
class XtotSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_opentalk_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sxt_opentalk.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['wf_show_allways'] = [
      '#type' => 'checkbox',
      '#title' => t('Workflow show allways'),
      '#description' => t('If not set collaboration is shown only if it is active.'),
      '#default_value' => SlogXtot::getWfShowOnrequest(),
    ];

    $form['wfdiscuss'] = [
      '#type' => 'details',
      '#title' => $this->t('MW Discussion'),
      '#open' => TRUE,
    ];

    $form['wfdiscuss']['wfdiscuss_max'] = [
      '#type' => 'textfield',
      '#title' => t('Max. comments'),
      '#description' => t("Max. comments to be posted. Min=20, Max=50, default=30"),
      '#default_value' => SlogXtot::getWfDiscussMax(),
      '#size' => 10,
    ];
    $args = [
      '@maxdisc' => SlogXtot::XTOTT_WFDISCUSS_MAX,
      '@maxdepth' => SlogXtot::XTOTT_MWC_MAXDEPTH,
    ];
    $form['wfdiscuss']['wfdiscuss_hint'] = [
      '#type' => 'textarea',
      '#title' => t('Hint'),
      '#description' => t('MW Discussion hint, maybe in MedaWiki syntax. You may use token @maxdisc and @maxdepth.', $args),
      '#rows' => 3,
      '#required' => TRUE,
      '#default_value' => SlogXtot::getWfDiscussHint(),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $wfdiscuss_max = (integer) $form_state->getValue('wfdiscuss_max');
    $msg_not_valid = t('Not a valid value.');
    if ($wfdiscuss_max < 20 || $wfdiscuss_max > 50) {
      $form_state->setErrorByName('wfdiscuss_max', $msg_not_valid);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('sxt_opentalk.settings');
    $values = $form_state->getValues();
    $config->set('wf_show_allways', $values['wf_show_allways'])
        ->set('wfdiscuss_max', $values['wfdiscuss_max'])
        ->set('wfdiscuss_hint', trim($values['wfdiscuss_hint']))
        ->save();
    drupal_set_message($this->t('Xtot settings have been saved.'));
  }

}
