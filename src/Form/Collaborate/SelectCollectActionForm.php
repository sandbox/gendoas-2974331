<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Form\Collaborate\SelectCollectActionForm.
 */

namespace Drupal\sxt_opentalk\Form\Collaborate;

use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 * Provides a selection form for collect content actions.
 */
class SelectCollectActionForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_opentalk_select_collectaction';
  }

  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $items = [];
    $request = \Drupal::request();
    $path_info = urldecode($request->getPathInfo());
    $options = [
      'view' => [
        'title' => t('View'),
        'description' => t('View your posted content.'),
      ],
      'edit' => [
        'title' => t('Edit'),
        'description' => t('You can edit your post as long as you have not shared it.'),
      ],
      'delete' => [
        'title' => t('Delete'),
        'description' => t('You are free to delete your post as long as you have not shared it.'),
      ],
      'share' => [
        'title' => t('Share'),
        'description' => t('Note: shared post can no longer be edited or deleted.'),
      ],
    ];

    foreach ($options as $action_id => $data) {
      $items[] = [
        'liTitle' => $data['title'],
        'liDescription' => $data['description'],
        'path' => str_replace('{action}', $action_id, $path_info),
      ];
    }

    return $items;
  }

}
