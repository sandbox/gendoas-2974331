<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Form\Collaborate\CollabRadiosFormBase.
 */

namespace Drupal\sxt_opentalk\Form\Collaborate;

use Drupal\slogxt\Form\XtRadiosFormBase;
use Drupal\sxt_opentalk\XtotCollaborateFormTrait;

/**
 * Provides a ... form.
 */
abstract class CollabRadiosFormBase extends XtRadiosFormBase {

  use XtotCollaborateFormTrait;

  public function __construct() {
    $this->setCollabNodeUserData();
  }

}
