<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Form\Collaborate\SelectSeverityForm.
 */

namespace Drupal\sxt_opentalk\Form\Collaborate;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 * Provides a ... form.
 */
class SelectSeverityForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_opentalk_select_severity';
  }

  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $items = [];
    $request = \Drupal::request();
    $path_info = urldecode($request->getPathInfo());
    $severity_infos = $request->get('severityInfos');

    foreach (SlogXtwf::getStateSeverityOptions() as $severity_id => $label) {
      $items[] = [
        'liTitle' => $label,
        'liDescription' => $severity_infos[$severity_id] ?? '_???_getXtOptions',
        'liButton' => SlogXt::htmlLiButton(),
        'path' => str_replace('{severity}', $severity_id, $path_info),
      ];
    }

    return $items;
  }

}
