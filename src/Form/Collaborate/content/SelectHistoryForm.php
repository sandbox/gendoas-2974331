<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Form\Collaborate\content\SelectHistoryForm.
 */

namespace Drupal\sxt_opentalk\Form\Collaborate\content;

use Drupal\slogxt\SlogXt;
use Drupal\sxt_opentalk\Form\Collaborate\content\CollabHistoryForm;
use Drupal\slogxt\XtDateFormaterTrait;

/**
 * Provides a ... form.
 */
class SelectHistoryForm extends CollabHistoryForm {

  use XtDateFormaterTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xtot_ccolab_restore_from_history';
  }

  protected function hideRadios() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $node_state = $this->node_state;
    $rollback_values = $node_state->getValues();

    $options = [];
    $date_formater = $this->getXtDateFormater();
    $history_data = $node_state->getHistory();
    $path_info = urldecode(\Drupal::request()->getPathInfo());
    foreach ($history_data as $timestamp => $values) {
      $node_state->setValues($values);

      $state_label = $node_state->getWfState()->label();
      $f_changed = $date_formater->format($timestamp, 'xt_date_only');
      $options[$timestamp] = [
        'liTitle' => "$f_changed: $state_label",
        'liDescription' => $node_state->getSubtitleInfo(),
        'liButton' => SlogXt::htmlLiButton(),
        'liDetails' => $node_state->getStateInfo(TRUE, FALSE, FALSE),
        'path' => str_replace('{history_key}', $timestamp, $path_info),
      ];
    }

    $node_state->setValues($rollback_values);
    krsort($options);
    return array_values($options);
  }

}
