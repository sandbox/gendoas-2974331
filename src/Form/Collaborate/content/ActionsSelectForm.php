<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Form\Collaborate\content\ActionsSelectForm.
 */

namespace Drupal\sxt_opentalk\Form\Collaborate\content;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 * Provides the action select form for collaborate.
 */
class ActionsSelectForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xtot_actions_collaborate_select_form';
  }
  
  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $baseEntityId = \Drupal::request()->get('base_entity_id');
    $items = SlogXt::pluginManager('collaborate', 'sxt_opentalk')->getActionsData('xtot_collaborate', $baseEntityId);
    foreach ($items as & $item) {
      $item['liButton'] = SlogXt::htmlLiButton();
    }
    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmptyWarning() {
    return t('Collaboration is not supported for this node type.');
  }

}
