<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Form\Collaborate\content\dev\DevCollabHistoryForm.
 */

namespace Drupal\sxt_opentalk\Form\Collaborate\content\dev;

use Drupal\sxt_opentalk\Form\Collaborate\content\CollabHistoryForm;

/**
 * Provides a ... form.
 */
class DevCollabHistoryForm extends CollabHistoryForm {

  protected function getStateDetails($node_state) {
    $values = $node_state->getValues();
    unset($values['history']);

    $info = '<div style=" resize: both; "><pre>' . var_export($values, true) . '</pre></div>';
    return $info;
  }

}
