<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Form\Collaborate\content\dev\DevActionSelectForm.
 */

namespace Drupal\sxt_opentalk\Form\Collaborate\content\dev;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 * Provides a ... form.
 */
class DevActionSelectForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_opentalk_devgenerate_action';
  }
  
  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $baseEntityId = \Drupal::request()->get('base_entity_id');
    return SlogXt::pluginManager('collaborate', 'sxt_opentalk')->getActionsData('devcgenerate', $baseEntityId);
  }

}
