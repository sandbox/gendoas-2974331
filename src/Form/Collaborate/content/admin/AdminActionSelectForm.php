<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Form\Collaborate\content\admin\AdminActionSelectForm.
 */

namespace Drupal\sxt_opentalk\Form\Collaborate\content\admin;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 * Provides a ... form.
 */
class AdminActionSelectForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_opentalk_ccollab_admin_actions';
  }
  
  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $baseEntityId = \Drupal::request()->get('base_entity_id');
    return SlogXt::pluginManager('collaborate', 'sxt_opentalk')->getActionsData('ccollab_admin', $baseEntityId);
  }

}
