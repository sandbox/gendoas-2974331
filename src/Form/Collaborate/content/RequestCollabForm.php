<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Form\Collaborate\content\RequestCollabForm.
 */

namespace Drupal\sxt_opentalk\Form\Collaborate\content;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\sxt_workflow\XtwfNodeSubscribe;
use Drupal\sxt_workflow\Entity\XtwfNodeState;
use Drupal\slogxt\Controller\AjaxFormControllerBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_opentalk\Form\Collaborate\CollabEditFormBase;
use Drupal\sxt_opentalk\XtotRequestFormTrait;

/**
 * Provides a ... form.
 */
class RequestCollabForm extends CollabEditFormBase {

  use XtotRequestFormTrait;

  public function __construct() {
    parent::__construct();

    $request = \Drupal::request();
    $this->node = $request->get('node');
    $this->isAdmin = SlogXtwf::hasNodePermCollaborateAdmin($this->node);
//todo::next:: ----sxt_workflow --- notifikation
    $this->can_notify = SlogXtwf::canNotify();

    $this->severity_id = $request->get('severity');
    $this->workflow = $request->get('workflow');
    if (!$this->workflow) {
      $message = t('Workflow not found or workflow invalid.');
      throw new \LogicException($message);
    }

    $this->xtwf_state = $xtwf_state = $this->workflow->getTypePlugin()->getState(SlogXtwf::XTWF_REQUEST_COLLABORATE);

    $state_id = $this->xtwf_state->id();
    $workflow_id = $this->workflow->id();
    if ($this->has_node_state) {
      $this->node_state->assertState($state_id, $workflow_id);
      if ($this->has_user_data) {
        $this->can_notify = FALSE;
      }
    }
    else {
      $severity = $xtwf_state->getSeverity($this->severity_id);
      $timeout = $severity['timeout'];
      $this->node_state = XtwfNodeState::create([
            'nid' => $this->node_id,
            'state' => $state_id,
            'workflow' => $workflow_id,
            'severity' => $this->severity_id,
            'data' => [],
            'expire' => $this->getExpireTimestamp($timeout),
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xtot_collaborate_request_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $workflow_id = $this->workflow->id();
    $severity_id = $this->severity_id;
    $finalize_id = 'requests';
    $strict_id = SlogXtwf::XTWF_SELECT_NO;
    $discuss_id = SlogXtwf::XTWF_SELECT_NO;
    if ($this->has_node_state && $this->node_state->hasUserData($this->user_id)) {
      $user_data = $this->node_state->getUserData($this->user_id) ?? [];
      $workflow_id = $user_data['workflow'] ?? $workflow_id;
      $severity_id = $user_data['severity'] ?? $severity_id;
      $finalize_id = $user_data['finalize'] ?? $finalize_id;
      $strict_id = $user_data['strict'] ?? $strict_id;
      $discuss_id = $user_data['discuss'] ?? $discuss_id;
    }

    $wrapper = $this->getInputFieldWrapper();
    $workflows = SlogXtwf::getWorkflows($this->node->getType(), 'trunk');
    if ($this->has_node_state) {
      if (count($workflows) > 1) {
        $this->addFieldWorkflow($form, $workflow_id, TRUE);
      }
      else {
        $form['workflow'] = [
          '#type' => 'hidden',
          '#value' => $workflow_id,
        ];
      }

      $this->addFieldSeverity($form, $severity_id, TRUE);
    }
    else {
      $this->addFieldNotice($form);
      $form['workflow'] = [
        '#type' => 'hidden',
        '#value' => $workflow_id,
      ];
      $form['severity'] = [
        '#type' => 'hidden',
        '#value' => $severity_id,
      ];
    }

    // how to finalize
    $this->addFieldFinalizeBy($form, $finalize_id);
    // strict
    $this->addFieldStrict($form, $strict_id);
    // discuss
    $this->addFieldDiscuss($form, $discuss_id);

    // notifikation
    if ($this->can_notify && !XtwfNodeSubscribe::hasSubscribed($this->node_id, $this->user_id)) {
      $form['notify'] = [
        '#type' => 'checkbox',
        '#title' => t('Subscribe for notifikation'),
        '#description' => t('Check if you want to be notified by e-mail about each transition.'),
        '#default_value' => TRUE,
          ] + $wrapper;
    }

    $this->makeMsgClosable($form);
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->formValidateHasChanged($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $calledObject = AjaxFormControllerBase::getCalledObject();
    $values = $form_state->getValues();
    if ($this->can_notify && isset($values['notify'])) {
      if ((boolean) $values['notify']) {
        XtwfNodeSubscribe::subscribe($this->node_id, $this->user_id);
      }
      else {
        XtwfNodeSubscribe::unsubscribe($this->node_id, $this->user_id);
      }
    }

    $node_state = $this->node_state;
    $user_data = [
      'severity' => $values['severity'],
      'workflow' => $values['workflow'],
      'finalize' => $values['finalize'],
      'strict' => $values['strict'],
    ];
    if (!empty($values['discuss'])) {
      $user_data['discuss'] = $values['discuss'];
    }
    $node_state->setUserData($this->user_id, $user_data);
    if (!empty($values['notice'])) {
      $request_data = [
        'notice' => (string) $values['notice'],
      ];
      $finalize_by_timeout = $node_state->getFinalizeByTimeoutFlag($values['finalize']);
      $finalize_strict = $node_state->getFinalizeStrictFlag($values['strict']);
      $node_state->setData($request_data, 'request_data')
          ->setData($finalize_by_timeout, 'finalize_by_timeout')
          ->setData($finalize_strict, 'finalize_strict');
    }
    $node_state->save();

    // check whether to finalize by required posts
    if ($result = $node_state->checkFinalizeByRequired()) {
      $success = (boolean) $result['success'];
      if (!empty($result['message'])) {
        $calledObject->addFinalMoreMessage($result['message'], $success);
      }
    }

    $msg = $node_state->getStateInfo(TRUE, TRUE);
    $calledObject->setPreFormMessage($msg, $form_state);
  }

}
