<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Form\Collaborate\content\CollabHistoryForm.
 */

namespace Drupal\sxt_opentalk\Form\Collaborate\content;

use Drupal\slogxt\SlogXt;
use Drupal\sxt_opentalk\Form\Collaborate\CollabRadiosFormBase;
use Drupal\slogxt\XtDateFormaterTrait;

/**
 * Provides a ... form.
 */
class CollabHistoryForm extends CollabRadiosFormBase {

  use XtDateFormaterTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xtot_ccolab_history';
  }

  protected function hideRadios() {
    return TRUE;
  }

  protected function getStateDetails($node_state) {
    return $node_state->getStateInfo(TRUE, FALSE, FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $node_state = $this->node_state;
    $rollback_values = $node_state->getValues();

    $options = [];
    $date_formater = $this->getXtDateFormater();
    $history_data = $node_state->getHistory();
    foreach ($history_data as $timestamp => $values) {
      $node_state->setValues($values);

      $state_label = $node_state->getWfState()->label();
      $f_changed = $date_formater->format($timestamp, 'xt_date_only');
      $options[$timestamp] = [
        'liTitle' => "$f_changed: $state_label",
        'liDescription' => $node_state->getSubtitleInfo(),
        'liButton' => SlogXt::htmlLiButton(),
        'liDetails' => $this->getStateDetails($node_state),
        'action' => FALSE,
      ];
    }

    // add current state
    $node_state->setValues($rollback_values);
    $options[REQUEST_TIME] = [
      'liTitle' => $node_state->infoCurrentState(TRUE),
      'liDescription' => $node_state->getSubtitleInfo(),
      'liButton' => SlogXt::htmlLiButton(),
      'liDetails' => $this->getStateDetails($node_state),
      'action' => FALSE,
    ];

    krsort($options);
    return array_values($options);
  }

}
