<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Form\Collaborate\main\ActionsSelectForm.
 */

namespace Drupal\sxt_opentalk\Form\Collaborate\main;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 * Provides the action select form for collaborate.
 */
class ActionsSelectForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xtot_actions_collab_main_select_form';
  }
  
  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $baseEntityId = \Drupal::request()->get('base_entity_id');
    return SlogXt::pluginManager('collaborate', 'sxt_opentalk')->getActionsData('xtot_collab_main', $baseEntityId);
  }

  /**
   * {@inheritdoc}
   */
  public function getEmptyWarning() {
    return t('There is no collaboration yet.');
  }

}
