<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Form\Collaborate\SelectWorkflowForm.
 */

namespace Drupal\sxt_opentalk\Form\Collaborate;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 * Provides a ... form.
 */
class SelectWorkflowForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_opentalk_select_workflow';
  }

  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $request = \Drupal::request();
    $node = $request->get('node');
    $constraint = $request->get('xtwf_constraint');
    $workflows = SlogXtwf::getWorkflows($node->getType(), $constraint);

    $items = [];
    $path_info = urldecode(\Drupal::request()->getPathInfo());
    foreach ($workflows as $workflow_id => $workflow) {
      $type_settings = $workflow->getPluginCollections()['type_settings'];   
      $description = $type_settings->getConfiguration()['description'] ?? '_???_getXtOptions';
      $items[] = [
        'liTitle' => $workflow->label(),
        'liDescription' => $description,
        'liButton' => SlogXt::htmlLiButton(),
        'path' => str_replace('{workflow}', $workflow_id, $path_info),
      ];
    }

    return $items;
  }

}
