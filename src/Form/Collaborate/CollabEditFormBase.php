<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Form\Collaborate\CollabEditFormBase.
 */

namespace Drupal\sxt_opentalk\Form\Collaborate;

use Drupal\sxt_slogitem\Form\XtsiFormBase;
use Drupal\sxt_opentalk\XtotCollaborateFormTrait;

/**
 * Provides a ... form.
 */
abstract class CollabEditFormBase extends XtsiFormBase {

  use XtotCollaborateFormTrait;

  public function __construct() {
    $this->setCollabNodeUserData();
  }

}
