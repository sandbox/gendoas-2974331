<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\XtotDevGenerateTrait.
 */

namespace Drupal\sxt_opentalk;

use Drupal\sloggen\SlogGen;

/**
 * A Trait for ...
 */
trait XtotDevGenerateTrait {

  protected function canDevGenerate() {
    if (\Drupal::moduleHandler()->moduleExists('sloggen') && !empty($this->has_node_state)) {
      $config = \Drupal::config('sloggen.settings');
      $integrate = (boolean) (SlogGen::configValue($config, 'sloggen_integrate', FALSE));
      return $integrate;
    }
    
    return FALSE;
  }

}
