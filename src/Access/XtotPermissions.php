<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Access\XtotPermissions.
 */

namespace Drupal\sxt_opentalk\Access;

use Drupal\slogtx\SlogTx;
use Drupal\taxonomy\TaxonomyPermissions;

/**
 * Provides dynamic permissions of the sxt_opentalk module.
 *
 * @see sxt_opentalk.permissions.yml
 */
class XtotPermissions extends TaxonomyPermissions {

  /**
   * Get slog taxonomy permissions.
   *
   * @return array
   *   Permissions array.
   */
  public function permissions() {
    $permissions = [
      "administer xtot collaborate" => ['title' => $this->t('Administer all collaboration')],
    ];
    // toolbars: administer only
    $toolbars = SlogTx::getToolbarsSorted(TRUE);
    foreach ($toolbars as $toolbar) {
      if (!$toolbar->isUnderscoreToolbar()) {
        $toolbar_id = $toolbar->id();
        $disabled = $toolbar->status() ? '' : ' - disabled';
        $args = ['%toolbar_id' => $toolbar->id(), '%toolbar' => $toolbar->label(), '%disabled' => $disabled];
        $permissions += [
          "administer collaborate $toolbar_id" => ['title' => $this->t('Administer collaborate %toolbar_id (%toolbar)%disabled', $args)],
        ];
      }
    }
    return $permissions;
  }

}
