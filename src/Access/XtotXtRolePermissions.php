<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Access\XtotXtRolePermissions.
 */

namespace Drupal\sxt_opentalk\Access;

/**
 * Provides permissions for Xt-Roles.
 */
class XtotXtRolePermissions {

  /**
   * Returns an array of Xtot Xt-Role permissions.
   *
   * @return array
   *   The Xt-Role permissions.
   *   @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function getXtRolePermissions() {
    $perms = [
      'admin sxtrole collaborate' => [
        'title' => 'Administer Xt-Role collaborate',
        'description' => 'Permission for administering Xt-Role collaboration.',
        'warning' => 'Warning: This should only be granted to power users.',
      ],
    ];

    return $perms;
  }

}
