<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\XtotCompetitionFormTrait.
 */

namespace Drupal\sxt_opentalk;

use Drupal\sxt_workflow\SlogXtwf;

/**
 * A Trait for ...
 */
trait XtotCompetitionFormTrait {

  protected function isAdminFinalize() {
    return FALSE;
  }

  protected function getOptions(array $competition, array $labels) {
    $options = [];
    $best_id = $competition['preferred']['best']['id'];
    $next_id = $competition['preferred']['next']['id'];
    $options[$best_id] = $labels[$best_id];
    $options[$next_id] = $labels[$next_id];
    
    return $options;
  }

  protected function addFieldWorkflow(array &$form, array $competition, $add_extra = FALSE) {
    $node_state = $this->node_state;
    $wf_labels = $node_state->getWorkflowLabels();
    $best_id = $competition['preferred']['best']['id'];
    $options = $this->getOptions($competition, $wf_labels);
    $target = 'workflow';
    if ($this->isAdminFinalize()) {
      $winner_data = $node_state->getData('state_data')['winner_data'];
      $default_id = ($winner_data[$target] ?? $best_id);
    } else {
      $user_data = $node_state->getUserData($this->user_id) ?? [];
      $default_id = ($user_data[$target] ?? $best_id);
    }

    $form[$target] = [
      '#type' => 'select',
      '#title' => t('Workflow'),
      '#description' => $competition['description'],
      '#options' => $options,
      '#default_value' => $default_id,
      '#required' => TRUE,
      '#attributes' => [
        'id' => "xtot-$target",
      ],
        ] + $this->getInputFieldWrapper();

    if ($add_extra) {
      $c_description = $competition['description'];
      $workflows = $node_state->getWorkflows();
      foreach ($workflows as $workflow_id => $workflow) {
        if (isset($options[$workflow_id])) {
          $label = $workflow->label();
          $type_settings = $workflow->getPluginCollections()['type_settings'];
          $description = $type_settings->getConfiguration()['description'] ?? '_???_addFieldWorkflow';
          $form["xtot-workflow-$workflow_id"] = [
            '#type' => 'hidden',
            '#value' => "$c_description<br />$label: $description",
            '#attributes' => [
              'id' => "xtot-workflow-$workflow_id",
            ],
          ];
        }
      }
    }
  }

  protected function addFieldSeverity(array &$form, array $competition) {
    $node_state = $this->node_state;
    $sv_labels = SlogXtwf::getStateSeverityOptions();
    $best_id = $competition['preferred']['best']['id'];
    $options = $this->getOptions($competition, $sv_labels);
    $target = 'severity';
    if ($this->isAdminFinalize()) {
      $winner_data = $node_state->getData('state_data')['winner_data'];
      $default_id = ($winner_data[$target] ?? $best_id);
    } else {
      $user_data = $node_state->getUserData($this->user_id) ?? [];
      $default_id = ($user_data[$target] ?? $best_id);
    }
    $c_description = $competition['description'];
    $description = t('Severity levels differ in the required items to collect and the timeout.');

    $form[$target] = [
      '#type' => 'select',
      '#title' => t('Severity'),
      '#description' => "$c_description<br />$description",
      '#options' => $options,
      '#default_value' => $default_id,
      '#required' => TRUE,
      '#attributes' => [
        'id' => "xtot-$target",
      ],
        ] + $this->getInputFieldWrapper();
  }

  protected function addFieldRenewId(array &$form, array $competition, $is_admin = FALSE) {
    $node_state = $this->node_state;
    $rid_labels = $node_state->getOptionsRequestRenew();
    $best_id = $competition['preferred']['best']['id'];
    $options = $this->getOptions($competition, $rid_labels);
    $target = 'renew_id';
    if ($this->isAdminFinalize()) {
      $winner_data = $node_state->getData('state_data')['winner_data'];
      $default_id = ($winner_data[$target] ?? $best_id);
    } else {
      $user_data = $node_state->getUserData($this->user_id) ?? [];
      $default_id = ($user_data[$target] ?? $best_id);
    }
    $c_description = $competition['description'];
    if ($is_admin) {
      $description = t('Select the action or select the state to continue with.');
    } else {
      $description = t('Vote for action or vote for the state to continue with.');
    }

    $form[$target] = [
      '#type' => 'select',
      '#title' => t('Next'),
      '#description' => "$c_description<br />$description",
      '#options' => $options,
      '#default_value' => $default_id,
      '#required' => TRUE,
      '#attributes' => [
        'id' => "xtot-$target",
      ],
        ] + $this->getInputFieldWrapper();
  }

}
