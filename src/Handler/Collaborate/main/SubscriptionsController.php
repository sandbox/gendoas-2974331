<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\main\SubscriptionsController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\main;

use Drupal\sxt_opentalk\SlogXtot;
use Drupal\slogxt\SlogXt;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\Controller\XtEditControllerBase;

/**
 * Defines a controller ....
 */
class SubscriptionsController extends XtEditControllerBase {

  /**
   * {@inheritdoc}
   */
  protected function getFormObjectArg() {
    return 'Drupal\slogxt\Form\EmptyCheckboxesForm';
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return t('Subscriptions');
  }

  /**
   * {@inheritdoc}
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);

    $items = [];
    $uid = \Drupal::currentUser()->id();
    $keyed_sids = SlogXtot::getSubscribedSids($uid, TRUE);
    $keyed_nids = array_flip($keyed_sids);
    $items_data = SlogXtsi::getContentAttachDataBySids(array_values($keyed_sids));

    if (!empty($keyed_nids)) {
      $xtwf_node_states = \Drupal::entityTypeManager()
          ->getStorage('xtwf_node_state')
          ->loadMultiple(array_values($keyed_nids));
    }

    foreach ($items_data as $key => $item) {
      $li_details = '_???_' . $item['data']['byEntity'];
      $nid = isset($keyed_nids[$key]) ? $keyed_nids[$key] : FALSE;
      if ($nid && $xtwf_node_states[$nid]) {
        $li_details = $xtwf_node_states[$nid]->getStateInfo(TRUE, TRUE);
      }

      $item['data']['liDetails'] = $li_details;
      $items[] = $item['data'];
    }

    $slogxtData = & $form_state->get('slogxtData');
    $slogxtData['isLastPage'] = TRUE;
    $slogxtData['runCommand'] = 'wizardFormEdit';
    $hasItems = (count($items) > 0);
    $args = [
      'items' => $items,
      'hasItems' => $hasItems,
      'radios' => FALSE,
      'selIdx' => 'current',
    ];
    if (!$hasItems) {
      $msg = t('There are no subscriptions yet.');
      $args['notice'] = SlogXt::htmlMessage($msg, 'warning');
    }
    $slogxtData['alterForm'] = [
      'command' => 'sxt_slogitem::alterFormAddList',
      'args' => $args,
    ];
  }

}
