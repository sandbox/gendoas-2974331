<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\main\ActionsCollabMainController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\main;

use Drupal\slogxt\Controller\XtEditControllerBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class ActionsCollabMainController extends XtEditControllerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    return 'Drupal\sxt_opentalk\Form\Collaborate\main\ActionsSelectForm';
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return t('Select action');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    return ['slogxtData' => $form_state->get('slogxtData')];
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::appendAttachments();
   */
  protected function appendAttachments() {
    return TRUE;
  }

}
