<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\CollabCCollectController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content;

use Drupal\sxt_opentalk\Handler\Collaborate\content\CollabCCollectControllerBase;

/**
 * Defines a controller for deleting user's collaboration request.
 */
class CollabCCollectController extends CollabCCollectControllerBase {
  
}
