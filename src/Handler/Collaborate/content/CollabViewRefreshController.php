<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\CollabViewRefreshController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content;

//use Drupal\slogxt\SlogXt;
//use Drupal\sxt_workflow\SlogXtwf;
//use Drupal\node\Entity\Node;
//use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\Controller\XtEditControllerBase;

/**
 * Defines a controller ....
 */
class CollabViewRefreshController extends XtEditControllerBase {

//  protected $node;
//  protected $workflow;
//  protected $on_select = FALSE;
//  protected $on_change = FALSE;
//  protected $node_state = FALSE;

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $request = \Drupal::request();
//    $this->node = $node = Node::load($request->get('base_entity_id'));
//    $node_id = $node->id();
//    $request->attributes->set('node', $this->node);



    return SlogXt::arrangeUrgentMessageForm();
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return t('Subscribe request');
  }

//  protected function getSubmitLabel() {
//    return $this->t('Save');
//  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
//  protected function buildContentResult(&$form, FormStateInterface $form_state) {
//  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
//              protected function getOnWizardFinished() {
//              }
}
