<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\CollabCompetitionController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content;

use Drupal\sxt_opentalk\SlogXtot;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_opentalk\XtotConfirmControllerBase;
use Drupal\sxt_opentalk\XtotCompetitionFormTrait;

/**
 * Defines a controller for deleting user's collaboration request.
 */
class CollabCompetitionController extends XtotConfirmControllerBase {

  use XtotCompetitionFormTrait;

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return t('Vote');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    parent::hookFormAlter($form, $form_state, $form_id);

    $node_state = $this->node_state;
    $competitions = $node_state->getCompetitionData('competitions') ?? [];
    foreach ($competitions as $competition) {
      $target = $competition['target'];
      if ($target === 'renew_id') {
        $this->addFieldRenewId($form, $competition);
      }
      elseif ($target === 'workflow') {
        $this->addFieldWorkflow($form, $competition, TRUE);
      }
      elseif ($target === 'severity') {
        $this->addFieldSeverity($form, $competition);
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $this->makeMsgClosable($form);
    $msg = $this->node_state->getStateInfo(TRUE, TRUE);
    $warn_only_msg = '';
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['alterForm'] = [
      'command' => 'sxt_opentalk::alterFormCollaborate',
    ];
    if ($this->has_user_data) {
      $warn_only_msg = $this->htmlHrPlus() . t('You already voted for this competition. You can change it now.');
    }
    else {
      $warn_only_msg = $this->htmlHrPlus() . t('Do your initial vote for this competition.');
    }
    $slogxtData['message'] = ['msg' => $msg];

    $this->setPreFormMessage($msg, $form_state, $warn_only_msg);
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function formValidate(array &$form, FormStateInterface $form_state) {
    parent::formValidate($form, $form_state);
    self::calledObject()->formValidateHasChanged($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    $node_state = $calledObject->node_state;
    $user_data = [];
    foreach (['renew_id', 'workflow', 'severity'] as $target) {
      $value = $form_state->getValue($target);
      if (!empty($value)) {
        $user_data[$target] = $value;
      }
    }

    $node_state->setUserData($calledObject->user_id, $user_data)->save();

    // check whether to finalize by required posts
    if ($result = $node_state->checkFinalizeByRequired()) {
      $success = (boolean) $result['success'];
      if (!empty($result['message'])) {
        $calledObject->addFinalMoreMessage($result['message'], $success);
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    drupal_get_messages();  // clear messages
    $this->addFinalMoreMessage(t('Your vote has been saved.'));
    $this->setFinalMoreMessages();

    return [
      'command' => 'sxt_opentalk::finishedWorkflowChanged',
      'args' => SlogXtot::getNodeStateRefreshArgs($this->node_id),
    ];
  }

}
