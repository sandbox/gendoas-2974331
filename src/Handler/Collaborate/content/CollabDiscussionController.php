<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\CollabDiscussionController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content;

use Drupal\sxt_opentalk\SlogXtot;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\sxt_opentalk\XtotCollaborateTrait;
use Drupal\sxt_slogitem\Handler\Other\MwCommentEditActions;

/**
 * Defines a controller for deleting user's collaboration request.
 */
class CollabDiscussionController extends MwCommentEditActions {

  use XtotCollaborateTrait;

  protected $discuss_objects = [];
  protected $base_node_id = 0;
  protected $discussion_node = FALSE;
  protected $has_discuss_node = FALSE;
  protected $urgent_err_data = FALSE;

  public function __construct(ModuleHandlerInterface $module_handler, RendererInterface $renderer, ClassResolverInterface $class_resolver, EventDispatcherInterface $dispatcher) {
    $this->moduleHandler = $module_handler;
    $this->renderer = $renderer;
    $this->classResolver = $class_resolver;
    $this->dispatcher = $dispatcher;

    $this->setCollabNodeUserData();
    $this->discuss_objects = $objects = SlogXtot::getMwDiscussionObjects($this->node_state, FALSE);
    $this->discussion_node = $objects['discussion_node'];
    $this->has_discuss_node = !empty($this->discussion_node);
    $this->base_node_id = $this->node_state->getNode()->id();
  }

  protected function prepareFormObjectArg() {
    $request = \Drupal::request();

    if (!$this->has_discuss_node) {
      $this->discuss_objects = $objects = SlogXtot::getMwDiscussionObjects($this->node_state, TRUE);
      $this->discussion_node = $objects['discussion_node'];
      $this->has_discuss_node = !empty($objects['discussion_node']);
      if (!$this->has_discuss_node) {
        $this->urgent_err_data = [
          'message' => t('Error on creating discussion structure.'),
          'type' => 'error',
        ];
      }
      else {
        $request->attributes->set('node_id', $this->discussion_node->id());    // discussion node id !!!!
        $request->attributes->set('mwc_action', 'addnew');
        $request->attributes->set('comment_id', 0);
      }
    }
    else {
      $request->attributes->set('node_id', $this->discussion_node->id());    // discussion node id !!!!
      $request->attributes->set('mwc_action', 'addnew');
      $request->attributes->set('comment_id', 0);
    }

    parent::prepareFormObjectArg();
  }

  /**
   * {@inheritdoc}
   */
  protected function postBuildForm(&$form, FormStateInterface $form_state) {
    if (!empty($this->urgent_err_data)) {
      $slogxt = & $form_state->get('slogxt');
      $slogxt['urgentMsgData'] = $this->urgent_err_data;
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    // there ist first comment only in this controller
    return t('Add first comment: ') . $this->node->label();
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    $result = parent::getOnWizardFinished();
    $result['command'] = 'sxt_opentalk::createdFirstDiscussComment';

    $route_name = 'sxt_opentalk.collaborate.c.viewdiscussion';
    $route_parameters = ['node_id' => $this->base_node_id];
    $result['args']['path'] = $this->getPathFromRoute($route_name, $route_parameters);
    $result['args']['base_node_id'] = $this->base_node_id;

    return $result;
  }

}
