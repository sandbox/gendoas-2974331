<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\RequestFinishFinallyController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_opentalk\XtotConfirmControllerBase;
use Drupal\sxt_opentalk\XtotRequestFormTrait;

/**
 * Defines a controller ....
 */
class RequestFinishFinallyController extends XtotConfirmControllerBase {

  use XtotRequestFormTrait;

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    if ($this->is_ff_request) {
      $title = t('Vote for finish finally');
    }
    else {
      $title = t('Create request for finish finally');
    }
    return $title;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getSubmitLabel();
   */
  protected function getSubmitLabel() {
    if ($this->is_ff_request) {
      $label = t('Save vote');
    }
    else {
      $label = t('Create request');
    }
    return $label;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    parent::hookFormAlter($form, $form_state, $form_id);

    $node_state = $this->node_state;
    $this->is_ff_request = $node_state->isStateRequestFinishFinally();
    if ($this->is_ff_request) {
      $options = [];
      if ($node_state->canDiscussRequest()) {
        $options[SlogXtwf::XTWF_DISCUSSION] = SlogXtwf::getTxtLetsDiscuss();
      } 
      $options += [
        SlogXtwf::XTWF_SELECT_NO => (string) t('No, do NOT finish finally'),
        SlogXtwf::XTWF_SELECT_YES => (string) t('Yes, do finish collaboration finally'),
      ];
      $form['vote'] = [
        '#type' => 'select',
        '#title' => t('Your vote'),
        '#description' => t('Vote for or against permanent termination of collaboration, or vote for a discussion.'),
        '#options' => $options,
        '#required' => TRUE,
          ] + $this->getInputFieldWrapper();

      if ($this->has_user_data) {
        $user_data = $node_state->getUserData($this->user_id) ?? [];
        $form['vote']['#default_value'] = $user_data['vote'] ?? '';
      }
    }
    else {
      $this->addFieldNotice($form);
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $this->makeMsgClosable($form);
    $msg = $this->node_state->getStateInfo(TRUE, TRUE);
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['alterForm'] = [
      'command' => 'sxt_opentalk::alterFormCollaborate',
    ];

    $warning = t('INFO: Finally finished collaboration is permanent. There will be no more collaboration for this content.');
    if ($this->is_ff_request) {
      if ($this->has_user_data) {
        $action_info = t('Change your vote for finish finally');
      }
      else {
        $action_info = t('Cast your vote for finish finally');
      }
    }
    else {
      $action_info = t('You are creating a request for terminating collaboration permanently.');
    }

    $warn_only_msg = $this->htmlHrPlus() . "$action_info<br />$warning";
    $this->setPreFormMessage($msg, $form_state, $warn_only_msg);
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function formValidate(array &$form, FormStateInterface $form_state) {
    parent::formValidate($form, $form_state);
    self::calledObject()->formValidateHasChanged($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    $node_state = $calledObject->node_state;
    $success = TRUE;
    $vote = $form_state->getValue('vote');

    if (!$calledObject->is_ff_request) {
      $vote = SlogXtwf::XTWF_SELECT_YES;
      $notice = $form_state->getValue('notice');
      $success = $node_state->initializeRequest(SlogXtwf::XTWF_REQUEST_FINISH_FINALLY, $notice);
    }

    if ($success) {
      $user_data = ['vote' => $vote];
      $node_state->setUserData($calledObject->user_id, $user_data)->save();
    }
  }

}
