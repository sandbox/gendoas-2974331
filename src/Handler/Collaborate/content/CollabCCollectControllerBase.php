<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\CollabCCollectControllerBase.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content;

use Drupal\sxt_opentalk\SlogXtot;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogxt\SlogXt;
use Drupal\sxt_slogitem\Entity\SlogItem;
use Drupal\node\Entity\Node;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_slogitem\Handler\XtsiEdit\xtsiContent\NodeEditControllerBase;
use Drupal\sxt_opentalk\XtotCollaborateTrait;

/**
 * Defines a controller for ...
 */
class CollabCCollectControllerBase extends NodeEditControllerBase {

  use XtotCollaborateTrait;

  protected $action;
  protected $is_new;
  protected $is_on_select_action = FALSE;
  protected $shared_timestamp = 0;
  protected $slogitem = FALSE;
  protected $content_nid = FALSE;
  protected $content_node = FALSE;
  protected $content_saved_title = FALSE;
  protected $collect_menu_tid;

  protected function actionsAvailable() {
    return ['view', 'edit', 'delete', 'share'];
  }

  protected function isOnCreate() {
    return ($this->action === 'edit' && $this->is_new);
  }

  protected function isOnSelect() {
    return $this->is_on_select_action;
  }

  protected function isOnEdit() {
    return ($this->action === 'edit');
  }

  protected function isOnDelete() {
    return ($this->action === 'delete');
  }

  protected function isOnShare() {
    return ($this->action === 'share');
  }

  protected function isOnView() {
    return ($this->action === 'view');
  }

  protected function isOnSharedView() {
    return ($this->action === 'view' && !empty($this->shared_timestamp));
  }

  /**
   * 
   * @return boolean
   */
  protected function initVars() {
    $this->setCollabNodeUserData();
    if (!$this->has_node_state || !$this->node_state->stateIsCollectContentType()) {
      return FALSE;
    }

    $action = \Drupal::request()->get('action');
    $node_state = $this->node_state;
    if ($this->has_user_data) {
      $user_data = $node_state->getUserData($this->user_id) ?? [];
      if (!$this->slogitem = SlogXtsi::getSlogitem($user_data['sid'])) {
        return FALSE;
      }

      if (!empty($user_data['shared'])) {
        $action = 'view';
        $this->shared_timestamp = (integer) $user_data['shared'];
      }
    }

    $this->is_new = !$this->has_user_data;
    $available = $this->actionsAvailable();
    if (!in_array($action, $available)) {
      if ($this->is_new) {
        $action = 'edit';
      }
      else {
        $this->is_on_select_action = TRUE;
      }
    }
    $this->action = $action;

    if ($this->has_user_data) {
      if (!$content_nid = (integer) ($user_data['nid'] ?? 0)) {
        return FALSE;
      }

      if (!$this->content_node = Node::load($content_nid)) {
        return FALSE;
      }

      $this->content_saved_title = $this->content_node->label();
    }
    else {
      $node_type = 'media_wiki';
      $this->content_node = $this
          ->entityTypeManager()
          ->getStorage('node')
          ->create(['type' => $node_type]);
    }

    return TRUE;
  }

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    if (!$this->initVars()) {
      return SlogXt::arrangeUrgentMessageForm(t('Init error occured.'), 'error');
    }

    if ($this->isOnView()) {
      return 'Drupal\slogxt\Form\XtGenericConfirmForm';
    }
    elseif ($this->isOnSelect()) {
      return 'Drupal\sxt_opentalk\Form\Collaborate\SelectCollectActionForm';
    }
    elseif ($this->isOnEdit()) {
      $this->isNodeEdit = true;
      $this->createNew = $this->isOnCreate();
      return $this->getEntityFormObject('node.default', $this->content_node);
    }
    else {
      return 'Drupal\slogxt\Form\XtGenericConfirmForm';
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    $title = t('..collectTitle');
    if ($this->isOnSharedView()) {
      $title = t('View your shared post');
    }
    elseif ($this->isOnView()) {
      $title = t('View your post');
    }
    elseif ($this->isOnSelect()) {
      $title = t('Select action');
    }
    elseif ($this->isOnCreate()) {
      $title = t('Create content');
    }
    elseif ($this->isOnEdit()) {
      $title = t('Edit content');
    }
    elseif ($this->isOnDelete()) {
      $title = t('Delete content');
    }
    elseif ($this->isOnShare()) {
      $title = t('Share content');
    }
    return $title;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getSubmitLabel();
   */
  protected function getSubmitLabel() {
    if ($this->isOnSelect()) {
      $label = t('Next');
    }
    elseif ($this->isOnEdit()) {
      if ($this->formResult === 'edit') {
        $label = t('Preview');
      }
      elseif ($this->isOnCreate()) {
        $label = t('Create');
      }
      else {
        $label = t('Update');
      }
    }
    elseif ($this->isOnDelete()) {
      $label = t('Delete');
    }
    elseif ($this->isOnShare()) {
      $label = t('Share');
    }
    else {
      $label = t('Next');
    }

    return $label;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    if ($this->isOnDelete() || $this->isOnShare()) {
      // add callbacks
      $controllerClass = get_class($this);
      $form['#validate'][] = "$controllerClass::formValidate";
    }
    else {
      parent::hookFormAlter($form, $form_state, $form_id);
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preBuildForm();
   */
  protected function preBuildForm(FormStateInterface $form_state) {
    if ($this->isOnEdit()) {
      parent::preBuildForm($form_state);
    }
    else {
      $form_state->set('response', false);  // prevent redirection after submission
      $form_state->set('slogxt', ['ajax' => true]); // more slogxt infos, server side
      $form_state->set('slogxtData', ['runCommand' => 'default']); // more slogxt infos, client side
      $op = \Drupal::request()->get('op', false);
      $this->opSave = ($op && $op === (string) t('Save'));
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preRenderForm();
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    if ($this->isOnEdit()) {
      parent::preRenderForm($form, $form_state);
    }
    else {
      $this->preRenderFormBase($form, $form_state);
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    if ($this->isOnView()) {
      $result = $this->slogitem->ajaxExecute();
      $header = $result['header']['mainLabel'] ?? '??header';
      $content = $result['content'] ?? '??content';
      $slogxtData['htmlContent'] = $this->renderedDialogViewContent($header, $content);
      $slogxtData['runCommand'] = 'viewcontent';
      $slogxtData['isLastPage'] = TRUE;

      return parent::_doBuildDialogViewContentResult($form, $form_state);
    }
    elseif ($this->isOnSelect()) {
      $slogxtData['runCommand'] = 'radios';
    }
    else {
      $slogxtData['runCommand'] = 'wizardFormEdit';
      if ($this->opSave && !$form_state->hasAnyErrors()) {
        $slogxtData['wizardFinished'] = true;
        $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
      }
      if ($this->isOnDelete() || $this->isOnShare()) {
        $msg = $this->node_state->getStateInfo(TRUE, TRUE);
        if ($this->isOnDelete()) {
          $warn_only_msg = t('You are about to delete your post.');
        }
        else {
          $warn_only_msg = t('You are about to share your post.');
          $warn_only_msg .= '<br />' . t('NOTE: You can no longer change the content after sharing.');
        }

        $warn_only_msg .= '<br />' . t('WARNING: this action cannot be undone.');
        $warn_only_msg = $this->htmlHrPlus() . $warn_only_msg;
        $this->setPreFormMessage($msg, $form_state, $warn_only_msg);
      }
    }
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * Callback
   */
  public static function formValidate(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    if ($calledObject->isOnEdit()) {
      parent::formValidate($form, $form_state);

      if (!$form_state->getErrors()) {
        $node_state = $calledObject->node_state;
        $node_id = $calledObject->node_id;
        $menu_label = $node_state->buildNodeTitleByState();
        $menu_term = SlogXtsi::getMwCollectBaseDraftMenuTerm($node_id, $menu_label, TRUE);
        if (empty($menu_term)) {
          $form_state->setErrorByName('none', t('Container for collection is not available.'));
        }
        else {
          $calledObject->collect_menu_tid = $menu_term->id();
        }
      }
    }
    else {
      $submit_handlers = &$form_state->getTriggeringElement()['#submit'];
      $submit_handlers[] = [get_class(self::calledObject()), 'formSubmit'];
      $form_state->setSubmitHandlers($submit_handlers);
    }

//    $form_state->setErrorByName('title', '....CollabCCollectControllerBase');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::save();
   */
  public static function save(array $form, FormStateInterface $form_state) {
    // create, edit
    parent::save($form, $form_state);

    $calledObject = self::calledObject();
    $node_state = $calledObject->node_state;
    $user_id = $calledObject->user_id;
    $user_data = $node_state->getUserData($user_id) ?? [];

    $content_nid = $calledObject->savedValues['nid'];
    $node = Node::load($content_nid);
    $content_title = $node->label();
    $saved_title = (string) $calledObject->content_saved_title;

    if ($calledObject->isOnCreate()) {
      // create new slog item
      $slogitem = SlogItem::create([
            'tid' => $calledObject->collect_menu_tid,
            'title' => $content_title,
            'status' => 1,
            'weight' => REQUEST_TIME,
            'created' => REQUEST_TIME,
      ]);
      $slogitem->setRouteData('node', $content_nid)->save();

      $user_data = [
        'nid' => $content_nid,
        'sid' => $slogitem->id(),
        'shared' => 0,
      ];
      $node_state->setUserData($user_id, $user_data)->save();
      $msg = $node_state->getStateInfo(TRUE, TRUE);
      $calledObject->setPreFormMessage($msg, $form_state);
      $calledObject->addFinalMoreMessage(t('Your post has been saved.'));
    }
    else {
      if ($content_title !== $saved_title && $calledObject->slogitem) {
        $calledObject->slogitem->setLabel($content_title)->save();
      }
      $calledObject->addFinalMoreMessage(t('Your post has been updated.'));
    }
  }

  /**
   * Callback
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    // share, delete
    $calledObject = self::calledObject();
    $node_state = $calledObject->node_state;

    if ($calledObject->isOnDelete()) {
      $node_state->deleteUserData($calledObject->user_id);
      $calledObject->addFinalMoreMessage(t('Your post has been deleted.'));
    }
    elseif ($calledObject->isOnShare()) {
      $success = $node_state->shareUserData($calledObject->user_id);
      if ($success) {
        $calledObject->addFinalMoreMessage(t('Your post has been shared.'));
      }
      else {
        $calledObject->addFinalMoreMessage(t('An error occured on sharing your post.'), FALSE);
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    drupal_get_messages();  // clear messages
    $this->setFinalMoreMessages();

    return [
      'command' => 'sxt_opentalk::finishedWorkflowChanged',
      'args' => SlogXtot::getNodeStateRefreshArgs($this->node_id),
    ];
  }

}
