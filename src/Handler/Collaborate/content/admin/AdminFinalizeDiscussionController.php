<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\admin\AdminFinalizeDiscussionController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content\admin;

/**
 * Defines a controller ....
 */
class AdminFinalizeDiscussionController extends AdminFinalizeControllerBase {

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return $this->t('Finalize discussion');
  }

  protected function getBuildWarning() {
    return t('You are about to finalize the discussion.');
  }

  protected function getFinishedMsg() {
    return t('Discussion has been finalized.');
  }

}
