<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\admin\AdminFinalizeControllerBase.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content\admin;

use Drupal\sxt_opentalk\SlogXtot;
use Drupal\sxt_workflow\SlogXtwf;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_opentalk\XtotConfirmControllerBase;
use Drupal\sxt_opentalk\XtotRequestFormTrait;

/**
 * Defines a controller ....
 */
abstract class AdminFinalizeControllerBase extends XtotConfirmControllerBase {

  use XtotRequestFormTrait;

  abstract protected function getFinishedMsg();

  protected function isAdminFinalize() {
    return TRUE;
  }

  protected function addFinalizeByField(array &$form, $default_value = '') {
    if (empty($default_value)) {
      $default_value = $this->node_state->getFinalizeByTimeoutKey();
    }
    $description = t('Set finalize setting for the next state.');
    $this->addFieldFinalizeBy($form, $default_value, $description);
  }

  protected function getNotizDescription() {
    return t('Specify a reason for finalizing the state.');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    parent::hookFormAlter($form, $form_state, $form_id);
    $description = $this->getNotizDescription();;
    $this->addFieldAdminNotice($form, $description);
  }

  protected function getBuildWarning() {
    return '';
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $this->makeMsgClosable($form);
    $msg = $this->node_state->getStateInfo(TRUE, TRUE);
    $warn_only_msg = $this->getBuildWarning();
    if (!empty($warn_only_msg)) {
      $warn_only_msg = $this->htmlHrPlus() . $warn_only_msg;
    }
    $this->setPreFormMessage($msg, $form_state, $warn_only_msg);
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    drupal_get_messages();  // clear messages
    $this->addFinalMoreMessage($this->getFinishedMsg());
    $this->setFinalMoreMessages();

    return [
      'command' => 'sxt_opentalk::finishedWorkflowChanged',
      'args' => SlogXtot::getNodeStateRefreshArgs($this->node_id),
    ];
  }

  /**
   * Provide submit form with args for finalizing state.
   */
  protected function getFinalizeArgs($values, $action = 'finalize') {
    $calledObject = self::calledObject();
    $node_state = $calledObject->node_state;

    $finalize_by_timeout = (boolean) $node_state->getData('finalize_by_timeout');
    if (isset($values['finalize'])) {
      $finalize_by_timeout = $node_state->getFinalizeByTimeoutFlag($values['finalize']);
    }
    $finalize_strict = (boolean) $node_state->getData('finalize_strict');
    if (isset($values['strict'])) {
      $finalize_strict = $node_state->getFinalizeStrictFlag($values['strict']);
    }
    $args = [
      'action' => $action,
      'finalize_by_timeout' => $finalize_by_timeout,
      'finalize_strict' => $finalize_strict,
    ];
    
    if (!empty($values['admin_notice'])) {
      $args['admin_data'] = [
        'admin_action' => $action,
        'admin_notice' => (string) ($values['admin_notice']),
      ];
    }
    
    return $args;
  }

  /**
   * {@inheritdoc}
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    $args = $calledObject->getFinalizeArgs($form_state->getValues());

    $node_state = $calledObject->node_state;
    if ($result = $node_state->stateFinalizeByAdmin($args)) {
      $success = (boolean) $result['success'];
      $is_terminate = ($args['action'] === 'terminate');
      if ($success && $is_terminate && $calledObject->is_ff_request) {
        $result['message'] = FALSE;
      }
      if (!empty($result['message'])) {
        $calledObject->addFinalMoreMessage($result['message'], $success);
      }
    }
  }

}
