<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\admin\AdminLetsDiscussController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content\admin;

use Drupal\sxt_opentalk\SlogXtot;
use Drupal\slogxt\SlogXt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_opentalk\XtotConfirmControllerBase;

/**
 * Defines a controller ....
 */
class AdminLetsDiscussController extends XtotConfirmControllerBase {

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return t("Let's discuss");
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    parent::hookFormAlter($form, $form_state, $form_id);
    $description = t('Specify a reason for discussing request.');
    $this->addFieldAdminNotice($form, $description);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $this->makeMsgClosable($form);
    $msg = $this->node_state->getStateInfo(TRUE, TRUE);
    $warning = t('You are about to suspend voting and open a discussion.');
    $warn_only_msg = $this->htmlHrPlus() . $warning;
    $this->setPreFormMessage($msg, $form_state, $warn_only_msg);
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    $args = [
      'action' => 'suspend',
      'discuss_first' => TRUE,
      'admin_data' => [
        'admin_action' => 'suspend',
        'admin_notice' => (string) $form_state->getValue('admin_notice'),
      ],
    ];

    $node_state = $calledObject->node_state;
    if ($result = $node_state->stateFinalizeByAdmin($args)) {
      $calledObject->success = $success = (boolean) $result['success'];
      if ($success) {
        $msg = t('Request has been suspended for discussion');
        $calledObject->addFinalMoreMessage($msg, $success);
      }
      elseif (!empty($result['message'])) {
        $calledObject->addFinalMoreMessage($result['message'], $success);
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    drupal_get_messages();  // clear messages
    if (!$this->success) {
      $this->addFinalMoreMessage(SlogXt::txtSeeLogMessages(), FALSE);
    }
    $this->setFinalMoreMessages();

    return [
      'command' => 'sxt_opentalk::finishedWorkflowChanged',
      'args' => SlogXtot::getNodeStateRefreshArgs($this->node_id),
    ];
  }

}
