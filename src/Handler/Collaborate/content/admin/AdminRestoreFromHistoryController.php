<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\admin\AdminRestoreFromHistoryController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content\admin;

use Drupal\sxt_opentalk\SlogXtot;
use Drupal\slogxt\SlogXt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_opentalk\XtotConfirmControllerBase;
use Drupal\sxt_opentalk\XtotRequestFormTrait;

/**
 * Defines a controller ....
 */
class AdminRestoreFromHistoryController extends XtotConfirmControllerBase {

  use XtotRequestFormTrait;

  protected $success = FALSE;
  protected $history_key = 0;
  protected $has_severity = FALSE;
  protected $xtwf_state = FALSE;
  protected $selected_info = '_???_selected_info';
  protected $selected_line = '_???_selected_line';

  protected function getFormObjectArg() {
    $this->history_key = (integer) \Drupal::request()->get('history_key');
    if (!$this->history_key) {
      return '\Drupal\sxt_opentalk\Form\Collaborate\content\SelectHistoryForm';
    }

    return parent::getFormObjectArg();
  }

  protected function getFormTitle() {
    if ($this->history_key) {
      return t('Restore from history');
    }
    else {
      return t('Select history');
    }
  }

  protected function getSubmitLabel() {
    if ($this->history_key) {
      return t('Restore');
    }
    else {
      return t('Next');
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    $node_state = $this->node_state;
    if (!$this->history_key) {
      // do nothing
      return;
    }
    $history_data = $node_state->getHistoryData($this->history_key);
    if (empty($history_data)) {
      // do nothing
      return;
    }

    // alter form
    parent::hookFormAlter($form, $form_state, $form_id);

    $form['history_key'] = [
      '#type' => 'hidden',
      '#value' => $this->history_key,
    ];

    $this->history_data = $history_data;
    $rollback_values = $node_state->getValues();

    $description = t('Specify a reason for restoring history.');
    $this->addFieldAdminNotice($form, $description);

    $node_state->setValues($history_data);
    if (!empty($history_data['state'])) {
      $this->xtwf_state = $xtwf_state = $node_state->getWfState($history_data['state']);
    }
    if ($xtwf_state && $xtwf_state->getUseAnySeverity()) {
      $this->has_severity = TRUE;
      $default = $node_state->getSeverityId();
      $description = t('Select the severity level for restored state.');
      $this->addFieldSeverity($form, $default, FALSE, $description);
      $finalize_id = $node_state->getFinalizeByTimeoutKey();
      $description = t('Select how to finalize the restored state.');
      $this->addFieldFinalizeBy($form, $finalize_id, $description);
      $strict_id = $node_state->getFinalizeStrictKey();
      $this->addFieldStrict($form, $strict_id);
    }
    $node_state->setValues($rollback_values);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    if (!$this->history_key) {
      $slogxtData = $form_state->get('slogxtData');
      $result = parent::buildContentResult($form, $form_state);
      $result['slogxtData'] = $slogxtData + $result['slogxtData'];
      return $result;
    }
    else {
      if (!$form_state->isSubmitted() || $form_state->hasAnyErrors()) {
        $this->prepareSelectedInfo();
        $msg = $this->selected_line . $this->htmlHrPlus('') . $this->selected_info;
        $warn1 = t('WARNING: you are about to restore selected state.');
        $warn2 = t('Be careful with this functionality.');
        $warn_only_msg = $this->htmlHrPlus() . "$warn1 $warn2";
        $this->setPreFormMessage($msg, $form_state, $warn_only_msg);
      }

      return parent::buildContentResult($form, $form_state);
    }
  }

  protected function prepareSelectedInfo() {
    $node_state = $this->node_state;
    if (!empty($this->history_data)) {
      $rollback_values = $node_state->getValues();

      $node_state->setValues($this->history_data);
      $this->selected_info = $this->node_state->getStateInfo(TRUE);
      $state_label = SlogXt::htmlHighlightText($this->xtwf_state->label());
      $date_formater = $this->getXtDateFormater();
      $f_changed = $date_formater->format($this->history_key, 'xt_date_only');
      $this->selected_line = (string) t('Selected state:') . " $state_label ($f_changed)";

      $node_state->setValues($rollback_values);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    $node_state = $calledObject->node_state;

    $values = $form_state->getValues();
    $args = [
      'action' => 'restore',
      'restore_history_key' => $values['history_key'],
      'admin_data' => [
        'admin_action' => 'restore',
        'admin_notice' => (string) $values['admin_notice'],
      ],
    ];
    if ($calledObject->has_severity) {
      $args += [
        'severity' => $values['severity'],
        'finalize_by_timeout' => $node_state->getFinalizeByTimeoutFlag($values['finalize']),
        'finalize_strict' => $node_state->getFinalizeStrictFlag($values['strict']),
      ];
    }

    if ($result = $node_state->restoreFromHistoryByAdmin($args)) {
      $calledObject->setCollabNodeUserData($reload = TRUE);
      $calledObject->success = $success = (boolean) $result['success'];
      $msg = $node_state->getStateInfo(TRUE, TRUE);
      $calledObject->prepareSelectedInfo();
      $msg = $calledObject->selected_line . $calledObject->htmlHrPlus() . $msg;
      $calledObject->setPreFormMessage($msg, $form_state);
      if (!empty($result['message'])) {
        $calledObject->addFinalMoreMessage($result['message'], $success);
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    drupal_get_messages();  // clear messages
    if (!$this->success) {
      $this->addFinalMoreMessage(SlogXt::txtSeeLogMessages(), FALSE);
    }
    $this->setFinalMoreMessages();

    return [
      'command' => 'sxt_opentalk::finishedWorkflowChanged',
      'args' => SlogXtot::getNodeStateRefreshArgs($this->node_id),
    ];
  }

}
