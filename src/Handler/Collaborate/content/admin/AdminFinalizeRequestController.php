<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\admin\AdminFinalizeRequestController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content\admin;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class AdminFinalizeRequestController extends AdminFinalizeControllerBase {

  protected function getBuildWarning() {
    return t('WARNING: This will finalize the request, bypassing the vote of the users.');
  }

  protected function getFinishedMsg() {
    if ($this->is_ff_request) {
      return t('Request has been canceled.');
    }
    else {
      return t('Request has been finalized.');
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return $this->t('Finalize request');
  }

  protected function getNotizDescription() {
    return t('Specify a reason for finalizing request.');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    parent::hookFormAlter($form, $form_state, $form_id);
    $node_state = $this->node_state;
    $this->is_ff_request = $node_state->isStateRequestFinishFinally();
    if ($this->is_ff_request) {
      $options = [];
      if ($this->node_state->canDiscussRequest()) {
        $options[SlogXtwf::XTWF_DISCUSSION] = SlogXtwf::getTxtLetsDiscuss();
      }
      $options += [
        SlogXtwf::XTWF_SELECT_NO => (string) t('No, do NOT finish finally'),
        SlogXtwf::XTWF_SELECT_YES => (string) t('Yes, do finish collaboration finally'),
      ];
      $form['vote'] = [
        '#type' => 'select',
        '#title' => t('Next'),
        '#description' => t('Select how to finalize request.'),
        '#options' => $options,
        '#required' => TRUE,
          ] + $this->getInputFieldWrapper();
    }
    else {
      $this->addFieldWorkflow($form);
      $this->addFieldSeverity($form);
      $this->addFinalizeByField($form);
      $this->addFieldStrict($form);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getFinalizeArgs($values, $action = 'finalize') {
    $args = [];
    if ($this->is_ff_request) {
      $args['vote'] = $vote = $values['vote'];
    }
    else {
      $keys = ['workflow', 'severity'];
      foreach ($keys as $key) {
        if (!empty($values[$key])) {
          $args[$key] = $values[$key];
        }
      }
    }

    $args += parent::getFinalizeArgs($values, $action);
    return $args;
  }

}
