<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\admin\AdminFinalizeCollectContentController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content\admin;

//use Drupal\sxt_opentalk\SlogXtot;
//use Drupal\sxt_workflow\SlogXtwf;
//use Drupal\node\Entity\Node;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class AdminFinalizeCollectContentController extends AdminFinalizeControllerBase {

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return $this->t('.......Finalize content collection');
  }

  protected function getBuildWarning() {
    return t('.......WARNING: This will finalize content collection.');
  }

  protected function getFinishedMsg() {
    return t('Content collection has been finalized.');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    parent::hookFormAlter($form, $form_state, $form_id);
    $this->addFinalizeByField($form);
  }

}
