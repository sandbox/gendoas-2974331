<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\admin\AdminFinalizeCompetitionController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content\admin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_opentalk\XtotCompetitionFormTrait;

/**
 * Defines a controller ....
 */
class AdminFinalizeCompetitionController extends AdminFinalizeControllerBase {

  use XtotCompetitionFormTrait;

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return $this->t('Finalize competition');
  }

  protected function getBuildWarning() {
    return t('WARNING: This will finalize competition.');
  }

  protected function getFinishedMsg() {
    return t('Competition has been finalized.');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    parent::hookFormAlter($form, $form_state, $form_id);
    $node_state = $this->node_state;
    $competitions = $node_state->getCompetitionData('competitions') ?? [];
    foreach ($competitions as $competition) {
      $target = $competition['target'];
      if ($target === 'workflow') {
        $this->addFieldWorkflow($form, $competition, TRUE);
      }
      if ($target === 'renew_id') {
        $this->addFieldRenewId($form, $competition, TRUE);
      }
      elseif ($target === 'severity') {
        $this->addFieldSeverity($form, $competition);
      }
    }

    // how to finalize
    $this->addFinalizeByField($form);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    if (!$form_state->isSubmitted() || $form_state->hasAnyErrors()) {
      $slogxtData = &$form_state->get('slogxtData');
      $slogxtData['alterForm'] = [
        'command' => 'sxt_opentalk::alterFormCollaborate',
      ];
    }
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getFinalizeArgs($values, $action = 'finalize') {
    $args = [];
    $keys = ['renew_id', 'workflow', 'severity'];
    foreach ($keys as $key) {
      if (!empty($values[$key])) {
        $args[$key] = $values[$key];
      }
    }

    $args += parent::getFinalizeArgs($values, $action = 'vote');
    return $args;
  }

}
