<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\admin\AdminCollabRenewController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content\admin;

use Drupal\sxt_opentalk\SlogXtot;
use Drupal\sxt_workflow\SlogXtwf;
use Drupal\slogxt\SlogXt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_opentalk\XtotConfirmControllerBase;
use Drupal\sxt_opentalk\XtotRequestFormTrait;

/**
 * Defines a controller ....
 */
class AdminCollabRenewController extends XtotConfirmControllerBase {

  use XtotRequestFormTrait;

  protected $is_renew_request = FALSE;

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return t('Renew collaboration');
  }

  protected function getSubmitLabel() {
    return t('Renew');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    parent::hookFormAlter($form, $form_state, $form_id);

    $node_state = $this->node_state;

    $description = t('Specify a reason for renewing collaboration.');
    $this->addFieldAdminNotice($form, $description);

    $renew_id = '';
    $severity_id = $node_state->getSeverityId();
    $finalize_id = $node_state->getFinalizeByTimeoutKey();
    $strict_id = $node_state->getFinalizeStrictKey();
    if ($node_state->isStateRequestRenew()) {
      $this->is_renew_request = TRUE;
      $wf_state_plugin = $node_state->getWfStatePlugin();
      $renew_id = $wf_state_plugin->getPreferredBest('renew_id');
      $severity_id = $wf_state_plugin->getPreferredBest('severity');
      $finalize_id = $wf_state_plugin->getPreferredBest('finalize');
      $strict_id = $wf_state_plugin->getPreferredBest('strict');
    }
    $options = $node_state->getOptionsRequestRenew();
    $form['renew_state_id'] = [
      '#type' => 'select',
      '#title' => t('Next'),
      '#description' => t('Select the state to continue with.'),
      '#options' => $options,
      '#default_value' => $renew_id,
      '#required' => TRUE,
      '#attributes' => [
        'id' => "xtot-renew-state",
      ],
        ] + $this->getInputFieldWrapper();

    $description = t('Select the severity level for further collaboration.');
    $this->addFieldSeverity($form, $severity_id, FALSE, $description);
    $description = t('Select how to finalize the next state.');
    $this->addFieldFinalizeBy($form, $finalize_id, $description);
    $description = t('Select whether the requirements should be strictly applied.');
    $this->addFieldStrict($form, $strict_id, $description);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $this->makeMsgClosable($form);
    $msg = $this->node_state->getStateInfo(TRUE, TRUE);

    if ($this->is_renew_request) {
      $action_info = t('WARNING: This will finalize the request, bypassing the vote of the users.');
    }
    else {
      $action_info = t("You are about to renew collaboration without user's request.");
    }

    $warn_only_msg = $this->htmlHrPlus() . $action_info;
    $this->setPreFormMessage($msg, $form_state, $warn_only_msg);
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    $node_state = $calledObject->node_state;

    $values = $form_state->getValues();
    $args = [
      'action' => 'renew',
      'renew_state_id' => $values['renew_state_id'],
      'severity' => $values['severity'],
      'finalize_by_timeout' => $node_state->getFinalizeByTimeoutFlag($values['finalize']),
      'finalize_strict' => $node_state->getFinalizeStrictFlag($values['strict']),
      'admin_data' => [
        'admin_action' => 'renew',
        'admin_notice' => (string) $values['admin_notice'],
      ],
    ];

    if ($result = $node_state->collabRenewByAdmin($args)) {
      $calledObject->success = $success = (boolean) $result['success'];
      if (!empty($result['message'])) {
        $calledObject->addFinalMoreMessage($result['message'], $success);
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    drupal_get_messages();  // clear messages
    if (!$this->success) {
      $this->addFinalMoreMessage(SlogXt::txtSeeLogMessages(), FALSE);
    }
    $this->setFinalMoreMessages();

    return [
      'command' => 'sxt_opentalk::finishedWorkflowChanged',
      'args' => SlogXtot::getNodeStateRefreshArgs($this->node_id),
    ];
  }

}
