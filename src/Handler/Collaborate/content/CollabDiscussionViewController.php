<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\CollabDiscussionViewController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\slogxt\Ajax\SxtInvokeCommand;
use Drupal\sxt_opentalk\XtotCollaborateTrait;
use Drupal\sxt_slogitem\Handler\OnSlogitemListSelect;

/**
 * Defines a controller for ....
 */
class CollabDiscussionViewController extends OnSlogitemListSelect {

  use XtotCollaborateTrait;
  
  public function __construct() {
    $request = \Drupal::request();
    $node_id = $request->get('node_id');
    $request->attributes->set('base_entity_id', $node_id);
    $this->setCollabNodeUserData();
  }

  public function getContentResult() {
    $slogitem_id = $term_id = 0;
    if ($this->has_node_state) {
      $state_data = $this->node_state->getStateData() ?? [];
      $slogitem_id = $state_data['slogitem-id'] ?? 0;
      $term_id = $state_data['menu-tid'] ?? 0;
    }
    
    $request = \Drupal::request();
    $request->attributes->set('slogitem_id', $slogitem_id);
    $request->attributes->set('term_id', $term_id);

    $data = $this->responseData();
    $data['path'] = trim($request->getPathInfo(), '/');

    $response = new AjaxResponse();
    $response->addCommand(new SxtInvokeCommand($data, $this->getAttachedAssets()));
    return $response;
  }

}
