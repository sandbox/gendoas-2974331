<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\CollabSubscribeController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content;

use Drupal\sxt_opentalk\SlogXtot;
use Drupal\sxt_workflow\SlogXtwf;
use Drupal\sxt_workflow\XtwfNodeSubscribe;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_opentalk\XtotConfirmControllerBase;

/**
 * Defines a controller for deleting user's collaboration request.
 */
class CollabSubscribeController extends XtotConfirmControllerBase {

  protected $has_subscribed;

  protected function getFormObjectArg() {
    $this->has_subscribed = XtwfNodeSubscribe::hasSubscribed($this->node_id, $this->user_id);
    return parent::getFormObjectArg();
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    if ($this->has_subscribed) {
      return t('Unsubscribe');
    }
    return t('Subscribe');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $msg = $this->node_state->getNodeInfo();
    $action_info = $this->has_subscribed //
        ? t('You are about to unsubscribe.<br />You will receive no more mails for this collaboration.') //
        : t('You are about to subscribe for receiving mails about all state transitions.');
    $warn_only_msg = $this->htmlHrPlus() . $action_info;

    $this->setPreFormMessage($msg, $form_state, $warn_only_msg);

    return parent::buildContentResult($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    $node_id = $calledObject->node_id;
    $user_id = $calledObject->user_id;
    if ($node_state = SlogXtwf::getNodeState($node_id, TRUE)) {
      if ($calledObject->has_subscribed) {
        XtwfNodeSubscribe::unsubscribe($node_id, $user_id);
      }
      else {
        XtwfNodeSubscribe::subscribe($node_id, $user_id);
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    drupal_get_messages();  // clear messages
    $msg = $this->has_subscribed ? t('You unsubscribed successfully.') : t('You subscribed successfully.');
    drupal_set_message($msg);

    return [
      'command' => 'sxt_opentalk::finishedWorkflowChanged',
      'args' => SlogXtot::getNodeStateRefreshArgs($this->node_id),
    ];
  }

}
