<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\RequestCollabController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content;

use Drupal\sxt_opentalk\SlogXtot;
use Drupal\sxt_workflow\SlogXtwf;
use Drupal\slogxt\SlogXt;
use Drupal\sxt_workflow\XtwfNodeSubscribe;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\Controller\XtEditControllerBase;

/**
 * Defines a controller ....
 */
class RequestCollabController extends XtEditControllerBase {

  protected $node;
  protected $workflow;
  protected $on_select = FALSE;
  protected $on_change = FALSE;
  protected $on_select_severity = FALSE;
  protected $node_state = FALSE;
  protected $has_user_request = FALSE;

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $request = \Drupal::request();
    $node_id = (integer) $request->get('base_entity_id');
    $this->node = $node = SlogXtwf::getNode($node_id);
    $request->attributes->set('node', $this->node);
    $workflow = $request->get('workflow');
    $severity = $request->get('severity');
    if ($workflow === '{workflow}') {
      if (SlogXtwf::hasNodeState($node_id)) {
        $this->node_state = $node_state = SlogXtwf::getNodeState($node_id);
        if (!$node_state->isStateRequestCollab()) {
          $message = t('Node state not handled for collaboration request.');
          throw new \LogicException($message);
        }
        $this->on_change = TRUE;
        $workflow = $node_state->getWorkflowId();
        $severity = $node_state->getSeverityId();
        $request->attributes->set('workflow', $workflow);
        $request->attributes->set('severity', $severity);
        $uid = \Drupal::currentUser()->id();
        $this->has_user_request = $node_state->hasUserData($uid);
      }
      else {
        $workflows = SlogXtwf::getWorkflows($node->getType(), 'trunk');
        if (count($workflows) === 1) {
          $wf = reset($workflows);
          $request->attributes->set('workflow', $wf->id());
          $workflow = $request->get('workflow');
        }
      }
    }

    if ($workflow === '{workflow}') {
      $this->on_select = TRUE;
      $request->attributes->set('xtwf_constraint', 'trunk');
      return 'Drupal\sxt_opentalk\Form\Collaborate\SelectWorkflowForm';
    }
    elseif ($severity === '{severity}') {
      $this->on_select = TRUE;
      $this->on_select_severity = TRUE;

      $xtwf_type_plugin = SlogXtwf::getWorkflow($workflow)->getTypePlugin();
      $xtwf_state = $xtwf_type_plugin->getState(SlogXtwf::XTWF_REQUEST_COLLABORATE);
      $request->attributes->set('severityInfos', $xtwf_state->getSeverityInfos());

      return 'Drupal\sxt_opentalk\Form\Collaborate\SelectSeverityForm';
    }

    // now the final form
    $this->workflow = SlogXtwf::getWorkflow($workflow);
    if (empty($this->workflow)) {
      $message = t('Not a valid workflow: @workflow', ['@workflow' => $workflow]);
      throw new \LogicException($message);
    }

    $xtwf_type_plugin = $this->workflow->getTypePlugin();
    $xtwf_state = $xtwf_type_plugin->getState(SlogXtwf::XTWF_REQUEST_COLLABORATE);
    $infos = $xtwf_state->getSeverityInfos() ?? [];
    $this->severity_id = $severity;
    $this->severity_info = $infos[$severity];
    if (empty($this->severity_info)) {
      $message = t('Info not found for severity @severity ', ['@severity' => $severity]);
      throw new \LogicException($message);
    }

    $request->attributes->set('workflow', $this->workflow);
    return 'Drupal\sxt_opentalk\Form\Collaborate\content\RequestCollabForm';
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    if ($this->on_select) {
      $title = $this->on_select_severity ? $this->t('Select severity') : t('Select workflow');
    }
    else {
      $args = ['%label' => $this->node->label()];
      $title = t('Request collaboration: %label', $args);
    }
    return $title;
  }

  protected function getSubmitLabel() {
    if ($this->on_select) {
      $label = t('Next');
    }
    elseif ($this->on_change) {
      $label = $this->has_user_request ? t('Change request') : t('Add request');
    }
    else {
      $label = t('Create request');
    }
    return $label;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preBuildForm();
   */
  protected function preBuildForm(FormStateInterface $form_state) {
    parent::preBuildForm($form_state);

    $op = \Drupal::request()->get('op', false);
    if ($op) {
      $this->opSave = ($op === (string) t('Save'));
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['runCommand'] = $this->on_select ? 'radios' : 'wizardFormEdit';
    if ($this->opSave && !$form_state->hasAnyErrors()) {
      $slogxtData['wizardFinished'] = true;
      $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
    }
    else {
      $no_message = $this->opSave;
      $warn_only_msg = '';
      if ($this->on_select) {
        $target = $this->on_select_severity ? 'severity' : 'workflow';
        $msg = t('Select the @target you prefer for the collaboration.', ['@target' => $target]);
      }
      else {
        $slogxtData['alterForm'] = [
          'command' => 'sxt_opentalk::alterFormCollaborate',
        ];

        //
        if ($this->on_change) {
          if (!$no_message) {
            $msg = $this->node_state->getStateInfo(TRUE, TRUE);
            if ($this->has_user_request) {
              $warn_only_msg = $this->htmlHrPlus() . t('You already requested for collaboration. You can change it now.');
            }
            else {
              $warn_only_msg = $this->htmlHrPlus() . t('Do your initial request for collaborating this content.');
            }
          }
        }
        else {
          $type_settings = $this->workflow->getPluginCollections()['type_settings'];
          $wf_txt = t('Workflow');
          $wf_selected = SlogXt::htmlHighlightText($this->workflow->label());
          $wf_info = $type_settings->getConfiguration()['description'] ?? '_???_$wf_info';
          $sv_txt = t('Severity');
          $sv_info = str_replace('<br />', ', ', $this->severity_info);
          $sv_selected = SlogXt::htmlHighlightText(SlogXtwf::getSeverityLabel($this->severity_id));
          $lines = [
            t('Create collaboration request with:'),
            "- $wf_txt: $wf_selected ($wf_info)",
            "- $sv_txt: $sv_selected ($sv_info)",
          ];
          $msg = implode('<br />', $lines);
        }
      }

      if (!$no_message) {
        $this->setPreFormMessage($msg, $form_state, $warn_only_msg);
      }
    }
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    drupal_get_messages();  // clear messages
    $node_id = $this->node->id();

    $success = TRUE;
    if (!empty($this->formSubmitResult) && empty($this->formSubmitResult['success'])) {
      $success = FALSE;
      $msg = $this->formSubmitResult['message'];
    }
    elseif ($this->on_change) {
      if ($this->has_user_request) {
        $msg = t('Your collaboration request has been changed.');
      }
      else {
        $msg = t('Your collaboration request has been appended');
      }
    }
    else {
      $msg = t('Initial collaboration request has been created.');
      $user_id = \Drupal::currentUser()->id();
      if (SlogXtwf::canNotify() && XtwfNodeSubscribe::hasSubscribed($node_id, $user_id)) {
        $msg2 =  t('You have been subscribed for that collaboration.');
      }
    }

    $this->addFinalMoreMessage($msg, $success);
    if (!empty($msg2)) {
      $this->addFinalMoreMessage($msg2, $success);
    }
    $this->setFinalMoreMessages();
    
    return [
      'command' => 'sxt_opentalk::finishedWorkflowChanged',
      'args' => SlogXtot::getNodeStateRefreshArgs($node_id),
    ];
  }

}
