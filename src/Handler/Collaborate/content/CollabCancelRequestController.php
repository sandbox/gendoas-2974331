<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\CollabCancelRequestController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content;

use Drupal\sxt_opentalk\SlogXtot;
use Drupal\sxt_workflow\XtwfNodeSubscribe;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_opentalk\XtotConfirmControllerBase;

/**
 * Defines a controller for deleting user's collaboration request.
 */
class CollabCancelRequestController extends XtotConfirmControllerBase {

  protected $is_collab_request = FALSE;
  protected $is_subscribed = FALSE;
  protected $is_renew_request = FALSE;
  protected $is_last_request_vote = FALSE;
  protected $has_history = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function getFormObjectArg() {
    $node_state = $this->node_state;
    $this->is_collab_request = $node_state->isStateRequestCollab();
    $this->is_subscribed = XtwfNodeSubscribe::hasSubscribed($this->node_id, $this->user_id);
    $this->is_renew_request = $node_state->isStateRequestRenew();
    $this->is_last_request_vote = $node_state->isLastRequestVote();
    $this->has_history = $node_state->hasHistory();
    return parent::getFormObjectArg();
  }

  /**
   * {@inheritdoc}
   */
  protected function getFormTitle() {
    if ($this->is_collab_request) {
      return t('Cancel your request');
    }
    else {
      return t('Cancel your vote');
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    if ($this->has_node_state) {
      $node_state = $this->node_state;
      $msg = $node_state->getStateInfo(TRUE, TRUE);
      if ($this->is_collab_request) {
        $action_info = t('You are about to cancel your collaboration request.');
        if ($this->is_last_request_vote && !$this->has_history) {
          $action_info .= '<br />' . t('WARNING: This will remove the request completely.');
        }
      }
      elseif ($node_state->isStateRequestFinishFinally()) {
        $action_info = t('You are about to cancel your vote for finish finally request.');
        if ($this->is_last_request_vote) {
          $action_info .= '<br />' . t('WARNING: This will abort the finish finally request.');
        }
      }
      elseif ($this->is_renew_request) {
        $action_info = t('You are about to cancel your vote for renew request.');
        if ($this->is_last_request_vote) {
          $action_info .= '<br />' . t('WARNING: This will abort the renew request.');
        }
      }
      else {
        $action_info = '_???_$action_info';
      }


      $warn_only_msg = $this->htmlHrPlus() . $action_info;
      $this->setPreFormMessage($msg, $form_state, $warn_only_msg);
    }

    return parent::buildContentResult($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    $node_state = $calledObject->node_state;
    $node_state->deleteUserData($calledObject->user_id);

    if ($calledObject->is_collab_request) {
      // maybe request is deleted
      $calledObject->setCollabNodeUserData($reload = TRUE);
    }
    elseif ($calledObject->is_last_request_vote) {
      if ($result = $node_state->stateFinalizeByCancelRequest()) {
        $success = (boolean) $result['success'];
        if (!empty($result['message'])) {
          $calledObject->addFinalMoreMessage($result['message'], $success);
        }
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    drupal_get_messages();  // clear messages
    
    $this->addFinalMoreMessage(t('Your vote has been canceled.'));
    if ($this->is_collab_request) {
      if (!$this->has_node_state) {
        $this->addFinalMoreMessage(t('Collaboration request has been completely removed.'));
        if ($this->is_subscribed) {
          $this->addFinalMoreMessage(t('You have been unsubscibed for that content.'));
        }
      }
    }
    elseif (!$this->node_state->stateIsRequestType()) {
      $this->addFinalMoreMessage(t('Request has been canceled.'));
    }
    $this->setFinalMoreMessages();

    return [
      'command' => 'sxt_opentalk::finishedWorkflowChanged',
      'args' => SlogXtot::getNodeStateRefreshArgs($this->node_id),
    ];
  }

}
