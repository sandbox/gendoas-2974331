<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\dev\GenerateCDiscussionController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content\dev;

use Drupal\sxt_opentalk\SlogXtot;
use Drupal\slogxt\SlogXt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_opentalk\XtotConfirmControllerBase;

/**
 * Defines a controller for deleting user's collaboration request.
 */
class GenerateCDiscussionController extends XtotConfirmControllerBase {

  protected function getFormObjectArg() {
    $request = \Drupal::request();
    $request->attributes->set('_plugin_id', 'content');
    return 'Drupal\devel_generate\Form\DevelGenerateForm';
  }

  protected function getFormTitle() {
    return t('Generate discussion node');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    parent::hookFormAlter($form, $form_state, $form_id);

    $form['kill'] = [
      '#type' => 'hidden',
      '#value' => FALSE,
    ];
    $form['num'] = [
      '#type' => 'hidden',
      '#value' => 1,
    ];
    $form['time_range'] = [
      '#type' => 'hidden',
      '#value' => 604800,
    ];

    unset($form['add_alias']);
    unset($form['add_language']);
    unset($form['add_statistics']);

    // comments
    $form['max_comments']['#default_value'] = 10;
    // node_types
    $options = &$form['node_types']['#options'];
    $options = ['mwdiscussion' => $options['mwdiscussion']];
    $form['node_types']['#value'] = ['mwdiscussion' => TRUE];
    $form['node_types']['#disabled'] = TRUE;

    $form['emptyline'] = [
      '#type' => 'markup',
      '#markup' => '<div>&nbsp;</div>',
    ];
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preRenderForm();
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);
    $form['node_types']['#header']['comments']['class'] = [];
  }

  public static function formValidate(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();

    $node_state = $calledObject->node_state;
    if (!$node_state->isStateDiscussion()) {
      $form_state->setErrorByName('', t('Logical error: current state is not discussion state.'));
    }
    $history_data = $node_state->getPreviousHistoryData();
    if (empty($history_data)) {
      $form_state->setErrorByName('', t('Logical error: history data not found.'));
    }

    if ($form_state->getValue('max_comments') > 10) {
      $form_state->setErrorByName('max_comments', t('Batch processing is not supported (max=10).'));
    }
      
    // add submit handler
    $submit_handlers = &$form_state->getTriggeringElement()['#submit'];
    if (empty($submit_handlers) && !empty($form['#submit'])) {
      $submit_handlers = $form['#submit'];
    }
    $submit_handlers[] = [get_class(self::calledObject()), 'formSubmit'];
    $form_state->setSubmitHandlers($submit_handlers);
  }

  /**
   * {@inheritdoc}
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    $node_state = $calledObject->node_state;

    $discussion_node = $calledObject->getNewInsertedNode();
    if (!$discussion_node) {
      $success = FALSE;
      SlogXtot::logger()->error(t('Missing created node.'));
    }
    else {
      $objects = SlogXtot::getMwDiscussionObjects($node_state, TRUE);
      $success = $objects['success'];
    }

    // error ??
    if (!$success) {
      $calledObject->addFinalMoreMessage(t('Error occured!'), FALSE);
      $calledObject->addFinalMoreMessage(SlogXt::txtSeeLogMessages(), FALSE);
      return;
    }

    // contents for discussion node
    $node_contents = SlogXtot::getMwDiscussionNodeContents($node_state);
    $discussion_node_title = $node_contents['title'];
    $mwcontent = SlogXtot::buildMwDiscussionNodeMwContent($node_contents);

    $discussion_node->setTitle($discussion_node_title)
        ->set('mwcontent', $mwcontent)
        ->save();

    // set new node data
    $slogitem = $objects['slogitem'];
    $entity_type = 'node';
    $discussion_nid = $discussion_node->id();
    $slogitem->setRouteData($entity_type, $discussion_nid);
    $slogitem->save();

    $msg = t('Discussion node and some comments have been generated.');
    $calledObject->addFinalMoreMessage($msg, TRUE);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    drupal_get_messages();  // clear messages
    $this->setFinalMoreMessages();

    return [
      'command' => 'sxt_opentalk::finishedWorkflowChanged',
      'args' => SlogXtot::getNodeStateRefreshArgs($this->node_id),
    ];
  }

}
