<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\dev\DevTriggerTimeoutController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content\dev;

use Drupal\sxt_opentalk\SlogXtot;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_opentalk\XtotConfirmControllerBase;

/**
 * Defines a controller for deleting user's collaboration request.
 */
class DevTriggerTimeoutController extends XtotConfirmControllerBase {

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return t('Trigger timeout');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $msg = $this->node_state->getStateInfo(TRUE, TRUE);
    $warn_only_msg = $this->htmlHrPlus() . t('You are about to trigger timeout for current state.');
    $this->setPreFormMessage($msg, $form_state, $warn_only_msg);

    return parent::buildContentResult($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    $node_state = $calledObject->node_state;
    if ($result = $node_state->stateFinalizeByTimeout()) {
      $success = (boolean) $result['success'];
      if ($success) {
        $msg = t('State has been finished by timeout.');
      }
      elseif (!empty($result['message'])) {
        $msg = $result['message'];
      }
      else {
        $msg = t('Error on finalizing state by timeout.');
      }
      
      $calledObject->addFinalMoreMessage($msg, $success);
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    drupal_get_messages();  // clear messages
    $this->setFinalMoreMessages();

    return [
      'command' => 'sxt_opentalk::finishedWorkflowChanged',
      'args' => SlogXtot::getNodeStateRefreshArgs($this->node_id),
    ];
  }

}
