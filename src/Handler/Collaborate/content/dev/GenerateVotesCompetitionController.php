<?php

//desired requests 




/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\dev\GenerateVotesCompetitionController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content\dev;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller for deleting user's collaboration request.
 */
class GenerateVotesCompetitionController extends GenerateForRequestControllerBase {

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return t('Generate votes');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    parent::hookFormAlter($form, $form_state, $form_id);

    $wrapper = $this->getInputFieldWrapper();

    // multiplier
    $this->addFieldMultiplier($form);

    $this->ids_all = [];
    $node_state = $this->node_state;

    $competitions = $node_state->getCompetitionData('competitions') ?? [];
    foreach ($competitions as $competition) {
      $target = $competition['target'];
      $best = $competition['preferred']['best'];
      $next = $competition['preferred']['next'];

      if ($target === 'workflow') {
        $field_title = t('Workflow to prefer');
        $labels = $node_state->getWorkflowLabels();
      }
      if ($target === 'renew_id') {
        $field_title = t('Next');
        $labels = $node_state->getOptionsRequestRenew();
      }
      elseif ($target === 'severity') {
        $field_title = t('Severity to prefer');
        $labels = SlogXtwf::getStateSeverityOptions();
      }

      $options = [
        $best['id'] => $labels[$best['id']],
        $next['id'] => $labels[$next['id']],
      ];
      $this->ids_all[$target] = array_keys($options);

      $form[$target] = [
        '#type' => 'select',
        '#title' => $field_title,
        '#options' => $options,
        '#default_value' => $best['id'],
        '#required' => TRUE,
          ] + $wrapper;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    $node_state = $calledObject->node_state;
    $saved_request_data = $node_state->getData('request_data');

    // reset current state data
    $node_state->resetStateDataState();
    
    $desired = $calledObject->desired;
    $wf_number1 = $rid_number1 = $sv_number1 = (integer) (round($desired / 2));
    $ids_all = $calledObject->ids_all;

    $wf_best = $form_state->getValue('workflow');
    if ($has_workflow = !empty($wf_best)) {
      $array = array_diff($ids_all['workflow'], [$wf_best]);
      $wf_next = reset($array);
    }
    $rid_best = $form_state->getValue('renew_id');
    if ($has_renew_id = !empty($rid_best)) {
      $array = array_diff($ids_all['renew_id'], [$rid_best]);
      $rid_next = reset($array);
    }
    $sv_best = $form_state->getValue('severity');
    if ($has_severity = !empty($sv_best)) {
      $array = array_diff($ids_all['severity'], [$sv_best]);
      $sv_next = reset($array);
    }
    
    foreach ($calledObject->user_ids as $uid) {
      $user_data = [];
      
      if ($has_workflow) {
        if ($wf_number1 > 0) {
          $user_data['workflow'] = $wf_best;
          $wf_number1--;
        }
        else {
          $user_data['workflow'] = $wf_next;
        }        
      }
      
      if ($has_renew_id) {
        if ($rid_number1 > 0) {
          $user_data['renew_id'] = $rid_best;
          $rid_number1--;
        }
        else {
          $user_data['renew_id'] = $rid_next;
        }        
      }
      
      if ($has_severity) {
        if ($sv_number1 > 0) {
          $user_data['severity'] = $sv_best;
          $sv_number1--;
        }
        else {
          $user_data['severity'] = $sv_next;
        }        
      }
      

      $node_state->setUserData($uid, $user_data);
    }
    
    $started = REQUEST_TIME;
    $history = $node_state->getHistory();
    if (!empty($history)) {
      $keys = array_keys($history);
      $started = end($keys);
    }
    $node_state->setData($started, 'started');
        
    $generated = implode(';', $calledObject->user_ids);
    $node_state->setData($generated, 'generated')
        ->setData($saved_request_data, 'request_data');
    
    // save generated data
    $node_state->save();
  }

}
