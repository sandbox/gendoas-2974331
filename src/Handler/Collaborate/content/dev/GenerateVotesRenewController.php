<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\dev\GenerateVotesRenewController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content\dev;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller for deleting user's collaboration request.
 */
class GenerateVotesRenewController extends GenerateForRequestControllerBase {

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return t('Generate renew votes');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    parent::hookFormAlter($form, $form_state, $form_id);

    // multiplier
    $this->addFieldMultiplier($form);

    // next
    $this->addFieldNext($form);
    $form['rid_modality'] = $this->getFieldModality('next');
    // severity
    $this->addFieldSeverity($form);
    $form['sv_modality'] = $this->getFieldModality('severity');
    // finalize
    $this->addFieldFinalizeBy($form);
    // strict
    $this->addFieldStrict($form);
    // strict
    $this->addFieldDiscuss($form);
  }

  /**
   * {@inheritdoc}
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    $node_state = $calledObject->node_state;
    $saved_request_data = $node_state->getData('request_data');

    // reset current requests data, preserve base data
    $node_state->resetStateDataAll(TRUE);

    // prepare
    $desired = $calledObject->desired;
    $values = $form_state->getValues();

    //renew_id
    $renew_id = $values['renew_id'];
    $rid_part = $values['rid_modality'];
    $rid_decisive = ($rid_part === 'decisive');
    $rid_competition = ($rid_part === 'competition');
    $rid_number1 = (integer) ($rid_decisive ? round($desired * 3 / 4) : round($desired / 2));
    $rid_number2 = 0;
    $rid_all = array_keys($calledObject->all_renew_ids);
    $rid_left = array_values(array_diff($rid_all, [$renew_id]));
    if (count($rid_left) === 0) {
      $rid_number1 = $desired;
    }
    elseif ($rid_competition) {
      $idx = random_int(0, count($rid_left) - 1);
      $renew_id2 = $rid_left[$idx];
      $rid_left = array_values(array_diff($rid_left, [$rid_number2]));
      $rid_number2 = (integer) round($desired * 3 / 8);
    }
    //severity
    $severity = $values['severity'];
    $sv_part = $values['sv_modality'];
    $sv_decisive = ($sv_part === 'decisive');
    $sv_competition = ($sv_part === 'competition');
    $sv_number1 = (integer) ($sv_decisive ? round($desired * 3 / 4) : round($desired / 2));
    $sv_number2 = 0;
    $sv_all = array_keys($calledObject->all_severities);
    $sv_left = array_values(array_diff($sv_all, [$severity]));
    if ($sv_competition) {
      $idx = random_int(0, count($sv_left) - 1);
      $severity2 = $sv_left[$idx];
      $sv_left = array_values(array_diff($sv_left, [$severity2]));
      $sv_number2 = (integer) round($desired * 3 / 8);
    }

    //finalize
    $fn_number = (integer) round($desired * 1 / 2);
    $fn_all = array_keys($calledObject->all_finalize);
    $finalize = $values['finalize'];
    $fn_left = array_values(array_diff($fn_all, [$finalize]));
    $finalize2 = $fn_left[0];

    //strict
    $st_number = (integer) round($desired * 1 / 2);
    $st_yes = ($values['strict'] === SlogXtwf::XTWF_SELECT_YES);
    $strict = $node_state->getFinalizeStrictKey($st_yes);
    $strict2 = $node_state->getFinalizeStrictKey(!$st_yes);

    //discuss
    $has_discussion_state = $node_state->hasDiscussionState();
    if ($has_discussion_state) {
      $ds_number = (integer) round($desired * 1 / 2);
      $ds_yes = ($values['discuss'] === SlogXtwf::XTWF_SELECT_YES);
      $discuss = $node_state->getDiscussFirstKey($ds_yes);
      $discuss2 = $node_state->getDiscussFirstKey(!$ds_yes);
    }

    // execute
    $rid_mod = count($rid_left);
    $sv_mod = count($sv_left);
    $rid_left_idx = 0;
    $sv_left_idx = 0;
    foreach ($calledObject->user_ids as $uid) {
      $user_data = [];
      // rid
      if ($rid_number1 > 0) {
        $user_data['renew_id'] = $renew_id;
        $rid_number1--;
      }
      elseif ($rid_number2 > 0) {
        $user_data['renew_id'] = $renew_id2;
        $rid_number2--;
      }
      elseif ($rid_mod > 0) {
        $user_data['renew_id'] = $rid_left[$rid_left_idx];
        $rid_left_idx = ++$rid_left_idx % $rid_mod;
      }
      else {
        $user_data['renew_id'] = !$rid_left_idx ? $renew_id : $renew_id2;
        $rid_left_idx = ++$rid_left_idx % 2;
      }
      // sv
      if ($sv_number1 > 0) {
        $user_data['severity'] = $severity;
        $sv_number1--;
      }
      elseif ($sv_number2 > 0) {
        $user_data['severity'] = $severity2;
        $sv_number2--;
      }
      elseif ($sv_mod > 0) {
        $user_data['severity'] = $sv_left[$sv_left_idx];
        $sv_left_idx = ++$sv_left_idx % $sv_mod;
      }
      else {
        $user_data['severity'] = !$sv_left_idx ? $severity : $severity2;
        $sv_left_idx = ++$sv_left_idx % 2;
      }
      // fn
      if ($fn_number > 0) {
        $user_data['finalize'] = $finalize;
        $fn_number--;
      }
      else {
        $user_data['finalize'] = $finalize2;
      }
      // strict
      if ($st_number > 0) {
        $user_data['strict'] = $strict;
        $st_number--;
      }
      else {
        $user_data['strict'] = $strict2;
      }
      // discuss
      if ($has_discussion_state) {
        if ($ds_number > 0) {
          $user_data['discuss'] = $discuss;
          $ds_number--;
        }
        else {
          $user_data['discuss'] = $discuss2;
        }
      }

      $node_state->setUserData($uid, $user_data);
    }

    $node_state->setData(REQUEST_TIME, 'started');
    $generated = implode(';', $calledObject->user_ids);
    $node_state->setData($generated, 'generated')
        ->setData($saved_request_data, 'request_data');

    // save generated data
    $node_state->save();

    // check whether to finalize by required posts
    if ($result = $node_state->checkFinalizeByRequired()) {
      $success = (boolean) $result['success'];
      if (!empty($result['message'])) {
        $calledObject->addFinalMoreMessage($result['message'], $success);
      }
    }
  }

}
