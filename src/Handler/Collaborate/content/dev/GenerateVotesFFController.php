<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\dev\GenerateVotesFFController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content\dev;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller for deleting user's collaboration request.
 */
class GenerateVotesFFController extends GenerateForRequestControllerBase {

  public $desired;

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return t('Generate votes');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    parent::hookFormAlter($form, $form_state, $form_id);

    $node_state = $this->node_state;
    // multiplier
    $this->addFieldMultiplier($form);

    $options = [];
    if ($node_state->hasDiscussionState()) {
      $options[SlogXtwf::XTWF_DISCUSSION] = SlogXtwf::getTxtLetsDiscuss();
    }
    $options += [
      SlogXtwf::XTWF_SELECT_NO => (string) t('No, do NOT finish finally'),
      SlogXtwf::XTWF_SELECT_YES => (string) t('Yes, do finish collaboration finally'),
    ];
    $this->all_votes = $options;
    $form['vote'] = [
      '#type' => 'select',
      '#title' => t('Vote to prefer'),
      '#options' => $options,
      '#required' => TRUE,
        ] + $this->getInputFieldWrapper();
    $form['vote_modality'] = $this->getFieldModality('vote');
  }

  protected function getBuildWarning() {
    return t('NOTE: expire will be set to REQUEST_TIME.')
        . '<br />' . t('You may trigger timeout or run CRON after generation.');
  }

  /**
   * {@inheritdoc}
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    $node_state = $calledObject->node_state;
    $saved_request_data = $node_state->getData('request_data');

    // reset current state data
    $node_state->resetStateDataState();

    $values = $form_state->getValues();
    $vote_selected = $values['vote'];
    $vote_modality = $values['vote_modality'];
    $vote_decisive = ($vote_modality === 'decisive');
    $vote_competition = ($vote_modality === 'competition');
    $all_votes = array_keys($calledObject->all_votes);
    $votes_left = array_values(array_diff($all_votes, [$vote_selected]));

    $desired = $calledObject->desired;
    $p_required = SlogXtwf::getPercentSevere();

    $v_number2 = 0;
    if ($vote_selected === 'yes') {
      $v_number1 = (integer) ($vote_decisive //
          ? ceil(($desired + 1) * $p_required / 100) //
          : ($desired - 1) * $p_required / 100);
      if (!$vote_decisive) {
        $v_number1 = (integer) min($v_number1, round($desired / 2));
      }
    }
    else {
      $v_number1 = (integer) ($vote_decisive ? round($desired * 3 / 4) : round($desired / 2));
    }

    if (count($votes_left) === 0) {
      $v_number1 = $desired;
    }
    elseif ($vote_competition) {
      $idx = random_int(0, count($votes_left) - 1);
      $vote2 = $votes_left[$idx];
      $votes_left = array_values(array_diff($votes_left, [$vote2]));
      $v_number2 = (integer) round($desired * 3 / 8);
    }

    $v_mod = count($votes_left);
    $v_left_idx = 0;
    $v2_done = FALSE;
    foreach ($calledObject->user_ids as $uid) {
      $user_data = [];
      // 
      if ($v_number2 > 0 && !$v2_done) {
        $user_data['vote'] = $vote2;
        $v_number2--;
        $v2_done = TRUE;
      }
      elseif ($v_number1 > 0) {
        $user_data['vote'] = $vote_selected;
        $v_number1--;
      }
      elseif ($v_number2 > 0) {
        $user_data['vote'] = $vote2;
        $v_number2--;
      }
      elseif ($v_mod > 0) {
        $user_data['vote'] = $votes_left[$v_left_idx];
        $v_left_idx = ++$v_left_idx % $v_mod;
      }
      else {
        $user_data['vote'] = !$v_left_idx ? $vote_selected : $vote2;
        $v_left_idx = ++$v_left_idx % 2;
      }

      $node_state->setUserData($uid, $user_data);
    }

    $started = REQUEST_TIME;
    $history = $node_state->getHistory();
    if (!empty($history)) {
      $keys = array_keys($history);
      $started = end($keys);
    }
    $node_state->setData($started, 'started');

    $generated = implode(';', $calledObject->user_ids);
    $node_state->setData($generated, 'generated')
        ->setData($saved_request_data, 'request_data')
        ->setExpire(REQUEST_TIME);

    // save generated data
    $node_state->save();

    // check whether to finalize by required posts
    if ($result = $node_state->checkFinalizeByRequired()) {
      $success = (boolean) $result['success'];
      if (!empty($result['message'])) {
        $calledObject->addFinalMoreMessage($result['message'], $success);
      }
    }
  }

}
