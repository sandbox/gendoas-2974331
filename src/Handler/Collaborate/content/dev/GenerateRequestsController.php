<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\dev\GenerateRequestsController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content\dev;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller for deleting user's collaboration request.
 */
class GenerateRequestsController extends GenerateForRequestControllerBase {

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return t('Generate requests');
  }

  protected function isGenerateCollabRequests() {
    return TRUE;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    parent::hookFormAlter($form, $form_state, $form_id);

    // multiplier
    $this->addFieldMultiplier($form);

    $form['preserve_data'] = [
      '#type' => 'checkbox',
      '#title' => t('Preserve history'),
      '#default_value' => TRUE,
        ] + $this->getInputFieldWrapper();

    // workflow
    $this->addFieldWorkflow($form);
    $form['wf_modality'] = $this->getFieldModality('workflow');
    // severity
    $this->addFieldSeverity($form);
    $form['sv_modality'] = $this->getFieldModality('severity');
    // finalize
    $this->addFieldFinalizeBy($form);
    // strict
    $this->addFieldStrict($form);
    // discuss
    $this->addFieldDiscuss($form);
  }

  protected function getBuildWarning() {
    return t('WARNING: This will destroy all existing workflow data.');
  }

  /**
   * {@inheritdoc}
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    $node_state = $calledObject->node_state;

    // prepare
    $workflow_id = $node_state->getWorkflowId();
    $severity_id = $node_state->getSeverityId();
    $finalize_by_timeout = (boolean) $node_state->getData('finalize_by_timeout');
    $finalize_strict = (boolean) $node_state->getData('finalize_strict');
    $saved_notice = $node_state->getRequestNotice();
    $desired = $calledObject->desired;
    $values = $form_state->getValues();
    $preserve_data = (boolean) $values['preserve_data'];

    // reset current requests data
    $node_state->resetStateDataAll($preserve_data);

    //workflow
    $workflow = $values['workflow'];
    $wf_part = $values['wf_modality'];
    $wf_decisive = ($wf_part === 'decisive');
    $wf_competition = ($wf_part === 'competition');
    $wf_number1 = (integer) ($wf_decisive ? round($desired * 3 / 4) : round($desired / 2));
    $wf_number2 = 0;
    $wf_all = array_keys($calledObject->all_workflows);
    $wf_left = array_values(array_diff($wf_all, [$workflow]));
    if (count($wf_left) === 0) {
      $wf_number1 = $desired;
    }
    elseif ($wf_competition) {
      $idx = random_int(0, count($wf_left) - 1);
      $workflow2 = $wf_left[$idx];
      $wf_left = array_values(array_diff($wf_left, [$workflow2]));
      $wf_number2 = (integer) round($desired * 3 / 8);
    }

    //severity
    $severity = $values['severity'];
    $sv_part = $values['sv_modality'];
    $sv_decisive = ($sv_part === 'decisive');
    $sv_competition = ($sv_part === 'competition');
    $sv_number1 = (integer) ($sv_decisive ? round($desired * 3 / 4) : round($desired / 2));
    $sv_number2 = 0;
    $sv_all = array_keys($calledObject->all_severities);
    $sv_left = array_values(array_diff($sv_all, [$severity]));
    if ($sv_competition) {
      $idx = random_int(0, count($sv_left) - 1);
      $severity2 = $sv_left[$idx];
      $sv_left = array_values(array_diff($sv_left, [$severity2]));
      $sv_number2 = (integer) round($desired * 3 / 8);
    }

    //finalize
    $fn_number = (integer) round($desired * 1 / 2);
    $fn_all = array_keys($calledObject->all_finalize);
    $finalize = $values['finalize'];
    $fn_left = array_values(array_diff($fn_all, [$finalize]));
    $finalize2 = $fn_left[0];

    //strict
    $st_number = (integer) round($desired * 1 / 2);
    $st_all = array_keys($calledObject->all_strict);
    $strict = $values['strict'];
    $st_left = array_values(array_diff($st_all, [$strict]));
    $strict2 = $st_left[0];

    //discuss
    $has_discussion_state = $node_state->hasDiscussionState();
    if ($has_discussion_state) {
      $ds_number = (integer) round($desired * 1 / 2);
      $ds_yes = ($values['discuss'] === SlogXtwf::XTWF_SELECT_YES);
      $discuss = $ds_yes ? SlogXtwf::XTWF_SELECT_YES : SlogXtwf::XTWF_SELECT_NO;
      $discuss2 = $ds_yes ? SlogXtwf::XTWF_SELECT_NO : SlogXtwf::XTWF_SELECT_YES;
    }

    // execute
    $wf_mod = count($wf_left);
    $sv_mod = count($sv_left);
    $wf_left_idx = 0;
    $sv_left_idx = 0;
    $wf2_done = FALSE;
    $sv2_done = FALSE;
    foreach ($calledObject->user_ids as $uid) {
      $user_data = [];
      // wf
      if ($wf_number2 > 0 && !$wf2_done) {
        $user_data['workflow'] = $workflow2;
        $wf_number2--;
        $wf2_done = TRUE;
      }
      elseif ($wf_number1 > 0) {
        $user_data['workflow'] = $workflow;
        $wf_number1--;
      }
      elseif ($wf_number2 > 0) {
        $user_data['workflow'] = $workflow2;
        $wf_number2--;
      }
      elseif ($wf_mod > 0) {
        $user_data['workflow'] = $wf_left[$wf_left_idx];
        $wf_left_idx = ++$wf_left_idx % $wf_mod;
      }
      else {
        $user_data['workflow'] = !$wf_left_idx ? $workflow : $workflow2;
        $wf_left_idx = ++$wf_left_idx % 2;
      }
      // sv
      if ($sv_number2 > 0 && !$sv2_done) {
        $user_data['severity'] = $severity2;
        $sv_number2--;
        $sv2_done = TRUE;
      }
      elseif ($sv_number1 > 0) {
        $user_data['severity'] = $severity;
        $sv_number1--;
      }
      elseif ($sv_number2 > 0) {
        $user_data['severity'] = $severity2;
        $sv_number2--;
      }
      elseif ($sv_mod > 0) {
        $user_data['severity'] = $sv_left[$sv_left_idx];
        $sv_left_idx = ++$sv_left_idx % $sv_mod;
      }
      else {
        $user_data['severity'] = !$sv_left_idx ? $severity : $severity2;
        $sv_left_idx = ++$sv_left_idx % 2;
      }
      // fn
      if ($fn_number > 0) {
        $user_data['finalize'] = $finalize;
        $fn_number--;
      }
      else {
        $user_data['finalize'] = $finalize2;
      }
      // strict
      if ($st_number > 0) {
        $user_data['strict'] = $strict;
        $st_number--;
      }
      else {
        $user_data['strict'] = $strict2;
      }
      // discuss
      if ($has_discussion_state) {
        if ($ds_number > 0) {
          $user_data['discuss'] = $discuss;
          $ds_number--;
        }
        else {
          $user_data['discuss'] = $discuss2;
        }
      }

      $node_state->setUserData($uid, $user_data);
    }

    $generated = implode(';', $calledObject->user_ids);
    $request_data = [
      'from_state' => 'none',
      'notice' => $saved_notice,
    ];
    if ($desired === 1) {
      $workflow_id = $workflow;
      $severity_id = $severity;
      $finalize_by_timeout = $node_state->getFinalizeByTimeoutFlag($values['finalize']);
      $finalize_strict = $node_state->getFinalizeStrictFlag($values['strict']);
    }
    $node_state->setData(REQUEST_TIME - (8 * 86400), 'started')
        ->setWorkflowId($workflow_id)
        ->setSeverity($severity_id)
        ->setData($generated, 'generated')
        ->setData($request_data, 'request_data')
        ->setData($finalize_by_timeout, 'finalize_by_timeout')
        ->setData($finalize_strict, 'finalize_strict')
        ->save();

    // check whether to finalize by required posts
    if ($result = $node_state->checkFinalizeByRequired()) {
      $success = (boolean) $result['success'];
      if (!empty($result['message'])) {
        $calledObject->addFinalMoreMessage($result['message'], $success);
      }
    }
  }

}
