<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\dev\DevActionSelectController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content\dev;

use Drupal\slogxt\Controller\XtEditControllerBase;

/**
 * Defines a controller ....
 */
class DevActionSelectController extends XtEditControllerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    return '\Drupal\sxt_opentalk\Form\Collaborate\content\dev\DevActionSelectForm';
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return $this->t('dev: Generate plus');
  }

}
