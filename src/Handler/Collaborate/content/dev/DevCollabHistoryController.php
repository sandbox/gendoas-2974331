<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\dev\DevCollabHistoryController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content\dev;

use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_opentalk\XtotEditControllerBase;

/**
 * Defines a controller for deleting user's collaboration request.
 */
class DevCollabHistoryController extends XtotEditControllerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $this->setCollabNodeUserData();
    return '\Drupal\sxt_opentalk\Form\Collaborate\content\dev\DevCollabHistoryForm';
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    $id = $this->node_state->id();
    $label = $this->node_state->getNode()->label();
    return t('__DEV__History - (%id) %label', ['%id' => $id, '%label' => $label]);
  }

  /**
   * {@inheritdoc}
   */
  protected function postBuildForm(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['isLastPage'] = TRUE;
  }

}
