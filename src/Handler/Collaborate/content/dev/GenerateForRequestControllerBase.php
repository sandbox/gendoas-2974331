<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\dev\GenerateForRequestControllerBase.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content\dev;

use Drupal\sxt_opentalk\SlogXtot;
use Drupal\sxt_workflow\SlogXtwf;
use Drupal\sloggen\SlogGen;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_opentalk\XtotConfirmControllerBase;

/**
 * Defines a controller for deleting user's collaboration request.
 */
abstract class GenerateForRequestControllerBase extends XtotConfirmControllerBase {

  protected function addFieldMultiplier(array &$form) {
    $form['multiplier'] = [
      '#type' => 'textfield',
      '#title' => t('Multiplier'),
      '#description' => t('Valid values: 0 <= multiplier <= 3. <br />Enter 0 for generating your request/vote only.'),
      '#default_value' => 1,
      '#size' => 10,
        ] + $this->getInputFieldWrapper();
  }

  protected function addFieldWorkflow(array &$form) {
    $options = [];
    $node_state = $this->node_state;
    $workflows = SlogXtwf::getWorkflows($this->node->getType(), 'trunk');
    $options += SlogXtwf::getWorkflowOptions($workflows);
    $this->all_workflows = $options;
    $form['workflow'] = [
      '#type' => 'select',
      '#title' => t('Workflow to prefer'),
      '#options' => $options,
      '#default_value' => $node_state->getWorkflowId(),
      '#required' => TRUE,
        ] + $this->getInputFieldWrapper();
  }

  protected function addFieldSeverity(array &$form) {
    $this->all_severities = $options = SlogXtwf::getStateSeverityOptions();
    $form['severity'] = [
      '#type' => 'select',
      '#title' => t('Severity to prefer'),
      '#options' => $options,
      '#default_value' => $this->node_state->getSeverityId(),
      '#required' => TRUE,
        ] + $this->getInputFieldWrapper();
  }

  protected function addFieldNext(array &$form) {
    $this->all_renew_ids = $options = $this->node_state->getOptionsRequestRenew();
    $form['renew_id'] = [
      '#type' => 'select',
      '#title' => t('Next'),
      '#description' => t('Vote for action or vote for the state to continue with.'),
      '#options' => $options,
      '#default_value' => $renew_id,
      '#required' => TRUE,
      '#attributes' => [
        'id' => "xtot-renew-state",
      ],
        ] + $this->getInputFieldWrapper();
  }

  protected function addFieldFinalizeBy(array &$form) {
    $description = t('Select which option to prefer.');
    $this->all_finalize = $options = SlogXtwf::getStateFinalizeOptions();
    $form['finalize'] = [
      '#type' => 'radios',
      '#title' => t('Finalize by'),
      '#description' => $description,
      '#options' => $options,
      '#default_value' => $this->node_state->getFinalizeByTimeoutKey(),
      '#required' => TRUE,
        ] + $this->getInputFieldWrapper(TRUE);
  }

  protected function addFieldStrict(array &$form) {
    $description = t('Select which option to prefer.');
    $this->all_strict = $options = SlogXtwf::getStrictOptions();
    $form['strict'] = [
      '#type' => 'radios',
      '#title' => t('Strict'),
      '#description' => $description,
      '#options' => $options,
      '#default_value' => $this->node_state->getFinalizeStrictKey(),
      '#required' => TRUE,
        ] + $this->getInputFieldWrapper(TRUE);
  }

  protected function addFieldDiscuss(array &$form) {
    if ($this->node_state->hasDiscussionState()) {
      $description = t('Select which option to prefer.');
      $options = SlogXtwf::getDiscussOptions();
      $form['discuss'] = [
        '#type' => 'radios',
        '#title' => SlogXtwf::getTxtLetsDiscuss(),
        '#description' => $description,
        '#options' => $options,
        '#default_value' => SlogXtwf::XTWF_SELECT_NO,
        '#required' => TRUE,
          ] + $this->getInputFieldWrapper(TRUE);
    }
  }

  protected function getFieldModality($for) {
    $part_options = [
      'decisive' => t('Decisive'),
      'competition' => t('Competitive'),
      'none' => t('None'),
    ];
    $field = [
      '#type' => 'radios',
      '#title' => t('Modality (@for)', ['@for' => $for]),
      '#options' => $part_options,
      '#default_value' => 'decisive',
      '#required' => TRUE,
        ] + $this->getInputFieldWrapper(TRUE);

    return $field;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return t('Generate requests/votes');
  }

  protected function getBuildWarning() {
    return '';
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $this->makeMsgClosable($form);
    $msg = $this->node_state->getStateInfo(TRUE, TRUE);
    $warn_only_msg = $this->getBuildWarning();
    if (!empty($warn_only_msg)) {
      $warn_only_msg = $this->htmlHrPlus() . $warn_only_msg;
    }
    $this->setPreFormMessage($msg, $form_state, $warn_only_msg);
    return parent::buildContentResult($form, $form_state);
  }

  protected function isGenerateCollabRequests() {
    return FALSE;
  }

  /**
   * Validate callback.
   * 
   * Use own validator for adding own submitter.
   */
  public static function formValidate(array &$form, FormStateInterface $form_state) {
    parent::formValidate($form, $form_state);
    $has_multiplier = $form_state->hasValue('multiplier');
    $multiplier = $has_multiplier ? floatval(str_replace(',', '.', $form_state->getValue('multiplier'))) : FALSE;
    if ($has_multiplier && ($multiplier < 0 || $multiplier > 3)) {
      $form_state->setErrorByName('multiplier', t('Not a valid value.'));
    }
    else {
      $calledObject = self::calledObject();
      $node_state = $calledObject->node_state;
      if ($calledObject->isGenerateCollabRequests()) {
        $node_state->setStateId(SlogXtwf::XTWF_REQUEST_COLLABORATE);
      }
      $required = $node_state->getSeverityRequired();

      $reset = ($multiplier <= 0.1);
      if ($reset) {
        $desired = 1;
        $calledObject->desired = 1;
        $calledObject->user_ids = [$calledObject->user_id];
      }
      else {
        $calledObject->desired = $desired = (integer) ($multiplier * $required - 1);
        $calledObject->user_ids = SlogGen::getRandomUserIds($desired, [$calledObject->user_id]);
      }

      if (count($calledObject->user_ids) < $desired) {
        $form_state->setErrorByName('desired', t('There are not enough users registered.'));
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    drupal_get_messages();  // clear messages
    drupal_set_message(t('Requests/Votes have been generated.'));
    $this->setFinalMoreMessages();

    return [
      'command' => 'sxt_opentalk::finishedWorkflowChanged',
      'args' => SlogXtot::getNodeStateRefreshArgs($this->node_id),
    ];
  }

}
