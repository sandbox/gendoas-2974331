<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\ActionsCollaborateController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\slogxt\Controller\XtEditControllerBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class ActionsCollaborateController extends XtEditControllerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    return 'Drupal\sxt_opentalk\Form\Collaborate\content\ActionsSelectForm';
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
//    $label = t('There is no collaboration yet');
    $node_id = (integer) \Drupal::request()->get('base_entity_id');
    $label = SlogXtwf::getNode($node_id)->label();
//    if (SlogXtwf::hasNodeState($node_id)) {
//      $node_state = SlogXtwf::getNodeState($node_id);
//      $label = $node_state->getNode()->label();
////      $state = $node_state->infoCurrentState();
//    }
    
    return t('Select action - %label', ['%label' => $label]);
//    return t('Select action - %state', ['%state' => $state]);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    return ['slogxtData' => $form_state->get('slogxtData')];
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::appendAttachments();
   */
  protected function appendAttachments() {
    return TRUE;
  }

}
