<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\CollabHistoryController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content;

use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_opentalk\XtotEditControllerBase;

/**
 * Defines a controller for deleting user's collaboration request.
 */
class CollabHistoryController extends XtotEditControllerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $this->setCollabNodeUserData();
    return '\Drupal\sxt_opentalk\Form\Collaborate\content\CollabHistoryForm';
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    $label = $this->node_state->getNode()->label();
    return t('History - %label', ['%label' => $label]);
  }

  /**
   * {@inheritdoc}
   */
  protected function postBuildForm(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['isLastPage'] = TRUE;
  }

}
