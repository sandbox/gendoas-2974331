<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Handler\Collaborate\content\RequestRenewController.
 */

namespace Drupal\sxt_opentalk\Handler\Collaborate\content;

use Drupal\sxt_opentalk\SlogXtot;
use Drupal\sxt_workflow\SlogXtwf;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_opentalk\XtotConfirmControllerBase;
use Drupal\sxt_opentalk\XtotRequestFormTrait;

/**
 * Defines a controller ....
 */
class RequestRenewController extends XtotConfirmControllerBase {

  use XtotRequestFormTrait;

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    if ($this->is_renew_request) {
      $title = t('Vote for renew collaboration');
    }
    else {
      $title = t('Create request for renew');
    }
    return $title;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getSubmitLabel();
   */
  protected function getSubmitLabel() {
    if ($this->is_renew_request) {
      $label = $this->has_user_data ? t('Change vote') : t('Add vote');
    }
    else {
      $label = t('Create request');
    }
    return $label;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    parent::hookFormAlter($form, $form_state, $form_id);

    $node_state = $this->node_state;
    $this->is_renew_request = $node_state->isStateRequestRenew();
    $severity_id = $node_state->getSeverityId();
    $finalize_id = $node_state->getFinalizeByTimeoutKey();
    $strict_id = $node_state->getFinalizeStrictKey();
    $discuss_id = SlogXtwf::XTWF_SELECT_NO;
    $renew_id = '';
    if ($this->is_renew_request) {
      $renew_id = $node_state->getWfStatePlugin()->getPreferredBest('renew_id');
      if ($this->has_user_data) {
        $user_data = $node_state->getUserData($this->user_id) ?? [];
        $renew_id = $user_data['renew_id'] ?? $renew_id;
        $severity_id = $user_data['severity'] ?? $severity_id;
        $finalize_id = $user_data['finalize'] ?? $finalize_id;
        $strict_id = $user_data['strict'] ?? $strict_id;
        $discuss_id = $user_data['discuss'] ?? $discuss_id;
      }
    }
    else {
      $this->addFieldNotice($form);
    }

    // vote, action or next state
    $form['renew_id'] = [
      '#type' => 'select',
      '#title' => t('Next'),
      '#description' => t('Vote for action or vote for the state to continue with.'),
      '#options' => $node_state->getOptionsRequestRenew(),
      '#default_value' => $renew_id,
      '#required' => TRUE,
      '#attributes' => [
        'id' => "xtot-renew-state",
      ],
        ] + $this->getInputFieldWrapper();

    // severity for next state
    $description = t('You may want to change the severity level for further collaboration.');
    $this->addFieldSeverity($form, $severity_id, FALSE, $description);

    // how to finalize
    $description = t('What do you prefer, how to finalize the next state.');
    $this->addFieldFinalizeBy($form, $finalize_id, $description);    
    // strict
    $this->addFieldStrict($form, $strict_id);
    // discuss
    $this->addFieldDiscuss($form, $discuss_id);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $this->makeMsgClosable($form);
    $msg = $this->node_state->getStateInfo(TRUE, TRUE);
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['alterForm'] = [
      'command' => 'sxt_opentalk::alterFormCollaborate',
    ];

    if ($this->is_renew_request) {
      if ($this->has_user_data) {
        $action_info = t('Change your vote for renewing collaboration');
      }
      else {
        $action_info = t('Cast your vote for renewing collaboration');
      }
    }
    else {
      $action_info = t('You are creating a request for renewing collaboration.');
    }

    $warn_only_msg = $this->htmlHrPlus() . $action_info;
    $this->setPreFormMessage($msg, $form_state, $warn_only_msg);
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function formValidate(array &$form, FormStateInterface $form_state) {
    parent::formValidate($form, $form_state);
    self::calledObject()->formValidateHasChanged($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    $node_state = $calledObject->node_state;

    $calledObject->success = TRUE;
    if (!$calledObject->is_renew_request) {
      $notice = $form_state->getValue('notice');
      $success = $node_state->initializeRequest(SlogXtwf::XTWF_REQUEST_RENEW, $notice);
      if (!$success) {
        $calledObject->success = FALSE;
      }
    }

    if ($calledObject->success) {
      $values = $form_state->getValues();
      $user_data = [
        'renew_id' => $values['renew_id'],
        'finalize' => $values['finalize'],
        'severity' => $values['severity'],
        'strict' => $values['strict'],
      ];
      if (!empty($values['discuss'])) {
        $user_data['discuss'] = $values['discuss'];
      }
      $node_state->setUserData($calledObject->user_id, $user_data)->save();

      // check whether to finalize by required posts
      if ($result = $node_state->checkFinalizeByRequired()) {
        $success = (boolean) $result['success'];
        if (!empty($result['message'])) {
          $calledObject->addFinalMoreMessage($result['message'], $success);
        }
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    drupal_get_messages();  // clear messages    
    $success = (boolean) $this->success;
    if (!$success) {
      $msg = t('Error on initializing request.');
    }
    elseif (!$this->is_renew_request) {
      $msg = t('Renew request has been created.');
    }
    elseif ($this->has_user_data) {
      $msg = t('You changed your vote for renewing collaboration.');
    }
    else {
      $msg = t('You voted for renewing collaboration.');
    }
    
    $this->addFinalMoreMessage($msg, $success);
    $this->setFinalMoreMessages();

    return [
      'command' => 'sxt_opentalk::finishedWorkflowChanged',
      'args' => SlogXtot::getNodeStateRefreshArgs($this->node_id),
    ];
  }

}
