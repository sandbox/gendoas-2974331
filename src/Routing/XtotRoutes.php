<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Routing\XtotRoutes.
 */

namespace Drupal\sxt_opentalk\Routing;

use Drupal\slogxt\SlogXt;
use Symfony\Component\Routing\Route;
use Drupal\slogxt\Routing\SlogxtRoutesBase;

/**
 * Defines a route subscriber to register a url for serving image styles.
 */
class XtotRoutes extends SlogxtRoutesBase {

  /**
   * Returns an array of route objects.
   *
   * @return \Symfony\Component\Routing\Route[]
   *   An array of route objects.
   */
  public function routes() {
    $baseAjaxPath = trim(SlogXt::getBaseAjaxPath('sxt_opentalk'), '/');
    $routes = $raw_data = [];

    $raw_data += $this->_rawAjaxCollaborateMain($baseAjaxPath); 
    $raw_data += $this->_rawAjaxCollaborate($baseAjaxPath); 
    $raw_data += $this->_rawAjaxCollaborateContentAdmin($baseAjaxPath);
    $raw_data += $this->_rawAjaxCollaborateContentDev($baseAjaxPath);
    
    $defaults = [
//        'requirements' => ['_access' => 'TRUE'],
      'requirements' => ['_permission' => 'access content'],
      'options' => [],
      'host' => '',
      'schemes' => [],
      'methods' => [],
      'condition' => null,
    ];

    foreach ($raw_data as $key => $data) {
      $data += $defaults;
      $routes["sxt_opentalk.$key"] = new Route(
          $data['path'], $data['defaults'], $data['requirements'], $data['options'], $data['host'], $data['schemes'], $data['methods'], $data['condition']
      );
    }

    return $routes;
  }

  /**
   * //todo::text
   * 
   * @param string $baseAjaxPath
   * @return array
   */
  private function _rawAjaxCollaborateMain($baseAjaxPath) {
    $base_entity_id = '{base_entity_id}';
    $baseCollaboratePath = $baseAjaxPath . '/collaborate_main';
    $controllerPath = '\Drupal\sxt_opentalk\Handler\Collaborate\main';
    $raw_data = [
      'collaborate.main.actions' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/actions",
        'defaults' => [
          '_controller' => "{$controllerPath}\ActionsCollabMainController::getContentResult",
        ],
      ],
      'collaborate.main.subscriptions' => [
        'path' => "/$baseAjaxPath/edit/profile/$base_entity_id/subscriptions",
        'defaults' => [
          '_controller' => "{$controllerPath}\SubscriptionsController::getContentResult",
        ],
        'options' => self::$optAjaxBase,
      ],
    ];

    return $raw_data;
  }

  /**
   * //todo::text
   * 
   * @param string $baseAjaxPath
   * @return array
   */
  private function _rawAjaxCollaborate($baseAjaxPath) {
    $base_entity_id = '{base_entity_id}';
    $next_entity_id = '{next_entity_id}';
    $baseCollaboratePath = $baseAjaxPath . '/collaborate/c';
    $controllerPath = '\Drupal\sxt_opentalk\Handler\Collaborate\content';
    $raw_data = [
      'collaborate.c.actions' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/actions",
        'defaults' => [
          '_controller' => "{$controllerPath}\ActionsCollaborateController::getContentResult",
        ],
      ],
      'collaborate.c.request_collab' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/workflow/request/{workflow}/{severity}",
        'defaults' => [
          '_controller' => "{$controllerPath}\RequestCollabController::getContentResult",
        ],
      ],
      'collaborate.c.request_renew' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/workflow/requestrenew",
        'defaults' => [
          '_controller' => "{$controllerPath}\RequestRenewController::getContentResult",
        ],
      ],
      'collaborate.c.request_finish_finally' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/workflow/requestff",
        'defaults' => [
          '_controller' => "{$controllerPath}\RequestFinishFinallyController::getContentResult",
        ],
      ],
      'collaborate.c.collect' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/workflow/ccollect/{action}",
        'defaults' => [
          '_controller' => "{$controllerPath}\CollabCCollectController::getContentResult",
        ],
      ],
      'collaborate.c.competition' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/workflow/competition",
        'defaults' => [
          '_controller' => "{$controllerPath}\CollabCompetitionController::getContentResult",
        ],
      ],
      'collaborate.c.discussion' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/wfdiscussion/$next_entity_id",
        'defaults' => [
          '_controller' => "{$controllerPath}\CollabDiscussionController::getContentResult",
        ],
      ],
      'collaborate.c.viewdiscussion' => [
        'path' => "/$baseCollaboratePath/viewdiscussion/{node_id}",
        'defaults' => [
          '_controller' => "{$controllerPath}\CollabDiscussionViewController::getContentResult",
        ],
      ],
      'collaborate.c.cancel_request' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/workflow/cancelrequest",
        'defaults' => [
          '_controller' => "{$controllerPath}\CollabCancelRequestController::getContentResult",
        ],
      ],
      'collaborate.c.subscribe' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/workflow/subscribe",
        'defaults' => [
          '_controller' => "{$controllerPath}\CollabSubscribeController::getContentResult",
        ],
      ],
      'collaborate.c.history' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/workflow/history",
        'defaults' => [
          '_controller' => "{$controllerPath}\CollabHistoryController::getContentResult",
        ],
      ],
    ];

    return $raw_data;
  }

  private function _rawAjaxCollaborateContentAdmin($baseAjaxPath) {
    $base_entity_id = '{base_entity_id}';
    $baseCollaboratePath = $baseAjaxPath . '/collaborate/c';
    $controllerPath = '\Drupal\sxt_opentalk\Handler\Collaborate\content\admin';
    $raw_data = [
      'collaborate.c.admin' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/admin/select",
        'defaults' => [
          '_controller' => "{$controllerPath}\ActionsSelectController::getContentResult",
        ],
      ],    
      'collaborate.c.admin.finalize_request' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/admin/rcomplete",
        'defaults' => [
          '_controller' => "{$controllerPath}\AdminFinalizeRequestController::getContentResult",
        ],
      ],    
      'collaborate.c.admin.finalize_ccollect' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/admin/cccomplete",
        'defaults' => [
          '_controller' => "{$controllerPath}\AdminFinalizeCollectContentController::getContentResult",
        ],
      ],    
      'collaborate.c.admin.finalize_discussion' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/admin/disccomplete",
        'defaults' => [
          '_controller' => "{$controllerPath}\AdminFinalizeDiscussionController::getContentResult",
        ],
      ],    
      'collaborate.c.admin.finalize_ccompetition' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/admin/ccompetition",
        'defaults' => [
          '_controller' => "{$controllerPath}\AdminFinalizeCompetitionController::getContentResult",
        ],
      ],    
      'collaborate.c.admin.renew' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/admin/renew",
        'defaults' => [
          '_controller' => "{$controllerPath}\AdminCollabRenewController::getContentResult",
        ],
      ],    
      'collaborate.c.admin.hrestore' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/admin/hrestore/{history_key}",
        'defaults' => [
          '_controller' => "{$controllerPath}\AdminRestoreFromHistoryController::getContentResult",
          'history_key' => 0,
        ],
      ],    
      'collaborate.c.admin.dodiscuss' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/admin/dodiscuss",
        'defaults' => [
          '_controller' => "{$controllerPath}\AdminLetsDiscussController::getContentResult",
        ],
      ],    
      'collaborate.c.admin.terminate' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/admin/terminate",
        'defaults' => [
          '_controller' => "{$controllerPath}\AdminTerminateController::getContentResult",
        ],
      ],    
    ];

    return $raw_data;
  }

  private function _rawAjaxCollaborateContentDev($baseAjaxPath) {
    $base_entity_id = '{base_entity_id}';
    $baseCollaboratePath = $baseAjaxPath . '/collaborate/c';
    $controllerPath = '\Drupal\sxt_opentalk\Handler\Collaborate\content\dev';
    $raw_data = [
      'collaborate.c.dev.generate' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/generate/select",
        'defaults' => [
          '_controller' => "{$controllerPath}\DevActionSelectController::getContentResult",
        ],
      ],    
      'collaborate.c.dev.generate_requests' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/generate/requests",
        'defaults' => [
          '_controller' => "{$controllerPath}\GenerateRequestsController::getContentResult",
        ],
      ],    
      'collaborate.c.dev.generate_ff_requests' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/generate/ffrequests",
        'defaults' => [
          '_controller' => "{$controllerPath}\GenerateVotesFFController::getContentResult",
        ],
      ],    
      'collaborate.c.dev.generate_renew_requests' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/generate/renewrequests",
        'defaults' => [
          '_controller' => "{$controllerPath}\GenerateVotesRenewController::getContentResult",
        ],
      ],    
      'collaborate.c.dev.generate_cvotes' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/generate/cvotes",
        'defaults' => [
          '_controller' => "{$controllerPath}\GenerateVotesCompetitionController::getContentResult",
        ],
      ],    
      'collaborate.c.dev.generate_cposts' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/generate/cposts",
        'defaults' => [
          '_controller' => "{$controllerPath}\GenerateCPostsController::getContentResult",
        ],
      ],    
      'collaborate.c.dev.generate_cdiscussion' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/generate/cdiscuss",
        'defaults' => [
          '_controller' => "{$controllerPath}\GenerateCDiscussionController::getContentResult",
        ],
      ],    
      'collaborate.c.dev.trigger_timeout' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/triggerto",
        'defaults' => [
          '_controller' => "{$controllerPath}\DevTriggerTimeoutController::getContentResult",
        ],
      ],    
      'collaborate.c.dev.history' => [
        'path' => "/$baseCollaboratePath/$base_entity_id/generate/history",
        'defaults' => [
          '_controller' => "{$controllerPath}\DevCollabHistoryController::getContentResult",
        ],
      ],    
    ];

    return $raw_data;
  }
  
}
