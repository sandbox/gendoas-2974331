<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\SlogXtot.
 */

namespace Drupal\sxt_opentalk;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogxt\SlogXt;
use Drupal\slogtx\SlogTx;
use Drupal\sxt_slogitem\Entity\SlogItem;
use Drupal\sxt_workflow\Entity\XtwfNodeStateInterface;
use Drupal\sxt_workflow\XtwfNodeSubscribe;
use Drupal\node\Entity\Node;
use Drupal\Component\Utility\Crypt;

/**
 */
class SlogXtot {

  protected static $config = NULL;

  // tokens
  const XTOTT_MWC_MAXDEPTH = '%%xtotmwcmaxdepth%%';
  const XTOTT_WFDISCUSS_MAX = '%%xtotwfdiscussmax%%';
  const XTOTT_WFDISCUSS_HINT = '%%xtotwfdiscusshint%%';
  const XTOT_TXMENU_DISCUSSIONS = 'Discussions';

  public static function getConfig() {
    if (empty(self::$config)) {
      self::$config = \Drupal::config('sxt_opentalk.settings');
    }
    return self::$config;
  }

  /**
   * 
   * @return boolean
   */
  public static function getWfShowOnrequest() {
    return (boolean) (self::getConfig()->get('wf_show_allways'));
  }

  /**
   * Max. comments to be posted in discussion.
   * 
   * @return integer
   */
  public static function getWfDiscussMax() {
    return (integer) (self::getConfig()->get('wfdiscuss_max') ?? 30);
  }

  /**
   * 
   * @return string
   */
  public static function getWfDiscussHint() {
    return (string) (self::getConfig()->get('wfdiscuss_hint') ?? '...........wfdiscuss_hint (setting)<hr />');
  }

  public static function getNodeStateShow($node_id) {
    $show_special_data = FALSE;
    if (SlogXtwf::hasNodeState($node_id)) {
      $show_special_data = TRUE;
      if (!self::getWfShowOnrequest()) {
        $exclude = [
          SlogXtwf::XTWF_REQUEST_COLLABORATE,
          SlogXtwf::XTWF_FINISH_RENEWABLE,
          SlogXtwf::XTWF_FINISH_FINALLY,
        ];
        $state_id = SlogXtwf::getNodeState($node_id)->getStateId();
        $show_special_data = !in_array($state_id, $exclude);
      }
    }

    return $show_special_data;
  }

  public static function getNodeStateSpecialData($node_id) {
    $special_data = [
      'node_id' => $node_id,
      'nodeStateShow' => FALSE,
    ];

    if (SlogXtwf::hasNodeState($node_id, TRUE)) {
      $node_state = SlogXtwf::getNodeState($node_id);
      $nodeStateShow = self::getNodeStateShow($node_id);
      $htmlLeft = $node_state->getStateInfo();
      $htmlRight = "nid: $node_id";
      $cstate = SlogXt::htmlHighlightText($node_state->getWfState()->label());
      $winfo = $node_state->getStateBaseInfo();
      $hash = Crypt::hashBase64(serialize("$htmlLeft::$htmlRight::$nodeStateShow"));
      $special_data = [
        'node_id' => $node_id,
        'nodeStateShow' => $nodeStateShow,
        'nodeStateHash' => $hash,
        'htmlLeft' => "$cstate: $winfo<br />hash: $hash",
        'htmlRight' => $htmlRight,
      ];
    }


    return $special_data;
  }

  public static function getNodeStateRefreshArgs($node_id) {
    return SlogXtot::getNodeStateSpecialData($node_id);
  }

  public static function getSubscribedSids($uid, $keyed_sids = FALSE) {
    $sids = [];
    $nids = XtwfNodeSubscribe::getSubscribedNodeIds($uid);
    $nodes = Node::loadMultiple($nids);
    foreach ($nodes as $nid => $node) {
      if ($node && $xtnodebase_sid = $node->get('xtnodebase')->base_sid) {
        $sids[$nid] = $xtnodebase_sid;
      }
    }

    if (!$keyed_sids) {
      $sids = array_values($sids);
    }

    return $sids;
  }

  /**
   * Return menu term object for discussions if exists.
   * 
   * @param integer $node_id
   *  Node id
   * @param boolean $do_create
   *  Do create menu term if not exists
   * @return Drupal\slogtx\Entity\MenuTerm
   *  return menu term object or NULL
   */
  protected static function getMwDiscussionMenuTerm($node_id, $do_create = FALSE) {
//            $node = Node::load($node_id);
//            $options = [
//              'plugin_id' => SlogTx::SYS_VOCAB_ID_SUBMENU,
//              'entity' => $node,
//              'do_create' => $do_create,
//            ];
//            $rtget_plugin = SlogTx::pluginManager('rtget_sys_node')->getInstance($options);
//            $rootterm = $rtget_plugin->getRootTermByName($rt_name, $do_create);
    $rootterm = SlogTx::getSysNodeRootTermSubmenu($node_id, $do_create);
///todo::current:: TxRootTermGetPlugin - new
//    $rootterm = SlogXtsi::getNodeSubmenuRootTerm($node_id);
////todo::current:: TxRootTermGetPlugin - new
//    if (!$rootterm && $do_create) {
//      $rootterm = SlogXtsi::prepareNodeSubmenuRootTerm($node_id);
//    }

    $node = Node::load($node_id);
    if ($rootterm) {
      $name = self::XTOT_TXMENU_DISCUSSIONS;
      if ($rootterm->hasMenuTermByName($name)) {
        $menu_term = $rootterm->getMenuTermByName($name);
      }
      elseif ($do_create) {
        $args = [
          '@node_id' => $node_id,
          '@label' => $node->label(),
        ];
        $description = t('Container for discussions of a request: (@node_id) @label', $args);
        $menu_term = $rootterm->createMenuTermByName($name, $description);
      }
    }

    return ($menu_term ?? FALSE);
  }

  public static function buildMwDiscussionNodeMwContent($node_contents) {
    $base_node_header = $node_contents['node']['header'];
    $base_node_info = $node_contents['node']['info'];
    $state_header = $node_contents['state']['header'];
    $state_info = $node_contents['state']['info'];
    $start_header = $node_contents['start']['header'];
    $start_msg = $node_contents['start']['info'];
    $node_cls = 'class="mwdiscuss-node-info"';
    $state_cls = 'class="mwdiscuss-state-info"';

    $mwcontent = "== $base_node_header ==\n\n<div $node_cls>$base_node_info</div>\n\n"
        . "== $state_header ==\n\n<div $state_cls>$state_info</div>\n\n"
        . "== $start_header ==\n\n$start_msg\n";

    return $mwcontent;
  }

  /**
   * 
   * @param XtwfNodeStateInterface $node_state
   * @return string
   */
  public static function getMwDiscussionNodeContents(XtwfNodeStateInterface $node_state) {
    $node_contents = [
      'node' => [
        'header' => t('Base node info'),
        'info' => $node_state->getNodeInfo(),
      ],
      'start' => [
        'header' => t('Discussion'),
        'info' => self::XTOTT_WFDISCUSS_HINT,
      ],
    ];

    $history_data = $node_state->getPreviousHistoryData();
    if (!empty($history_data)) {
      $rollback_values = $node_state->getValues();
      // revert to history
      $node_state->setValues($history_data);

      // contents from history
      $node_contents += [
        'title' => $node_state->buildNodeTitleByState(),
        'state' => [
          'header' => $node_state->getWfState()->label(),
          'info' => $node_state->getStateInfo(TRUE),
        ],
      ];

      // rollback
      $node_state->setValues($rollback_values);
    }

    $node_contents += [
      'title' => '???',
      'state' => [
        'header' => '???',
        'info' => '???',
      ],
    ];

    return $node_contents;
  }

  /**
   * 
   * @param XtwfNodeStateInterface $node_state
   * @return type
   */
  protected static function createMwDiscussionNode(XtwfNodeStateInterface $node_state) {
    $node_contents = self::getMwDiscussionNodeContents($node_state);
    $discussion_node_title = $node_contents['title'];
    $mwcontent = self::buildMwDiscussionNodeMwContent($node_contents);

    $discussion_node = Node::create([
          'type' => 'mwdiscussion',
          'title' => $discussion_node_title,
          'mwcontent' => $mwcontent,
    ]);
    $discussion_node->save();

    return $discussion_node;
  }

  /**
   * 
   * @param XtwfNodeStateInterface $node_state
   * @param integer $menu_tid
   * @param boolean $do_create
   * @return Drupal\sxt_slogitem\Entity\SlogItemInterface or FALSE
   */
  protected static function getMwDiscussionSlogitem(XtwfNodeStateInterface $node_state, $menu_tid, $do_create = FALSE) {
    // set history data current
    $rollback_values = $node_state->getValues();
    $history_data = $node_state->getPreviousHistoryData();

    if (empty($history_data)) {
      return FALSE;
    }

    // revert to history
    $node_state->setValues($history_data);

    // get slogitem if exists
    $discussion_node_title = $node_state->buildNodeTitleByState();
    $slogitem = SlogXtsi::getSlogitemFromTidAndTitle($menu_tid, $discussion_node_title);

    // ensure $discussion_node
    if ($slogitem) {
      $nid = $slogitem->getTargetEntityId();
      $discussion_node = Node::load($nid);
    }
    elseif ($do_create && empty($discussion_node)) {
      $discussion_node = self::createMwDiscussionNode($node_state);
      if (!$discussion_node) {
        self::logger()->error(t('Discussion node not created.'));
      }
    }

    // rollback
    $node_state->setValues($rollback_values);

    if (empty($discussion_node)) {
      // there shouldn't be slogitem without node
      return FALSE;
    }
    if (empty($discussion_node->getEntityTypeId() !== 'mwdiscussion')) {
      $args = ['@type' => 'mwdiscussion'];
      self::logger()->error(t('Discussion node not of type @type.', $args));
      return FALSE;
    }

    // 
    $discussion_nid = $discussion_node->id();
    $entity_type = 'node';
    $weight = $node_state->getStarted();

    // 
    if (!empty($slogitem)) {
      $slogitem->setRouteData($entity_type, $discussion_nid);
      $slogitem->save();
    }
    elseif ($do_create) {
      // create new slog item
      $slogitem = SlogItem::create([
            'tid' => $menu_tid,
            'title' => $discussion_node_title,
            'status' => 1,
            'weight' => $weight,
            'created' => REQUEST_TIME,
      ]);
      $slogitem->setRouteData($entity_type, $discussion_nid);
      $slogitem->save();
    }

    return $slogitem;
  }

  /**
   * 
   * @param XtwfNodeStateInterface $node_state
   * @param boolean $do_create
   * @return array or FALSE
   */
  public static function getMwDiscussionObjects(XtwfNodeStateInterface $node_state, $do_create = FALSE) {
    $menu_term = $slogitem = FALSE;
    $result = [
      'success' => TRUE,
      'menu_term' => FALSE,
      'slogitem' => FALSE,
      'discussion_node' => FALSE,
    ];


    $state_data = $node_state->getStateData();
    $menu_term_id = $state_data['menu-tid'] ?? FALSE;
    $slogitem_id = $state_data['slogitem-id'] ?? FALSE;
    $has_old_state_data = ($menu_term_id && $slogitem_id);

    // menu term
    $base_node_id = $node_state->getNode()->id();
    if ($menu_term_id) {
      $menu_term = SlogTx::getMenuTerm($menu_term_id);
    }
    if (!$menu_term) {
      $menu_term_id = FALSE;
      $menu_term = self::getMwDiscussionMenuTerm($base_node_id, $do_create);
    }
    if ($menu_term) {
      $result['menu_term'] = $menu_term;
      if (!$menu_term_id) {
        // prevent loading by $slogitem_id
        $slogitem_id = FALSE;
      }
    }
    elseif ($do_create) {
      $result['success'] = FALSE;
      self::logger()->error(t('Missing menu term.'));
    }

    // slogitem
    if ($result['success'] && $menu_term) {
      if ($slogitem_id) {
        $slogitem = SlogXtsi::getSlogitem($slogitem_id);
      }
      if (!$slogitem) {
        $slogitem_id = FALSE;
        $slogitem = self::getMwDiscussionSlogitem($node_state, $menu_term->id(), $do_create);
      }
      if ($slogitem) {
        $result['slogitem'] = $slogitem;
      }
      elseif ($do_create) {
        $result['success'] = FALSE;
        self::logger()->error(t('Missing slogitem.'));
      }
    }

    // discussion node
    if ($result['success'] && $slogitem) {
      $nid = $slogitem->getTargetEntityId();
      $discussion_node = Node::load($nid);
      if ($discussion_node) {
        $result['discussion_node'] = $discussion_node;
      }
      else {
        $result['success'] = FALSE;
        self::logger()->error(t('Missing discussion node.'));
      }
    }


    if (!$result['success']) {
      if ($has_old_state_data) {
        // invalidate old state
        $key = REQUEST_TIME;
        $state_data['lost'][$key] = [
          'menu-tid' => $state_data['menu-tid'],
          'slogitem-id' => $state_data['slogitem-id'],
        ];
        $state_data['menu-tid'] = FALSE;
        $state_data['slogitem-id'] = FALSE;
        $node_state->setStateData($state_data)->save();
      }
    }
    elseif ($menu_term && $slogitem) {
      $new_tid = $menu_term->id();
      $new_sid = $slogitem->id();
      if ($has_old_state_data) {
        $old_tid = $state_data['menu-tid'];
        $old_sid = $state_data['slogitem-id'];
        if ($new_tid !== $old_tid || $new_sid !== $old_sid) {
          // invalidate old state
          $key = REQUEST_TIME;
          $state_data['lost'][$key] = [
            'menu-tid' => $state_data['menu-tid'],
            'slogitem-id' => $state_data['slogitem-id'],
          ];
        }
      }
      // set state data
      $state_data['menu-tid'] = $new_tid;
      $state_data['slogitem-id'] = $new_sid;
      $node_state->setStateData($state_data)->save();
    }

    return $result;
  }

  /**
   * 
   * @param integer $discussion_node_id
   * @return boolean
   */
  public static function mwDiscussionHasComments($discussion_node_id) {
    $result = \Drupal::database()->select('comment_field_data', 'c')
        ->fields('c', ['cid'])
        ->condition('entity_id', $discussion_node_id)
        ->range(0, 1)
        ->execute()
        ->fetchCol();

    return !empty($result);
  }

  public static function logger() {
    return \Drupal::logger('sxt_opentalk');
  }

}
