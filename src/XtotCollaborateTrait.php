<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\XtotCollaborateTrait.
 */

namespace Drupal\sxt_opentalk;

use Drupal\sxt_workflow\SlogXtwf;

/**
 * A Trait for ...
 */
trait XtotCollaborateTrait {
  
  protected function setCollabNodeUserData($reload = FALSE) {
    $this->node_id = $node_id = (integer) \Drupal::request()->get('base_entity_id');
    $this->node = SlogXtwf::getNode($node_id);
    $this->has_node_state = $has_node_state = SlogXtwf::hasNodeState($node_id, $reload);
    $this->node_state = $node_state = $has_node_state ? SlogXtwf::getNodeState($node_id) : FALSE;
    $this->user = \Drupal::currentUser();
    $this->user_id = $this->user->id();
    $this->has_user_data = $node_state ? $node_state->hasUserData($this->user_id) : FALSE;
  }

}
