<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\XtotCollaborateFormTrait.
 */

namespace Drupal\sxt_opentalk;

use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_opentalk\XtotCollaborateTrait;

/**
 * A Trait for ...
 */
trait XtotCollaborateFormTrait {
  
  use XtotCollaborateTrait;

  protected function makeMsgClosable(array &$form) {
    $form['#attributes']['class'][] = 'slogxt-msg';
  }

  public function formValidateHasChanged(FormStateInterface $form_state) {
    if ($this->has_user_data) {
      $changed = FALSE;
      $user_data = $this->node_state->getUserData($this->user_id) ?? [];
      foreach ($user_data as $key => $value) {
        $new = $form_state->getValue($key);
        if (isset($new) && $new !== $value) {
          $changed = TRUE;
          break;
        }
      }
      if (!$changed) {
        $form_state->setErrorByName('', t('No changes have been done.'));
      }
    }
  }

  protected function addFieldAdminNotice(array &$form, $description = '') {
    if (empty($description)) {
      $description = t('Specify a reason for your action.');
    }
    $form['admin_notice'] = [
      '#type' => 'textfield',
      '#title' => t('Notice'),
      '#description' => $description,
      '#required' => TRUE,
      '#maxlength' => 64,
        ] + $this->getInputFieldWrapper();
  }

}
