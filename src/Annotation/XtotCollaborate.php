<?php

/**
 * @file
 * Contains \Drupal\sxt_opentalk\Annotation\XtotCollaborate.
 */

namespace Drupal\sxt_opentalk\Annotation;

use Drupal\slogxt\Annotation\XtAnnotationBase;

/**
 * Defines a Plugin annotation object for opentalk collaborate plugins.
 *
 * @Annotation
 */
class XtotCollaborate extends XtAnnotationBase {

//            /**
//             * Required permissions
//             *
//             * @var array  
//             */
//            public $permissions = FALSE;
  
}
