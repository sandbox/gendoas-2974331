/**
 * @file
 * A Backbone View for Content Collaborate Dialog.
 */

(function ($, Drupal, drupalSettings, Backbone, _, undefined) {

  "use strict";

  var _xtot = Drupal.sxt_opentalk
      , _sxt = Drupal.slogxt;

  var xtotViewExt = {
    sxtThisClass: 'sxt_opentalk.XtotCollaborateView',
    actions: {},
    nix: {}
  };

  var actionsExt = {
    __actionBookmark: function () {
      alert('XtotCollaborateView::__actionBookmark');
    },
    __actionSave: function () {
      alert('XtotCollaborateView::__actionSave');
    }
  };
  xtotViewExt.actions.sxt_opentalk = actionsExt;

  /**
   * Backbone View for Content Collaborate Dialog
   */
  _xtot.XtotCollaborateView = _sxt.XtEditFormView.extend(xtotViewExt);

})(jQuery, Drupal, drupalSettings, Backbone, _);
