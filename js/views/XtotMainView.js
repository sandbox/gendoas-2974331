/**
 * @file
 * A Backbone View for ....
 */

(function ($, Drupal, drupalSettings, Backbone, _) {

  "use strict";

  var _xtot = Drupal.sxt_opentalk
      , _sxt = Drupal.slogxt;

  var xtotViewExt = {
    sxtThisClass: 'sxt_opentalk.XtotMainView',
    initViews: function () {
      var _xtsi = Drupal.sxt_slogitem;
      $.each(_xtsi.targetsContent, function (idx, target) {
        var cView = _xtsi.getSiContentView(target);

        cView.specialView = new _xtot.XtotSpecialView({
          el: cView.$el.find('.content-special'),
          model: new _xtot.XtotSpecialModel(),
          contentView: cView
        });
      });
    },
    initActions: function () {
      var xtsiContentViewProto = Drupal.sxt_slogitem.XtsiContentView.prototype
          , actionsExt = {
            actionVote: function () {
              var args = {
                view: 'AtWorkView',
                provider: 'slogxt',
                options: {
                  infoText: 'actionVote'
                }
              };
              _sxt.dialogViewOpen(args);
            },
            actionCollaborate: function () {
              var nid = this.getTbnodeId();
              if (nid) {
                var args = {
                  view: 'XtotCollaborateView',
                  provider: 'sxt_opentalk',
                  options: {
                    editTarget: 'collaborate/c',
                    entityId: nid,
                    noWizardPath: true,
                    contentProvider: 'sxt_opentalk',
                    mainView: _xtot.mainView()
                  }
                };
                _sxt.dialogViewOpen(args);
              }
            }
          };
      xtsiContentViewProto.actions.sxt_opentalk = actionsExt;
    },
    onDocumentReady: function () {
      this.initActions();
      this.initViews();
    }

  };

  /**
   * Backbone View for a slogitem list.
   */
  _xtot.XtotMainView = Backbone.View.extend(xtotViewExt);

})(jQuery, Drupal, drupalSettings, Backbone, _);
