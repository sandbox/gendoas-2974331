/**
 * @file
 * A Backbone View for ....
 */

(function ($, Drupal, drupalSettings, Backbone, _) {

  "use strict";

  var _xtot = Drupal.sxt_opentalk
      , _sxt = Drupal.slogxt;

  var xtotViewExt = {
    sxtThisClass: 'sxt_opentalk.XtotSpecialView',
    initialize: function (options) {
      this.contentView = options.contentView;
      this.contentModel = this.contentView.model;
      this.initElements();

      // events
      this.listenTo(this.contentModel, 'change:specialData', this.onChangeSpecialData);

      return this;
    },
    initElements: function () {
      this.$leftWrapper = this.$el.find('.special-left-wrapper');
      this.$rightWrapper = this.$el.find('.special-right-wrapper');
      this.$btnFirst = this.$el.find('.buttons-wrapper .special-button.first');
      this.$btnNext = this.$el.find('.buttons-wrapper .special-button.next');
      //props
      this.$btnFirst.addClass('sxtot-icon-first').show();
      this.$btnFirst.attr('title', Drupal.t('Vote'));
      this.$btnNext.addClass('sxtot-icon-next').show();
      this.$btnNext.attr('title', Drupal.t('Collaborate'));
      // attach
      this.$el.find('.buttons-wrapper').on('click', function (e) {
        e.stopPropagation();
      });
      this.$btnFirst.on('click', $.proxy(this.clickFirst, this));
      this.$btnNext.on('click', $.proxy(this.clickNext, this));
    },
    onChangeSpecialData: function (cm, data) {
      var nodeStateShow = data ? data.nodeStateShow || false : false;
      if (nodeStateShow) {
        this.$leftWrapper.html(data.htmlLeft);
        this.$rightWrapper.html(data.htmlRight);
      }
      this.contentView.showSpecial(nodeStateShow);
    },
    callAction: function (action, args) {
      var provider = 'sxt_opentalk'
          , callback = this.contentView.actions[provider][action];
      if (callback && $.isFunction(callback)) {
        callback.call(this.contentView, args);
      } else {
        var target = this.contentView.sxtThisClass + '.actions.' + provider + '.' + action;
        _sxt.notImplemented(target);
      }
    },
    clearState: function (e) {
      this.$leftWrapper.empty();
      this.$rightWrapper.empty();
    },
    clickFirst: function (e) {
      this.contentView.activateContent();
      this.callAction('actionVote');
    },
    clickNext: function (e) {
      this.contentView.activateContent();
      this.callAction('actionCollaborate');
    }

  };

  /**
   * Backbone View for a slogitem list.
   */
  _xtot.XtotSpecialView = Backbone.View.extend(xtotViewExt);

})(jQuery, Drupal, drupalSettings, Backbone, _);
