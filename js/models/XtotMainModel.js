/**
 * @file
 * A Backbone Model for SlogMain.
 */

(function ($, Backbone, Drupal, drupalSettings, _) {

  "use strict";

  var _xtot = Drupal.sxt_opentalk
      , _xtsi = Drupal.sxt_slogitem
      , _sxt = Drupal.slogxt;

  /**
   * Backbone model ....
   */
  var xtotMainModelExt = {
    sxtThisClass: 'sxt_opentalk.XtotMainModel',
    defaults: {
    },
    initialize: function () {

//      var stateModel = _sxt.stateModel();
//      this.listenTo(stateModel, 'change:stateState', this.onChangeStateState);

      return this;
    },
    onChangeStateState: function (stateModel, stateState) {
//      var stateData = stateModel.get('stateData')
//          , tblineModels = _xtsi.getTblineModels()
//          , thisModel = _xtsi.mainModel()
//          , tblData, data, tbData;
//      try {
//        switch (stateState) {
//          case 'loaded':
//            break;
//          case 'writing':
//            break;
//        }
//      } catch (e) {
//        slogErr('XtotMainModel.onChangeStateState: ' + stateState + "\n" + e.message);
//      }
    }

  };

  _xtot.XtotMainModel = Backbone.Model.extend(xtotMainModelExt);

})(jQuery, Backbone, Drupal, drupalSettings, _);
