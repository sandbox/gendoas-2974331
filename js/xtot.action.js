/**
 * @file
 * ...
 */

(function ($, Drupal, drupalSettings) {

  "use strict";

//  var _xtot = Drupal.sxt_opentalk
//      , _sxt = Drupal.slogxt;
  var _sxt = Drupal.slogxt;

  var mainActionsExt = {
    actionCollaboration: function () {
      _sxt.resetGroupPerms();
      var args = {
        view: 'XtEditFormView',
        provider: 'slogxt',
        options: {
          editTarget: 'collaborate_main',
          entityId: _sxt.getUserId(),
          contentProvider: 'sxt_opentalk',
          noWizardPath: true
        }
      };
      _sxt.dialogViewOpen(args);
    }
  };
  _sxt.MainActionsView.prototype.actions.sxt_opentalk = mainActionsExt;

//                  _xtot.addXtsiActions = function () {
//                    var histActionsExt = {
//                      actionFollow: function () {
//                //        slogLog('xtot.action.js.........XtsiHistoryView.. actionFollow');
//                        alert('xtot.action.js.........XtsiHistoryView.. actionFollow.. to be removed...no follow!!!!');
//                        var idxData = this._sxtGetCheckedItemsIdxData();
//                        if (idxData && idxData.count > 0) {
//                        } else {
//                          this._sxtAlertNotSelected();
//                        }
//                      }
//                    };
//                    Drupal.sxt_slogitem.XtsiHistoryView.prototype.actions.sxt_opentalk = histActionsExt;
//
//                    var bmActionsExt = {
//                      actionFollow: function () {
//                        alert('xtot.action.js...XtsiBookmarkView........ actionFollow.. to be removed...no follow!!!!');
//                        var idxData = this._sxtGetCheckedItemsIdxData();
//                        if (idxData && idxData.count > 0) {
//                //        var type = this.getContentType()
//                //            , tabData = {
//                //              labels: {
//                //                dialogTitle: Drupal.t('Save'),
//                //                submitLabel: Drupal.t('Next')
//                //              },
//                //              selected: idxData,
//                //              path: _xtsi.getBmstorageActionPath(type, 'save'),
//                //              provider: 'sxt_slogitem'
//                //            };
//                //        this._sxtActionsWizardStart(tabData);
//                        } else {
//                          this._sxtAlertNotSelected();
//                        }
//                      }
//                    };
//                    Drupal.sxt_slogitem.XtsiBookmarkView.prototype.actions.sxt_opentalk = bmActionsExt;
//                  }

  //
  // on document ready
  //
//                $(document).ready(function () {
//                  // workaround 
//                  // because it is not impossible to set dependency sxt_slogitem/sxt_slogitem.core 
//                  // (see sxt_opentalk.libraries.yml)
//                  _xtot.addXtsiActions();
//                });

})(jQuery, Drupal, drupalSettings);

