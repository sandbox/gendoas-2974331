/**
 * @file
 * sxt_opentalk/js/xtot.dvc.commands.js
 */

(function ($, Drupal, _, undefined) {

  var _sxt = Drupal.slogxt;

  var xtotCommands = {
    alterFormCollaborate: function () {
      var $form = this.$activeContent.find('form');
      $form.find('#xtot-severity')
          .once('xtot')
          .on('change', $.proxy(function (e) {
            e.stopPropagation();
            var severity = e.target.value || 'none'
                , $info = $form.find('#xtot-severity-' + severity)
                , info = $info.length ? $info[0].value : false;
            !!info && $(e.target).closest('.js-form-item').find('.description').html(info);

          }, this));
      $form.find('#xtot-workflow')
          .once('xtot')
          .on('change', $.proxy(function (e) {
            e.stopPropagation();
            var workflow = e.target.value || 'none'
                , $info = $form.find('#xtot-workflow-' + workflow)
                , info = $info.length ? $info[0].value : false;
            !!info && $(e.target).closest('.js-form-item').find('.description').html(info);

          }, this));
      $form.find('#xtot-severity').trigger('change');
      $form.find('#xtot-workflow').trigger('change');
    },
    finishedWorkflowChanged: function (args) {
      function proccess(result) {
        if (!result) {
          //refresh ?????
          slogErr('xtot.dvc.commands.js..finishedWorkflowChanged.proccess: ' + !!result);
        } else {
          var _xtsi = Drupal.sxt_slogitem
              , special = result ? result.special || [] : []
              , changed = (!special || (special.nodeStateHash !== special_new.nodeStateHash));
          if (changed) {
            // update db
            result.special = special_new;
            $.sxtIdbStorage.setItem({store: 'slogxtContent'}, storageKey, JSON.stringify(result));
            // update view
            $.each(_xtsi.targetsContent, function (idx, target) {
              var cmView = _xtsi.getSiContentView(target)
                  , nid = !!cmView.nodeData ? cmView.nodeData.nodeId || false : false;
              if (nid && nid == node_id) {
                cmView.model.set('specialData', special_new);
              }
            });
          }
        }
      }


      // 
      var special_new = args
          , node_id = args.node_id
          , storageKey = 'xtsiRoute::node.' + node_id + '::data';
      $.sxtIdbStorage.getItem({store: 'slogxtContent'}, storageKey)
          .fail(function (error, event) {
            proccess(false);
          })
          .done(function (result, event) {
            if (result) {
              var result = JSON.parse(result);
              proccess(result);
            } else {
              proccess(false);
            }
          });
    },
    resolvePathDiscussionAction: function (key, tabData) {
      var args = tabData.resolveArgs ? tabData.resolveArgs[key] || {} : {}
      , btnTitle = Drupal.t('View discussion')
          , xtTitle = args.xtTitle || btnTitle
          , info = args.info || ''
          , xtInfo = args.xtInfo || ''
          , $doBtn;


      tabData['dialogTitle'] = tabData['actionTitle'] + ': ' + xtTitle;
      $doBtn = Drupal.theme.slogxtDialogDoButton(this.$activeContent, btnTitle, info, xtInfo);

      $doBtn.on('click', $.proxy(function () {
        var path = (tabData.resolveArgs || {}).path || false
            , aView = Drupal.sxt_slogitem.getAlternateCView();
        if (path) {
          aView.loading = true;
          aView.show();
          aView.activateContent();
          this.$btnClose.click();
          aView.setSyncOnchange();
          aView.loadOtherContent(path);
          aView.enlargeContentView();
        }
      }, this));
    },
    createdFirstDiscussComment: function (args) {
      if (args.path) {
        var base_node_id = args.base_node_id
            , baseView = Drupal.sxt_slogitem.getActiveContentView()
            , bvNodeId = (baseView.nodeData || {}).nodeId || 0
            , aView = Drupal.sxt_slogitem.getAlternateCView();
        aView.loading = true;
        aView.show();
        aView.activateContent();
        aView.setSyncOnchange();
        !!args.goto && aView.provideGoTo(args.goto);
        aView.loadOtherContent(args.path);
        aView.enlargeContentView();


        setTimeout($.proxy(function () {
          this.$btnClose.click();
          if (baseView && base_node_id === bvNodeId) {
            baseView.refresh();
          }
        }, this), 1000);
      }
    }
  };

  _sxt.dvc.commands.sxt_opentalk = xtotCommands;

})(jQuery, Drupal, _);
