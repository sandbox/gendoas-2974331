/**
 * @file sxt_opentalk.module.js
 *
 * Defines the behavior and other functions of sxt_opentalk .
 */
(function ($, Drupal, drupalSettings, Backbone, _, undefined) {

  "use strict";

  Drupal.sxt_opentalk = {models: {}, views: {}};
  var _xtot = Drupal.sxt_opentalk
  , _xtwf = Drupal.sxt_workflow;

  /**
   * Behaviors
   */
  Drupal.behaviors.sxt_opentalk = {
    attach: function (context) {
      var xtotModels = _xtot.models
          , xtotViews = _xtot.views;
      if (xtotModels.mainModel) {
        return;
      }

      xtotModels.mainModel = new _xtot.XtotMainModel();
      xtotViews.mainView = new _xtot.XtotMainView({
        el: $('body').first(),
        model: xtotModels.mainModel
      });

    } // attach: function
  };  // Drupal.behaviors.sxt_opentalk


  /**
   * Sxt opentalk namespace.
   */
  var xtotExt = {
    // A hash of instances: views, models and colls.
    views: {},
    models: {},
    filters: {},
    mainModel: function () {
      return this.models.mainModel;
    },
    mainView: function () {
      return this.views.mainView;
    },
    ntypeHasWorkflow: function (nType) {
      var workflows = _xtwf.getNodetypeWorkflows(nType);
      return !!workflows;
    }
  };
  $.extend(true, _xtot, xtotExt);
  

  /**
   * @type filter functions
   */
  var filterExt = {
    nodetypeNeedsWorkflow: function (nodeData) {
      if (!nodeData || !nodeData.nodeType) {
        slogErr('filter.nodetypeNeedsWorkflow: node type is required');
        return false;
      }

      return _xtot.ntypeHasWorkflow(nodeData.nodeType);
    }
  };
  $.extend(true, _xtot.filters, filterExt);

  //
  // on document ready
  //
  $(document).ready(function () {
//    slogLog('sxt_opentalk.module.js: ..............$(document).ready');
    _xtot.mainView().onDocumentReady();
  });


})(jQuery, Drupal, drupalSettings, Backbone, _);
